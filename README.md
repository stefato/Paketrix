# CI/CD

[![pipeline status](https://gitlab.com/stefato/ParcelMate/badges/master/pipeline.svg)](https://gitlab.com/stefato/ParcelMate/commits/master)
[![Build Status](https://www.bitrise.io/app/7b1b2203c5ed99ae/status.svg?token=ZQjFaEB9Mwant7JGPoAO_Q)](https://www.bitrise.io/app/7b1b2203c5ed99ae)

# Codeanalyse
[![sq bugs](https://sonarcloud.io/api/project_badges/measure?project=com.stefato.paketrix&metric=bugs)](https://sonarcloud.io/dashboard?id=com.stefato.paketrix)
[![sq vuln](https://sonarcloud.io/api/project_badges/measure?project=com.stefato.paketrix&metric=vulnerabilities
)](https://sonarcloud.io/dashboard?id=com.stefato.paketrix)
[![sq smell](https://sonarcloud.io/api/project_badges/measure?project=com.stefato.paketrix&metric=code_smells)](https://sonarcloud.io/dashboard?id=com.stefato.paketrix)
[![sq coverage](https://sonarcloud.io/api/project_badges/measure?project=com.stefato.paketrix&metric=coverage
)](https://sonarcloud.io/dashboard?id=com.stefato.paketrix)
[![sq duplicated](https://sonarcloud.io/api/project_badges/measure?project=com.stefato.paketrix&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=com.stefato.paketrix)
[![sq loc](https://sonarcloud.io/api/project_badges/measure?project=com.stefato.paketrix&metric=ncloc)](https://sonarcloud.io/dashboard?id=com.stefato.paketrix)

# Paketrix
Paketverfolgung über die Webseiten der großen Logistik-Dienstleister ist komplizierter als nötig. Ein Speichern der verfolgten Pakete ist zwar nach vorheriger Anmeldung und in unternehmenseigenen Apps möglich, ist aber die Anlage von mehreren Accounts oder die Installation von einer App pro Unternehmen nicht erwünscht, muss auf Drittparteien gesetzt werden. Eine einzige zusammenhängende Lösung ist aus offensichtlichen Gründen nicht zu erwarten.

Die offiziellen APIs sind nur für Geschäftskunden zugänglich und/oder erlauben keine Verwendung im Zusammenhang mit Konkurrenten.
Paketrix versucht daher durch die Verwendung von öffentlichen Schnittstellen und, wenn nötig, HTML-Scraping, eine Paketverfolgung für die 5 größten Logistik-Dienstleister in Deutschland bereitzustellen.
Da die Verwendung von öffentlich zugänglichen Daten der Unternehmen in einem Softwareprodukt rechtlich nicht eindeutig geklärt ist, ist diese App nur zu Lernzwecken erstellt worden. Eine kommerzielle Absicht liegt nicht vor.
## Ziele
 - eine App für die Verfolgung von Paketen der 5 größten deutschen Paketdienste: DHL, Hermes, DPD, UPS, GLS
 - automatische Zuordnung der Trackingnummer zu dem entsprechenden Dienstleister
 - automatische Aktualisierung des Paketstatus
 - minimale Größe der App
 - Anwendung der momentanen Best Practices für Android
 - möglichst automatisierter Workflow


## Implementierung
- Aufteilung in 3 nicht abhängige Schichten: Data, Domain, Presentation
- Domain-Schicht ist nicht abhängig von Android, kann also auch je nach Anforderungen mit anderen Data- und Presentation-Schichten verwendet werden
- somit sind andere Plattformen unter Wiederverwendung der Business-Logik denkbar wie z.B. einfaches CLI-Programm, Web-App, Desktop-App mit Swing/JavaFx, iOS-App mit j2objc
- MVI-/MVP-Architektur in der Presentation-Schicht
- zustandslose Views(WIP! #55) bedeuten, dass jedes UiModel zur Crashanalyse benutzt werden kann -> Zustände vor jedem Crash sind in Crashlytics sichtbar

## Bibliotheken
- Room Persistence Library als SQLite-Abstraktion
- Mosby für zahlreiche Erleichterungen bei Verwendung einer MVI-/MVP-Architektur
- Dependency Injection mit Dagger 2
- Netzwerk und REST mit OkHTTP und Retrofit
- Reactive Programming mit RxJava
- Immutable Datenklassen mit AutoValue
- Hintergrund-Jobs mit evernote-jobs
- Crash Reporting mit Crashlytics
- Performance Monitoring mit Firebase Performance
- MockWebServer für zuverlässige Netzwerktests
- Integrationstests mit Espresso und UiAutomator
- LeakCanary
- Guava
- Gson
- JSoup

## DevOps
- git-basierte Versionsnamen
- statische Codeanalyse mit SonarQube
- CI/CD mit GitLabCi und/oder Bitrise unter Verwendung von Docker-Containern
  - unit tests für alle feature branches
  - unit und integration tests für alle merge requests
  - unit und integration tests + SonarQube-Analyse für alle master commits
  - automatisches Deployment im Google App Store für jedes Tag mit v\*.\*.\*

## Download
<a href='https://play.google.com/store/apps/details?id=com.stefato.paketrix&pcampaignid=MKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Jetzt bei Google Play' src='https://play.google.com/intl/en_gb/badges/images/generic/de_badge_web_generic.png' height=90/></a>

## Lizenz
        Copyright 2018 stefato

        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at

            http://www.apache.org/licenses/LICENSE-2.0

        Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS,
        WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the License for the specific language governing permissions and
        limitations under the License.

