package com.stefato.paketrix

import com.stefato.paketrix.data.local.entities.TrackingEntity
import com.stefato.paketrix.data.local.entities.WaypointEntity
import com.stefato.paketrix.domain.model.Carrier
import com.stefato.paketrix.domain.model.Tracking
import com.stefato.paketrix.domain.model.Waypoint
import org.threeten.bp.LocalDateTime
import java.util.*

//TODO tidy up
object AndroidTestData {

    @JvmStatic
    val DB_NAME = "tracking-db"

    //Server reponses
    @JvmStatic
    val DPD_RESPONSE_JSON = "dpdResponse.json"
    @JvmStatic
    val DHL_RESPONSE_HTML = "dhlResponse.html"
    @JvmStatic
    val HERMES_RESPONSE_JSON = "hermesResponse.json"
    @JvmStatic
    val UPS_RESPONSE_JSON = "upsResponse.html"
    @JvmStatic
    val GLS_RESPONSE_JSON = "glsResponse.json"

    //Server responses on refresh
    @JvmStatic
    val DHL_RESPONSE_REFRESH_HTML = "dhlResponseRefresh.html"
    @JvmStatic
    val DPD_RESPONSE_REFRESH_JSON = "dpdReponseRefresh.json"

    //tracking numbers
    @JvmStatic
    val DPD_TEST_TRACKING_NUMBER = "09445981861275"
    @JvmStatic
    val DHL_TEST_TRACKING_NUMBER = "JJD000390005582054921"
    @JvmStatic
    val HERMES_TEST_TRACKING_NUMBER = "36333167377919"
    @JvmStatic
    val UPS_TEST_TRACKING_NUMBER = "1ZW000186893032102"
    @JvmStatic
    val GLS_TEST_TRACKING_NUMBER = "85440442362"

    val createTrackingTestParameters: Collection<Array<Any>>
        get() = Arrays.asList(
                arrayOf(dhlTestParcel, DHL_RESPONSE_HTML),
                arrayOf(hermesTestParcel, HERMES_RESPONSE_JSON),
                arrayOf(dpdTestParcel, DPD_RESPONSE_JSON),
                arrayOf(upsTestParcel, UPS_RESPONSE_JSON),
                arrayOf(glsTestParcel, GLS_RESPONSE_JSON)
        )

    @JvmStatic
    val dhlTestParcel
        get() = Tracking(
                tag = "Erfolgreich zugestellt",
                carrier = Carrier.DHL,
                trackingNumber = DHL_TEST_TRACKING_NUMBER,
                waypoints = listOf(
                        Waypoint("2017-12-08T14:24".toLocalDateTime(), message = "Erfolgreich zugestellt"),
                        Waypoint("2017-12-07T13:53".toLocalDateTime(), message = "Unterwegs"),
                        Waypoint("2017-12-06T09:11".toLocalDateTime(), message = "Auftrag eingegangen")
                )
        )

    @JvmStatic
    val refreshDhlTestParcel: Tracking
        get() = Tracking(
                tag = "Die Sendung wurde erfolgreicher zugestellt.",
                carrier = Carrier.DHL,
                trackingNumber = DHL_TEST_TRACKING_NUMBER,
                waypoints = listOf(
                        Waypoint("2017-11-19T09:00".toLocalDateTime(), message = "Die Sendung wurde erfolgreicher zugestellt."),
                        Waypoint("2017-12-08T14:24".toLocalDateTime(), message = "Erfolgreich zugestellt"),
                        Waypoint("2017-12-07T13:53".toLocalDateTime(), message = "Unterwegs"),
                        Waypoint("2017-12-06T09:11".toLocalDateTime(), message = "Auftrag eingegangen")
                )
        )

    @JvmStatic
    val dpdRefreshTestParcel: Tracking
        get() = Tracking(
                tag = "Zugestellt",
                carrier = Carrier.DPD,
                trackingNumber = DPD_TEST_TRACKING_NUMBER,
                waypoints = listOf(
                        Waypoint("2017-11-16T12:36".toLocalDateTime(), message = "Zugestellt"),
                        Waypoint("2017-11-16T06:01".toLocalDateTime(), message = "In Zustellung"),
                        Waypoint("2017-11-12T01:19".toLocalDateTime(), message = "Im Paketzentrum")
                )
        )

    @JvmStatic
    val dpdTestParcel: Tracking
        get() = Tracking(
                tag = "In Zustellung",
                carrier = Carrier.DPD,
                trackingNumber = DPD_TEST_TRACKING_NUMBER,
                waypoints = listOf(
                        Waypoint("2017-11-16T06:01".toLocalDateTime(), message = "In Zustellung"),
                        Waypoint("2017-11-12T01:19".toLocalDateTime(), message = "Im Paketzentrum")
                )
        )

    @JvmStatic
    val hermesTestParcel: Tracking
        get() = Tracking(
                tag = "Die Sendung wurde im Hermes Logistikzentrum sortiert und an die Retourenstelle des Versenders weitergeleitet.",
                carrier = Carrier.HERMES,
                trackingNumber = HERMES_TEST_TRACKING_NUMBER,
                waypoints = listOf(
                        Waypoint("2017-12-11T07:04".toLocalDateTime(), message = "Die Sendung wurde im Hermes Logistikzentrum sortiert und an die Retourenstelle des Versenders weitergeleitet."),
                        Waypoint("2017-12-08T12:56".toLocalDateTime(), message = "Der Empfänger hat einen WunschPaketShop für die Zustellung gewählt.")
                )
        )

    @JvmStatic
    val upsTestParcel: Tracking
        get() = Tracking(
                tag = "Zugestellt",
                carrier = Carrier.UPS,
                trackingNumber = UPS_TEST_TRACKING_NUMBER,
                waypoints = listOf(
                        Waypoint("2017-02-16T13:25".toLocalDateTime(), message = "Zugestellt")
                )
        )

    @JvmStatic
    val glsTestParcel: Tracking
        get() = Tracking(
                tag = "Das Paket wurde im GLS PaketShop zugestellt (siehe oben)",
                carrier = Carrier.GLS,
                trackingNumber = GLS_TEST_TRACKING_NUMBER,
                waypoints = listOf(
                        Waypoint("2017-12-08T14:24".toLocalDateTime(), message = "Das Paket wurde im GLS PaketShop zugestellt (siehe oben)"),
                        Waypoint("2017-12-08T07:59".toLocalDateTime(), message = "Das Paket befindet sich im GLS Zustellfahrzeug und wird voraussichtlich im Laufe des Tages zugestellt.")
                )
        )

    private fun String.toLocalDateTime() = LocalDateTime.parse(this)


    private val DATE_TIMES = getDateTimes()
    private val CITIES = listOf("Berlin", "Munich", "Hamburg", "Cologne")
    private const val MESSAGE = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr."
    private val IDS = listOf(22L, 35L)
    private const val TRACKING_NUMBER = "8294837583482939493"

    private fun getDateTimes(): List<LocalDateTime> {
        return listOf(
                LocalDateTime.of(2017, 3, 12, 15, 33),
                LocalDateTime.of(2017, 10, 9, 11, 48),
                LocalDateTime.of(1111, 11, 11, 11, 11),
                LocalDateTime.of(2018, 12, 3, 0, 29)
        )
    }

    @JvmStatic
    fun getParcelEntity(carrier: Carrier, i: Int) =
            TrackingEntity(id = IDS[i], name = "", trackingNumber = TRACKING_NUMBER,
                    carrier = carrier.id, tag = MESSAGE, waypoints = getWaypointEntities(i))


    private fun getWaypointEntities(index: Int) =
            (index..index + 2).map {
                WaypointEntity(city = CITIES[it], message = MESSAGE, dateTime = DATE_TIMES[it]) }

}

