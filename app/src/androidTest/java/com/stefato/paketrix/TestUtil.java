package com.stefato.paketrix;


import android.graphics.drawable.ColorDrawable;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.PerformException;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.espresso.util.HumanReadables;
import android.support.test.espresso.util.TreeIterables;
import android.support.test.internal.util.Checks;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.domain.model.Waypoint;
import com.stefato.paketrix.testUtil.CustomViewAction;
import com.stefato.paketrix.testUtil.RestServiceTestHelper;
import com.stefato.paketrix.presentation.MainActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.FormatStyle;

import java.util.concurrent.TimeoutException;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

public class TestUtil {

    public static Matcher<View> withBackgroundColor(final int color) {
        Checks.checkNotNull(color);
        return new BoundedMatcher<View, TextView>(TextView.class) {
            @Override
            public boolean matchesSafely(TextView textView) {
                return color == ((ColorDrawable) textView.getBackground()).getColor();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("with text color: ");
            }
        };
    }

    public static void checkIfTrackingVisible(String trackingNumber, Carrier carrier,
                                              Waypoint waypoint, int pos) {
        LocalDateTime dateTime = waypoint.getDateTime();
        String date = dateTime.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM));
        String time = dateTime.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT));


        onView(withId(R.id.tracking_rv)).check(
                matches(CustomViewAction.atPosition(pos,hasDescendant(withText(trackingNumber)))));
        onView(withId(R.id.tracking_rv)).check(
                matches(CustomViewAction.atPosition(0,hasDescendant(
                        withText(carrier.getDisplayName())))));
        onView(withId(R.id.tracking_rv)).check(
                matches(CustomViewAction.atPosition(0,hasDescendant(withBackgroundColor(
                        carrier.getColor())))));
        onView(withId(R.id.tracking_rv)).check(
                matches(CustomViewAction.atPosition(0,hasDescendant(
                        withText(waypoint.getMessage())))));
        onView(withId(R.id.tracking_rv)).check(
                matches(CustomViewAction.atPosition(0,hasDescendant(withText(date)))));
        onView(withId(R.id.tracking_rv)).check
                (matches(CustomViewAction.atPosition(0,hasDescendant(withText(time)))));
    }

    public static void checkIfTrackingVisible(Tracking tracking, int pos) {
        LocalDateTime dateTime = tracking.getWaypoints().get(0).getDateTime();
        String date = dateTime.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM));
        String time = dateTime.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT));
        Carrier carrier=tracking.getCarrier();


        onView(withId(R.id.tracking_rv)).check(
                matches(CustomViewAction.atPosition(pos,hasDescendant(
                        withText(tracking.getTrackingNumber())))));
        onView(withId(R.id.tracking_rv)).check(
                matches(CustomViewAction.atPosition(0,hasDescendant(
                        withText(carrier.getDisplayName())))));
        onView(withId(R.id.tracking_rv)).check(
                matches(CustomViewAction.atPosition(0,hasDescendant(
                        withBackgroundColor(carrier.getColor())))));
        onView(withId(R.id.tracking_rv)).check(
                matches(CustomViewAction.atPosition(0,hasDescendant(
                        withText(tracking.getTag())))));
        onView(withId(R.id.tracking_rv)).check(
                matches(CustomViewAction.atPosition(0,hasDescendant(withText(date)))));
        onView(withId(R.id.tracking_rv)).check(
                matches(CustomViewAction.atPosition(0,hasDescendant(withText(time)))));
    }


    //https://stackoverflow.com/questions/21417954/espresso-thread-sleep
    public static ViewAction waitId(final int viewId, final long millis) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isRoot();
            }

            @Override
            public String getDescription() {
                return "wait for a specific view with id <" + viewId + "> during " + millis + " millis.";
            }

            @Override
            public void perform(final UiController uiController, final View view) {
                uiController.loopMainThreadUntilIdle();
                final long startTime = System.currentTimeMillis();
                final long endTime = startTime + millis;
                final Matcher<View> viewMatcher = withId(viewId);

                do {
                    for (View child : TreeIterables.breadthFirstViewTraversal(view)) {
                        // found view with required ID
                        if (viewMatcher.matches(child)) {
                            return;
                        }
                    }

                    uiController.loopMainThreadForAtLeast(50);
                }
                while (System.currentTimeMillis() < endTime);

                // timeout happens
                throw new PerformException.Builder()
                        .withActionDescription(this.getDescription())
                        .withViewDescription(HumanReadables.describe(view))
                        .withCause(new TimeoutException())
                        .build();
            }
        };
    }

    public static void inputTracking(String trackingNumber, Carrier carrier) {
        onView(withId(R.id.create_btn)).perform(click());
        onView(withId(R.id.id_input)).perform(typeText(trackingNumber))
                .perform(closeSoftKeyboard());
        onView(withText(carrier.getDisplayName())).perform(click());
        ViewInteraction mDButton = onView(withId(R.id.md_buttonDefaultPositive));
        mDButton.perform(click());
    }

    public static void enqueueOkServerResponseFromFile(MockWebServer server, String filename) throws Exception {
        server.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(RestServiceTestHelper.getStringFromFile(
                        InstrumentationRegistry.getInstrumentation().getContext(), filename)));
    }

    public static int getRecyclerVIewSize(MainActivity activity) {
        RecyclerView recyclerView = activity.findViewById(R.id.tracking_rv);
        return recyclerView.getAdapter().getItemCount();
    }
}
