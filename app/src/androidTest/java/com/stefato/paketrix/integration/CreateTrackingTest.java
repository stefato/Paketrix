package com.stefato.paketrix.integration;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;

import com.stefato.paketrix.AndroidTestData;
import com.stefato.paketrix.PaketrixApplication;
import com.stefato.paketrix.TestUtil;
import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.presentation.MainActivity;
import com.stefato.paketrix.testDi.DaggerTestAppComponent;
import com.stefato.paketrix.testDi.TestAppComponent;
import com.stefato.paketrix.testUtil.CustomViewAction;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Collection;

import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static com.stefato.paketrix.TestUtil.enqueueOkServerResponseFromFile;
import static com.stefato.paketrix.TestUtil.getRecyclerVIewSize;
import static com.stefato.paketrix.TestUtil.inputTracking;
import static junit.framework.Assert.assertEquals;
import static org.junit.runners.Parameterized.Parameter;
import static org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CreateTrackingTest {

    @Parameter
    public Tracking tracking;
    @Parameter(value = 1)
    public String responseFileName;

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<>(MainActivity.class, false, false);
    private PaketrixApplication application;
    private MockWebServer server;

    @Parameters(name = "{0}")
    public static Collection<Object[]> initParameters() {
        return AndroidTestData.INSTANCE.getCreateTrackingTestParameters();
    }


    @Before
    public void setUp() throws Exception {
        application = (PaketrixApplication) InstrumentationRegistry
                .getTargetContext().getApplicationContext();
        InstrumentationRegistry.getTargetContext().deleteDatabase(AndroidTestData.getDB_NAME());
        server = new MockWebServer();
        server.start();
        TestAppComponent testAppComponent = DaggerTestAppComponent.builder()
                .application(application)
                .mockWebServer(server).build();
        testAppComponent.inject(application);
    }

    private void launchMainActivity() {
        activityTestRule.launchActivity(new Intent(application, MainActivity.class));
    }

    @Test
    public void inputTrackingNumberShouldGetAndDisplayCorrectTracking() throws Exception {
        String expectedRequestCarrier = tracking.getCarrier().toString().toLowerCase();
        int expectedRequestCount = 1;
        int expectedListSize = 1;
        enqueueOkServerResponseFromFile(server, responseFileName);

        launchMainActivity();
        inputTracking(tracking.getTrackingNumber(), tracking.getCarrier());


        onView(isRoot()).perform(CustomViewAction.waitForText(tracking.getTag(), 10000));

        int actualRequestCount = server.getRequestCount();
        String actualRequestCarrier = server.takeRequest().getRequestUrl().pathSegments().get(0);
        int actualListSize = getRecyclerVIewSize(activityTestRule.getActivity());

        assertEquals("Request count doesn't match", expectedRequestCount, actualRequestCount);
        assertEquals("Wrong carrier request", expectedRequestCarrier, actualRequestCarrier);
        assertEquals(expectedListSize, actualListSize);
        TestUtil.checkIfTrackingVisible(tracking, 0);
    }


    @After
    public void tearDown() throws Exception {
        server.shutdown();
        activityTestRule.getActivity().deleteDatabase(AndroidTestData.getDB_NAME());
        PaketrixApplication application = ((PaketrixApplication) activityTestRule.getActivity().getApplicationContext());
        InstrumentationRegistry.getInstrumentation().callApplicationOnCreate(application);
    }


}
