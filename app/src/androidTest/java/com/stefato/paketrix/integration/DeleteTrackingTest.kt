package com.stefato.paketrix.integration


import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.swipeLeft
import android.support.test.espresso.assertion.ViewAssertions.doesNotExist
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.stefato.paketrix.AndroidTestData
import com.stefato.paketrix.PaketrixApplication
import com.stefato.paketrix.R
import com.stefato.paketrix.data.local.config.AppDatabase
import com.stefato.paketrix.data.local.mapper.TrackingEntityMapper
import com.stefato.paketrix.presentation.MainActivity
import com.stefato.paketrix.testDi.DaggerTestAppComponent
import com.stefato.paketrix.testDi.TestAppComponent
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DeleteTrackingTest {

    @Rule
    @JvmField
    val activityTestRule = ActivityTestRule(MainActivity::class.java, false, false)
    lateinit var application: PaketrixApplication
    lateinit var server: MockWebServer
    lateinit var testAppComponent: TestAppComponent
    lateinit var database: AppDatabase

    @Before
    @Throws(Exception::class)
    fun setUp() {
        application = InstrumentationRegistry
                .getTargetContext().applicationContext as PaketrixApplication
        InstrumentationRegistry.getTargetContext().deleteDatabase(AndroidTestData.DB_NAME)
        server = MockWebServer()
        server.start()
        testAppComponent = DaggerTestAppComponent.builder().application(application).mockWebServer(server).build()
        testAppComponent.inject(application)
        database = testAppComponent.database()
    }

    private fun launchMainActivity() {
        activityTestRule.launchActivity(Intent(application, MainActivity::class.java))
    }

    @Test
    @Throws(Exception::class)
    fun swipeLeftShouldDeleteTracking() {
        val firstTracking = AndroidTestData.dhlTestParcel
        database.parcelDao().insert(TrackingEntityMapper.toTrackingEntity(firstTracking))

        launchMainActivity()

        onView(withId(R.id.tracking_rv)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, swipeLeft()))


        onView(withText(firstTracking.trackingNumber)).check(doesNotExist())
        assertTrue(database.parcelDao().dbIsEmpty())
    }

    @Test
    @Throws(Exception::class)
    fun undoAfterSwipeLeftShouldRevertDeletion() {
        var expectedTracking = AndroidTestData.dhlTestParcel
        expectedTracking = expectedTracking.copy(id = 1L)
        database.parcelDao().insert(TrackingEntityMapper.toTrackingEntity(expectedTracking))

        launchMainActivity()

        onView(withId(R.id.tracking_rv)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, swipeLeft()))


        onView(withId(android.support.design.R.id.snackbar_action))
                .perform(click())

        onView(withText(expectedTracking.trackingNumber)).check(matches(isDisplayed()))

        val actualTracking = TrackingEntityMapper.toTracking(database.parcelDao().all.blockingFirst()[0])
        assertEquals(expectedTracking, actualTracking)
    }

    @Test
    @Throws(Exception::class)
    fun undoShouldOnlyUndeleteLastDeletion() {
        val firstTracking = AndroidTestData.dhlTestParcel
        var expectedTracking = AndroidTestData.dpdTestParcel
        expectedTracking = expectedTracking.copy(id = 2L)
        database.parcelDao().insert(TrackingEntityMapper.toTrackingEntity(firstTracking))
        database.parcelDao().insert(TrackingEntityMapper.toTrackingEntity(expectedTracking))


        launchMainActivity()

        onView(withId(R.id.tracking_rv)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, swipeLeft()))
        onView(withId(R.id.tracking_rv)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, swipeLeft()))

        onView(withId(android.support.design.R.id.snackbar_action)).perform(click())


        onView(withText(expectedTracking.trackingNumber)).check(matches(isDisplayed()))
        val actualTracking = TrackingEntityMapper.toTracking(database.parcelDao().all.blockingFirst()[0])
        assertEquals(expectedTracking, actualTracking)
    }
}
