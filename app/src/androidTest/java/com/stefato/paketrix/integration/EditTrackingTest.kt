package com.stefato.paketrix.integration

import android.content.Intent
import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.RootMatchers.isDialog
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.stefato.paketrix.AndroidTestData
import com.stefato.paketrix.PaketrixApplication
import com.stefato.paketrix.R
import com.stefato.paketrix.data.local.config.AppDatabase
import com.stefato.paketrix.data.local.mapper.TrackingEntityMapper
import com.stefato.paketrix.presentation.MainActivity
import com.stefato.paketrix.testDi.DaggerTestAppComponent
import com.stefato.paketrix.testDi.TestAppComponent
import okhttp3.mockwebserver.MockWebServer
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class EditTrackingTest {

    @Rule
    @JvmField
    val activityTestRule = ActivityTestRule(MainActivity::class.java, false, false)
    lateinit var application: PaketrixApplication
    lateinit var server: MockWebServer
    lateinit var testAppComponent: TestAppComponent
    lateinit var database: AppDatabase

    @Before
    @Throws(Exception::class)
    fun setUp() {
        application = InstrumentationRegistry
                .getTargetContext().applicationContext as PaketrixApplication
        InstrumentationRegistry.getTargetContext().deleteDatabase(AndroidTestData.DB_NAME)

        server = MockWebServer()
        server.start()
        testAppComponent = DaggerTestAppComponent.builder().application(application).mockWebServer(server).build()
        testAppComponent.inject(application)
        database = testAppComponent.database()
    }

    private fun launchMainActivity() {
        activityTestRule.launchActivity(Intent(application, MainActivity::class.java))
    }

    @Test
    @Throws(Exception::class)
    fun swipeRightShouldEditTracking() {
        val firstTracking = AndroidTestData.dhlTestParcel.copy(id = 1L)
        val expectedTracking = firstTracking.copy(name = "abc")
        database.parcelDao().insert(TrackingEntityMapper.toTrackingEntity(firstTracking))

        launchMainActivity()

        onView(withId(R.id.tracking_rv)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, swipeRight()))


        onView(withHint("Name")).inRoot(isDialog()).perform(typeText("abc"))
        onView(withId(R.id.md_buttonDefaultPositive)).inRoot(isDialog()).perform(click())


        onView(withText("abc")).check(matches(isDisplayed()))
        val actualTracking = TrackingEntityMapper.toTracking(database.parcelDao().all.blockingFirst()[0])
        assertEquals(expectedTracking, actualTracking)
    }

    @Test
    @Throws(Exception::class)
    fun swipeRightAndInputNothingShouldDoNothing() {
        val expectedTracking = AndroidTestData.dhlTestParcel.copy(id = 1L)
        database.parcelDao().insert(TrackingEntityMapper.toTrackingEntity(expectedTracking))

        launchMainActivity()

        onView(withId(R.id.tracking_rv)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, swipeRight()))


        onView(withId(R.id.md_buttonDefaultPositive)).inRoot(isDialog()).perform(click())


        onView(withText(expectedTracking.trackingNumber)).check(matches(isDisplayed()))
        val actualTracking = TrackingEntityMapper.toTracking(database.parcelDao().all.blockingFirst()[0])
        assertEquals(expectedTracking, actualTracking)
    }

    @Test
    @Throws(Exception::class)
    fun swipeRightAndDeleteInputShouldResetNameToTrackingNr() {
        val firstTracking = AndroidTestData.dhlTestParcel.copy(id = 1L, name = "abc")
        val expectedTracking = firstTracking.copy(name = "")
        database.parcelDao().insert(TrackingEntityMapper.toTrackingEntity(firstTracking))

        launchMainActivity()

        onView(withId(R.id.tracking_rv)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, swipeRight()))


        onView(withHint("Name")).inRoot(isDialog()).perform(clearText())
        onView(withText(R.string.dialog_action_ok)).inRoot(isDialog()).perform(click())


        onView(withText(expectedTracking.trackingNumber)).check(matches(isDisplayed()))
        val actualTracking = TrackingEntityMapper.toTracking(database.parcelDao().all.blockingFirst()[0])
        assertEquals(expectedTracking, actualTracking)
    }
}
