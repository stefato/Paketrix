package com.stefato.paketrix.integration;


import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.contrib.DrawerActions;
import android.support.test.espresso.contrib.NavigationViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.widget.AppCompatTextView;

import com.stefato.paketrix.AndroidTestData;
import com.stefato.paketrix.PaketrixApplication;
import com.stefato.paketrix.R;
import com.stefato.paketrix.data.local.config.AppDatabase;
import com.stefato.paketrix.presentation.MainActivity;
import com.stefato.paketrix.testDi.DaggerTestAppComponent;
import com.stefato.paketrix.testDi.TestAppComponent;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.AllOf.allOf;

public class NavigationTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<>(MainActivity.class, false, false);
    private PaketrixApplication application;
    private MockWebServer server;
    private TestAppComponent testAppComponent;
    private AppDatabase database;

    @Before
    public void setUp() throws Exception {
        application = (PaketrixApplication) InstrumentationRegistry
                .getTargetContext().getApplicationContext();
        InstrumentationRegistry.getTargetContext().deleteDatabase(AndroidTestData.getDB_NAME());

        server = new MockWebServer();
        server.start();
        testAppComponent = DaggerTestAppComponent.builder().application(application).mockWebServer(server).build();
        testAppComponent.inject(application);
        database = testAppComponent.database();
    }

    private void launchMainActivity() {
        activityTestRule.launchActivity(new Intent(application, MainActivity.class));
    }

    @Test
    public void navigateToHomeShouldDoNothingIfAtHome() throws Exception {
        launchMainActivity();

        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.nvView)).perform(NavigationViewActions.navigateTo(R.id.nav_home));
    }

    @Test
    public void navigateToSettingsShouldDisplaySettings() throws Exception {
        launchMainActivity();

        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.nvView)).perform(NavigationViewActions.navigateTo(R.id.nav_settings));

        onView(allOf(instanceOf(AppCompatTextView.class), withParent(withId(R.id.toolbar))))
                .check(matches(withText(R.string.settings)));
        onView(withText("Trackingupdates")).check(matches(isDisplayed()));
    }

    @Test
    public void navigateToAboutShouldDisplayAbout() throws Exception {
        launchMainActivity();

        onView(withId(R.id.drawer_layout)).perform(DrawerActions.open());
        onView(withId(R.id.nvView)).perform(NavigationViewActions.navigateTo(R.id.nav_about));

        onView(allOf(instanceOf(AppCompatTextView.class), withParent(withId(R.id.toolbar))))
                .check(matches(withText(R.string.about)));
        onView(withId(R.id.aboutName)).check(matches(withText(R.string.app_name)));
    }
}
