package com.stefato.paketrix.integration;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.Until;

import com.stefato.paketrix.AndroidTestData;
import com.stefato.paketrix.PaketrixApplication;
import com.stefato.paketrix.R;
import com.stefato.paketrix.TestUtil;
import com.stefato.paketrix.data.local.config.AppDatabase;
import com.stefato.paketrix.data.local.mapper.TrackingEntityMapper;
import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.presentation.MainActivity;
import com.stefato.paketrix.presentation.jobs.PeriodicRefreshJob;
import com.stefato.paketrix.testDi.DaggerTestAppComponent;
import com.stefato.paketrix.testDi.TestAppComponent;
import com.stefato.paketrix.testUtil.CustomViewAction;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import okhttp3.mockwebserver.MockWebServer;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.uiautomator.By.pkg;
import static com.stefato.paketrix.TestUtil.enqueueOkServerResponseFromFile;
import static org.junit.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class RefreshTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule =
            new ActivityTestRule<>(MainActivity.class, false, false);
    private PaketrixApplication application;
    private MockWebServer server;
    private TestAppComponent testAppComponent;
    private AppDatabase database;

    @Before
    public void setUp() throws Exception {
        application = (PaketrixApplication) InstrumentationRegistry
                .getTargetContext().getApplicationContext();
        InstrumentationRegistry.getTargetContext().deleteDatabase(AndroidTestData.getDB_NAME());

        server = new MockWebServer();
        server.start();
        testAppComponent = DaggerTestAppComponent.builder().application(application).mockWebServer(server).build();
        testAppComponent.inject(application);
        database = testAppComponent.database();
    }

    private void launchMainActivity() {
        activityTestRule.launchActivity(new Intent(application, MainActivity.class));
    }

    @Test
    public void swipeDownShouldDoNothingIfListIsEmpty() throws Exception {
        launchMainActivity();
        onView(withId(R.id.refresh_layout)).perform(swipeDown());
    }

    //TODO fix
    @Test
    public void OnlyUpdatesShouldRefreshOnSwipeDown() throws Exception {
        Tracking firstTracking = AndroidTestData.getDhlTestParcel();
        Tracking secondTracking = AndroidTestData.getDpdTestParcel();
        Tracking refreshedTracking = AndroidTestData.getRefreshDhlTestParcel();
        database.parcelDao().insert(TrackingEntityMapper.toTrackingEntity(firstTracking));
        database.parcelDao().insert(TrackingEntityMapper.toTrackingEntity(secondTracking));


        launchMainActivity();

        enqueueOkServerResponseFromFile(server, AndroidTestData.getDHL_RESPONSE_REFRESH_HTML());
        enqueueOkServerResponseFromFile(server, AndroidTestData.getDPD_RESPONSE_JSON());


        onView(withId(R.id.refresh_layout)).perform(swipeDown());

        onView(isRoot()).perform(CustomViewAction.waitForText(refreshedTracking.getTag(), 10000));

        TestUtil.checkIfTrackingVisible(refreshedTracking, 0);
    }

    @Test
    public void trackingShouldRefreshOnSwipeDown() throws Exception {
        Tracking tracking = AndroidTestData.getDhlTestParcel();
        Tracking refreshedTracking = AndroidTestData.getRefreshDhlTestParcel();
        database.parcelDao().insert(TrackingEntityMapper.toTrackingEntity(tracking));

        launchMainActivity();

        enqueueOkServerResponseFromFile(server, AndroidTestData.getDHL_RESPONSE_REFRESH_HTML());
        onView(withId(R.id.refresh_layout)).perform(swipeDown());
        onView(isRoot()).perform(CustomViewAction.waitForText(refreshedTracking.getTag(), 10000));

        TestUtil.checkIfTrackingVisible(refreshedTracking, 0);
    }

    @Test
    public void testRefreshWithJob() throws Exception {

        PeriodicRefreshJob testJob = testAppComponent.refreshJob();
        Tracking tracking = AndroidTestData.getDhlTestParcel();
        Tracking refreshedTracking = AndroidTestData.getRefreshDhlTestParcel();
        database.parcelDao().insert(TrackingEntityMapper.toTrackingEntity(tracking));

        launchMainActivity();
        TestUtil.checkIfTrackingVisible(tracking.getTrackingNumber(), tracking.getCarrier(),
                tracking.getWaypoints().get(0), 0);

        enqueueOkServerResponseFromFile(server, AndroidTestData.getDHL_RESPONSE_REFRESH_HTML());
        testJob.executeJob(InstrumentationRegistry.getTargetContext());

        UiDevice device = UiDevice.getInstance(InstrumentationRegistry
                .getInstrumentation());

        onView(isRoot()).perform(CustomViewAction.waitForText(refreshedTracking.getTag(), 10000));


        device.openNotification();
        device.wait(Until.hasObject(pkg("com.android.systemui")), 10000);


        UiObject2 notification = device.findObject(By.text(application.getResources().getString(R.string.notification_content_title)));
        UiObject2 text = device.findObject(By.text(refreshedTracking.getTag()));
        UiObject2 text2 = device.findObject(By.textContains(tracking.getTrackingNumber()));


        assertNotNull(notification);
        assertNotNull(text);
        assertNotNull(text2);

        notification.click();

        onView(isRoot()).perform(CustomViewAction.waitForText(refreshedTracking.getTag(), 10000));


        TestUtil.checkIfTrackingVisible(refreshedTracking, 0);
    }
}
