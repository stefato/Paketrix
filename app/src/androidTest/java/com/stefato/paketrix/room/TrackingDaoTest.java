package com.stefato.paketrix.room;

import android.arch.persistence.room.Room;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.stefato.paketrix.AndroidTestData;
import com.stefato.paketrix.data.local.config.AppDatabase;
import com.stefato.paketrix.data.local.entities.TrackingEntity;
import com.stefato.paketrix.domain.model.Carrier;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class TrackingDaoTest {

    private AppDatabase db;

    @Before
    public void setUp() throws Exception {
        db = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), AppDatabase.class)
                .build();
    }

    @After
    public void tearDown() throws Exception {
        db.close();
    }

    @Test
    public void insertTrackingSavesData() throws Exception {
        final TrackingEntity expectedParcelEnitity = AndroidTestData.getParcelEntity(Carrier.DHL, 0);
        db.parcelDao().insert(expectedParcelEnitity);

        List<TrackingEntity> actualList=db.parcelDao().getAll().blockingFirst();
        TrackingEntity actualTrackingEntity =actualList.get(0);

        //check if flowable returned only one enitity
        assertEquals(1,actualList.size());
        assertEquals(expectedParcelEnitity, actualTrackingEntity);
    }

    @Test
    public void dbIsEmptyTest() throws Exception {
        boolean actualDbIsEmpty=db.parcelDao().dbIsEmpty();

        assertTrue(actualDbIsEmpty);
    }

    @Test
    public void dbIsEmptyFalse() throws Exception {
        db.parcelDao().insert(AndroidTestData.getParcelEntity(Carrier.DHL,0));
        boolean actualDbIsEmpty=db.parcelDao().dbIsEmpty();

        assertFalse(actualDbIsEmpty);
    }

    @Test
    public void insertAllTrackingsSavesData() throws Exception {
        final TrackingEntity expectedParcelEnitity = AndroidTestData.getParcelEntity(Carrier.DHL, 0);
        final TrackingEntity expectedParcelEnitity2 = AndroidTestData.getParcelEntity(Carrier.HERMES, 1);

        List<TrackingEntity> parcelEntities=new ArrayList<>(2);
        parcelEntities.add(expectedParcelEnitity);
        parcelEntities.add(expectedParcelEnitity2);

        db.parcelDao().insertAll(parcelEntities);

        List<TrackingEntity> actualParcelEnitites=db.parcelDao().getAll().blockingFirst();

        assertEquals(parcelEntities,actualParcelEnitites);
    }

    @Test
    public void deleteTrackingDeletesData() throws Exception {
        final TrackingEntity expectedParcelEnitity = AndroidTestData.getParcelEntity(Carrier.DHL, 0);

        db.parcelDao().insert(expectedParcelEnitity);

        db.parcelDao().deleteParcel(expectedParcelEnitity);

        assertTrue(db.parcelDao().dbIsEmpty());
    }

    @Test
    public void deleteTrackingsDeletesData() throws Exception {
        final TrackingEntity expectedParcelEnitity = AndroidTestData.getParcelEntity(Carrier.DHL, 0);
        final TrackingEntity expectedParcelEnitity2 = AndroidTestData.getParcelEntity(Carrier.HERMES, 1);

        List<TrackingEntity> parcelEntities=new ArrayList<>(2);
        parcelEntities.add(expectedParcelEnitity);
        parcelEntities.add(expectedParcelEnitity2);

        db.parcelDao().insertAll(parcelEntities);

        db.parcelDao().deleteParcels(parcelEntities);

        assertTrue(db.parcelDao().dbIsEmpty());
    }

    @Test
    public void updateTrackingEditsData() throws Exception {
        final TrackingEntity expectedParcelEnitity =
                AndroidTestData.getParcelEntity(Carrier.HERMES,0);

        db.parcelDao().insert(AndroidTestData.getParcelEntity(Carrier.DHL, 0));

        db.parcelDao().updateParcel(expectedParcelEnitity);

        List<TrackingEntity> actualList=db.parcelDao().getAll().blockingFirst();
        TrackingEntity actualTrackingEntity =actualList.get(0);

        assertEquals(expectedParcelEnitity, actualTrackingEntity);
    }

    @Test
    public void updateTrackingsEditsData() throws Exception {
        final TrackingEntity trackingEntity1 =
                AndroidTestData.getParcelEntity(Carrier.DHL, 0);
        final TrackingEntity trackingEntity2 =
                AndroidTestData.getParcelEntity(Carrier.HERMES, 1);

        final TrackingEntity updatedTrackingEntity1 =
                AndroidTestData.getParcelEntity(Carrier.DHL, 0);
        final TrackingEntity updatedTrackingEntity2 =
                AndroidTestData.getParcelEntity(Carrier.HERMES, 1);

        updatedTrackingEntity1.setName("abc");
        updatedTrackingEntity2.setName("def");

        List<TrackingEntity> trackingEntities=new ArrayList<>(2);
        trackingEntities.add(trackingEntity1);
        trackingEntities.add(trackingEntity2);

        db.parcelDao().insertAll(trackingEntities);

        trackingEntities.clear();
        trackingEntities.add(updatedTrackingEntity1);
        trackingEntities.add(updatedTrackingEntity2);

        db.parcelDao().updateParcels(trackingEntities);

        List<TrackingEntity> actualList=db.parcelDao().getAll().blockingFirst();

        assertEquals(trackingEntities,actualList);
    }

    @Test
    public void getAllReturnsAllData() throws Exception {
        final TrackingEntity expectedParcelEnitity = AndroidTestData.getParcelEntity(Carrier.DHL, 0);
        final TrackingEntity expectedParcelEnitity2 = AndroidTestData.getParcelEntity(Carrier.HERMES, 1);

        List<TrackingEntity> parcelEntities=new ArrayList<>(2);
        parcelEntities.add(expectedParcelEnitity);
        parcelEntities.add(expectedParcelEnitity2);

        db.parcelDao().insertAll(parcelEntities);
        List<TrackingEntity> actualParcelEnitites=db.parcelDao().getAll().blockingFirst();

        assertEquals(parcelEntities,actualParcelEnitites);
    }
}
