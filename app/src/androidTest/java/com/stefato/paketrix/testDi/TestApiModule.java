package com.stefato.paketrix.testDi;


import com.squareup.moshi.Moshi;
import com.stefato.paketrix.data.remote.api.ApiModule;
import com.stefato.paketrix.data.remote.api.dhl.DhlService;
import com.stefato.paketrix.data.remote.api.dpd.DpdService;
import com.stefato.paketrix.data.remote.api.gls.GlsService;
import com.stefato.paketrix.data.remote.api.hermes.HermesService;
import com.stefato.paketrix.data.remote.api.injection.DhlScrapingRetrofit;
import com.stefato.paketrix.data.remote.api.injection.DpdRetrofit;
import com.stefato.paketrix.data.remote.api.injection.GlsRetrofit;
import com.stefato.paketrix.data.remote.api.injection.HermesRetrofit;
import com.stefato.paketrix.data.remote.api.injection.UpsScrapingRetrofit;
import com.stefato.paketrix.data.remote.api.ups.UpsScrapingService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

@Module
public class TestApiModule extends ApiModule {

    @Provides
    @DpdRetrofit
    Retrofit provideDpdRetrofit(Moshi moshi, OkHttpClient okHttpClient, MockWebServer mockWebServer) {
        return new Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(mockWebServer.url("/dpd/"))
                .client(okHttpClient)
                .build();
    }

    @Provides
    @GlsRetrofit
    Retrofit provideGlsRetrofit(Moshi moshi, OkHttpClient okHttpClient, MockWebServer mockWebServer) {
        return new Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(mockWebServer.url("/gls/"))
                .client(okHttpClient)
                .build();
    }

    @Provides
    @HermesRetrofit
    Retrofit provideHermesRetrofit(Moshi moshi, OkHttpClient okHttpClient, MockWebServer mockWebServer) {
        return new Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(mockWebServer.url("/hermes/"))
                .client(okHttpClient)
                .build();
    }

    @Provides
    @UpsScrapingRetrofit
    Retrofit provideUpsScrapingRetrofit(OkHttpClient okHttpClient, MockWebServer mockWebServer) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(mockWebServer.url("/ups/"))
                .client(okHttpClient)
                .build();
    }

    @Provides
    @DhlScrapingRetrofit
    Retrofit dfprovideDhlScrapingRetrofit(OkHttpClient okHttpClient, MockWebServer mockWebServer) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(mockWebServer.url("/dhl/"))
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    DhlService provideDhlScraping(@DhlScrapingRetrofit Retrofit retrofit) {
        return retrofit.create(DhlService.class);
    }

    @Provides
    @Singleton
    DpdService provideDpd(@DpdRetrofit Retrofit retrofit) {
        return retrofit.create(DpdService.class);
    }

    @Provides
    @Singleton
    GlsService provideGls(@GlsRetrofit Retrofit retrofit) {
        return retrofit.create(GlsService.class);
    }

    @Provides
    @Singleton
    HermesService provideHermes(@HermesRetrofit Retrofit retrofit) {
        return retrofit.create(HermesService.class);
    }

    @Provides
    @Singleton
    UpsScrapingService provideUpsScraping(@UpsScrapingRetrofit Retrofit retrofit) {
        return retrofit.create(UpsScrapingService.class);
    }


}
