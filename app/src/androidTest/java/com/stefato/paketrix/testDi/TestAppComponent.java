package com.stefato.paketrix.testDi;

import android.app.Application;

import com.stefato.paketrix.PaketrixApplication;
import com.stefato.paketrix.data.local.config.AppDatabase;
import com.stefato.paketrix.presentation.jobs.JobsModule;
import com.stefato.paketrix.presentation.jobs.PeriodicRefreshJob;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import okhttp3.mockwebserver.MockWebServer;

@Singleton
@Component(modules = {TestAppModule.class, JobsModule.class})
public interface TestAppComponent extends AndroidInjector<PaketrixApplication> {
    PeriodicRefreshJob refreshJob();
    AppDatabase database();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        @BindsInstance
        Builder mockWebServer(MockWebServer mockWebServer);

        TestAppComponent build();
    }


}
