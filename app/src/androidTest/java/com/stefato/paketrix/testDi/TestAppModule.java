package com.stefato.paketrix.testDi;


import com.stefato.paketrix.presentation.injection.PerActivity;
import com.stefato.paketrix.presentation.MainActivity;
import com.stefato.paketrix.presentation.MainActivityModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module(includes = TestDataModule.class)
public abstract class TestAppModule {


    @PerActivity
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity bindMainActivity();
}
