package com.stefato.paketrix.testDi;

import com.squareup.moshi.Moshi;
import com.stefato.paketrix.data.TrackingDataRepository;
import com.stefato.paketrix.data.local.DbModule;
import com.stefato.paketrix.domain.repository.TrackingRepository;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module(includes = {TestRemoteModule.class, DbModule.class})
public abstract class TestDataModule {
    @Provides
    @Singleton
    static Moshi provideMoshi() {
        return new Moshi.Builder().build();
    }

    @Binds
    abstract TrackingRepository bindTrackingRepository(TrackingDataRepository trackingDataRepository);
}