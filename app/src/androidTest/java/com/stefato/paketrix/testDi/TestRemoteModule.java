package com.stefato.paketrix.testDi;

import com.stefato.paketrix.data.remote.net.NetModule;

import dagger.Module;


@Module(includes = {NetModule.class, TestApiModule.class})
public class TestRemoteModule {
}
