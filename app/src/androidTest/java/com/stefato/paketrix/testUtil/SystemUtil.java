package com.stefato.paketrix.testUtil;


import android.app.Instrumentation;
import android.os.ParcelFileDescriptor;

import java.io.FileInputStream;
import java.io.IOException;

public class SystemUtil {

    /**
     * Executes a shell command using shell user identity, and return the standard output in string
     * <p>Note: calling this function requires API level 21 or above
     *
     * @param instrumentation {@link Instrumentation} instance, obtained from a test running in
     *                        instrumentation framework
     * @param cmd             the command to run
     * @return the standard output of the command
     * @throws Exception
     */
    public static String runShellCommand(Instrumentation instrumentation, String cmd)
            throws IOException {
        ParcelFileDescriptor pfd = instrumentation.getUiAutomation().executeShellCommand(cmd);
        byte[] buf = new byte[512];
        int bytesRead;
        FileInputStream fis = new ParcelFileDescriptor.AutoCloseInputStream(pfd);
        StringBuilder stdout = new StringBuilder();
        while ((bytesRead = fis.read(buf)) != -1) {
            stdout.append(new String(buf, 0, bytesRead));
        }
        fis.close();
        return stdout.toString();
    }
}
