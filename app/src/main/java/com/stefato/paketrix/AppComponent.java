package com.stefato.paketrix;


import com.stefato.paketrix.presentation.jobs.JobsModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjector;

@Singleton
@Component(modules = {AppModule.class, JobsModule.class})
interface AppComponent extends AndroidInjector<PaketrixApplication> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<PaketrixApplication> {
    }

}
