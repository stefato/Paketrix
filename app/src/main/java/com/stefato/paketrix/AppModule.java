package com.stefato.paketrix;

import android.app.Application;

import com.stefato.paketrix.data.DataModule;
import com.stefato.paketrix.presentation.injection.PerActivity;
import com.stefato.paketrix.presentation.MainActivity;
import com.stefato.paketrix.presentation.MainActivityModule;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module(includes = {DataModule.class})
public abstract class AppModule {

    @Binds
    @Singleton
    abstract Application application(PaketrixApplication app);


    @PerActivity
    @ContributesAndroidInjector(modules = MainActivityModule.class)
    abstract MainActivity bindMainActivity();

}
