package com.stefato.paketrix;

import android.app.Activity;
import android.app.Application;
import android.support.v7.preference.PreferenceManager;

import com.akaita.java.rxjava2debug.RxJava2Debug;
import com.evernote.android.job.JobManager;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;
import com.stefato.paketrix.presentation.jobs.BackgroundJobCreator;
import com.stefato.paketrix.presentation.util.CrashlyticsTree;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import timber.log.Timber;

public class PaketrixApplication extends Application implements HasActivityInjector {


    @Inject
    DispatchingAndroidInjector<Activity> activityInjector;

    @Inject
    BackgroundJobCreator jobCreator;

    @Override
    public void onCreate() {
        super.onCreate();

        //better RxJava stacktrace
        RxJava2Debug.enableRxJava2AssemblyTracking(new String[]{this.getPackageName()});

        setupLeakCanary();
        setupTimber();
        AndroidThreeTen.init(this);

        DaggerAppComponent.builder().create(this).inject(this);
        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        JobManager.create(this).addJobCreator(jobCreator);
    }

    private void setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        } else {
            Timber.plant(new CrashlyticsTree());
        }
    }

    protected RefWatcher setupLeakCanary() {
        if (LeakCanary.isInAnalyzerProcess(this)) {
            return RefWatcher.DISABLED;
        }
        return LeakCanary.install(this);
    }

    @Override
    public AndroidInjector<Activity> activityInjector() {
        return activityInjector;
    }
}

