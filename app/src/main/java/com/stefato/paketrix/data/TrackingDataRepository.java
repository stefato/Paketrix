package com.stefato.paketrix.data;


import com.stefato.paketrix.data.local.DbRepository;
import com.stefato.paketrix.data.remote.RemoteRepository;
import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.domain.repository.TrackingRepository;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class TrackingDataRepository implements TrackingRepository {

    private final RemoteRepository remoteRepository;

    private final DbRepository dbRepository;

    @Inject
    public TrackingDataRepository(RemoteRepository remoteRepository, DbRepository dbRepository) {
        this.remoteRepository = remoteRepository;
        this.dbRepository = dbRepository;
    }

    @Override
    public Observable<Tracking> getRemoteTracking(Tracking tracking) {
        return remoteRepository.getTracking(tracking);
    }

    @Override
    public Observable<List<Tracking>> getTrackings() {
        return dbRepository.getTrackings();
    }

    public Observable<List<Tracking>> getRemoteTrackings(List<Tracking> trackings) {
        return remoteRepository.getTrackings(trackings);
    }

    @Override
    public Observable<Boolean> deleteTracking(Tracking tracking) {
        return dbRepository.deleteTracking(tracking);
    }

    @Override
    public Observable<Boolean> updateTracking(Tracking tracking) {
        return dbRepository.updateTracking(tracking);
    }

    @Override
    public Observable<Boolean> updateTrackings(List<Tracking> trackings) {
        return dbRepository.updateTrackings(trackings);
    }

    @Override
    public Observable<Long> insertTracking(Tracking tracking) {
        return dbRepository.insertTracking(tracking);
    }

    @Override
    public Observable<List<Tracking>> getTrackingsFlowable() {
        return dbRepository.getTrackingsFlowable().toObservable();
    }

    @Override
    public Observable<Tracking> getLastInsertedTracking() {
        return dbRepository.getLastInsertedTracking();
    }
}
