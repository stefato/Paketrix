package com.stefato.paketrix.data

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.lang.reflect.Type

inline fun <reified T> Class<T>.listType(): Type {
    return Types.newParameterizedType(List::class.java, this)
}

inline fun <reified T> Class<T>.createListAdapter(moshi: Moshi): JsonAdapter<List<T>> {
    val type = this.listType()
    return moshi.adapter(type)
}