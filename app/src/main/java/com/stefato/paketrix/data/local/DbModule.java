package com.stefato.paketrix.data.local;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.stefato.paketrix.data.local.config.AppDatabase;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class DbModule {

    private static final String DB_NAME = "tracking-db";

    @Provides
    @Singleton
    AppDatabase provideRoomDatabase(Application application) {
        //TODO migration
        return Room.databaseBuilder(application,
                AppDatabase.class, DB_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }
}
