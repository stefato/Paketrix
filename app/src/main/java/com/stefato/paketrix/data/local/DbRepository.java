package com.stefato.paketrix.data.local;


import com.stefato.paketrix.data.local.config.AppDatabase;
import com.stefato.paketrix.data.local.dao.TrackingDao;
import com.stefato.paketrix.data.local.mapper.TrackingEntityMapper;
import com.stefato.paketrix.domain.model.Tracking;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class DbRepository {

    private final TrackingDao trackingDao;

    @Inject
    DbRepository(AppDatabase database) {
        this.trackingDao = database.parcelDao();
    }

    public Flowable<List<Tracking>> getTrackingsFlowable() {
        return trackingDao.getAll().map(TrackingEntityMapper::toTrackingList);
    }

    public Observable<List<Tracking>> getTrackings() {
        return trackingDao.getAll().map(TrackingEntityMapper::toTrackingList).take(1)
                .toObservable();
    }

    public Observable<Long> insertTracking(Tracking tracking) {
        return Observable
                .fromCallable(() -> trackingDao.insert(TrackingEntityMapper
                        .toTrackingEntity(tracking)))
                .subscribeOn(Schedulers.io());
    }

    public Observable<Boolean> deleteTracking(Tracking tracking) {
        return Observable.fromCallable(() -> {
            trackingDao.deleteParcel(TrackingEntityMapper.toTrackingEntity(tracking));
            return Boolean.TRUE;
        }).subscribeOn(Schedulers.io());
    }


    public Observable<Boolean> updateTrackings(List<Tracking> trackings) {
        return Observable
                .fromCallable(() -> {
                    trackingDao.updateParcels(TrackingEntityMapper.toTrackingEntityList(trackings));
                    return Boolean.FALSE;
                })
                .subscribeOn(Schedulers.io());
    }

    public Observable<Boolean> updateTracking(Tracking tracking) {
        return Observable
                .fromCallable(() -> {
                    trackingDao.updateParcel(TrackingEntityMapper.toTrackingEntity(tracking));
                    return Boolean.FALSE;
                })
                .subscribeOn(Schedulers.io());
    }

    public Observable<Boolean> isTrackingTableEmpty() {
        return Observable.just(trackingDao.dbIsEmpty());
    }

    public Observable<Tracking> getLastInsertedTracking() {
        return Observable.fromCallable(trackingDao::getLastInsertedTracking)
                .flatMap(lastInserted -> {
                    if (lastInserted != null)
                        return Observable.just(lastInserted)
                                .map(TrackingEntityMapper::toTracking);
                    else
                        return Observable.just(new Tracking());
                })
                .subscribeOn(Schedulers.io());
    }
}
