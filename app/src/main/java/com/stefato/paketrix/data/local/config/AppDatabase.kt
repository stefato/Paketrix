package com.stefato.paketrix.data.local.config

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters

import com.stefato.paketrix.data.local.converter.DateTimeConverter
import com.stefato.paketrix.data.local.converter.WaypointConverter
import com.stefato.paketrix.data.local.dao.TrackingDao
import com.stefato.paketrix.data.local.entities.TrackingEntity


@Database(entities = [TrackingEntity::class], version = 1, exportSchema = false)
@TypeConverters(WaypointConverter::class, DateTimeConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun parcelDao(): TrackingDao
}
