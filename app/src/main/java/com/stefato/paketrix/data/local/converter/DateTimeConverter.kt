package com.stefato.paketrix.data.local.converter


import android.arch.persistence.room.TypeConverter
import com.stefato.paketrix.utils.toLocalDateTime

import org.threeten.bp.LocalDateTime

object DateTimeConverter {

    @TypeConverter
    @JvmStatic
    fun toDateTimeString(dateTime: LocalDateTime): String {
        return dateTime.toString()
    }

    @TypeConverter
    @JvmStatic
    fun toLocalDateTime(dateTimeString: String): LocalDateTime {
        return dateTimeString.toLocalDateTime()
    }
}
