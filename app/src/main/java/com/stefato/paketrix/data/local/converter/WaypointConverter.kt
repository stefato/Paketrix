package com.stefato.paketrix.data.local.converter

import android.arch.persistence.room.TypeConverter
import com.squareup.moshi.Moshi
import com.stefato.paketrix.data.local.entities.WaypointEntity
import com.stefato.paketrix.data.createListAdapter

object WaypointConverter {

    private val moshi = Moshi.Builder().build()
    private val stringListAdapter =
            String::class.java.createListAdapter(moshi)
    private val waypointEntityAdapter =
            WaypointEntity::class.java.createListAdapter(moshi)

    @TypeConverter
    @JvmStatic
    fun fromStringList(stringList: List<String>): String {
        return stringListAdapter.toJson(stringList)
    }

    @TypeConverter
    @JvmStatic
    fun fromString(json: String): List<String>? {
        return stringListAdapter.fromJson(json)
    }

    @TypeConverter
    @JvmStatic
    fun fromCheckpoint(waypoints: List<WaypointEntity>): String {
        return waypointEntityAdapter.toJson(waypoints)
    }

    @TypeConverter
    @JvmStatic
    fun waypointFromString(json: String): List<WaypointEntity>? {
        return waypointEntityAdapter.fromJson(json)
    }

}
