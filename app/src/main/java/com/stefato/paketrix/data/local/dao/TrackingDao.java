package com.stefato.paketrix.data.local.dao;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.stefato.paketrix.data.local.entities.TrackingEntity;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface TrackingDao {

    @Query("SELECT * FROM parcel")
    Flowable<List<TrackingEntity>> getAll();

    @Insert
    List<Long> insertAll(List<TrackingEntity> trackingEntities);

    @Insert
    Long insert(TrackingEntity trackingEntity);

    @Delete
    void deleteParcels(List<TrackingEntity> trackingEntities);

    @Update
    void updateParcels(List<TrackingEntity> trackingEntities);

    @Update
    void updateParcel(TrackingEntity trackingEntity);

    @Query("SELECT NOT(exists(SELECT 1 FROM parcel))")
    boolean dbIsEmpty();

    @Delete
    void deleteParcel(TrackingEntity trackingEntity);

    @Query("SELECT * FROM parcel WHERE ID = (SELECT MAX(ID)  FROM parcel)")
    TrackingEntity getLastInsertedTracking();
}
