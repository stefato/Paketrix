package com.stefato.paketrix.data.local.entities


import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "parcel")
data class TrackingEntity(
        @PrimaryKey(autoGenerate = true) var id: Long? = null,
        @ColumnInfo(name = "tracking_number") var trackingNumber: String? = null,
        var carrier: Int,
        var tag: String?,
        var name: String?,
        //room can't handle relationships automatically
        //use TypeConverter or @ForeignKey and a query as workaround
        var waypoints: List<WaypointEntity>?
)
