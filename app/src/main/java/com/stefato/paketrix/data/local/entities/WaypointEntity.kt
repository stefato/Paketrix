package com.stefato.paketrix.data.local.entities


import org.threeten.bp.LocalDateTime

//AutoValue room entity would be nice
//https://issuetracker.google.com/issues/62408420
data class WaypointEntity(
        val city: String?,
        val message: String?,
        val dateTime: LocalDateTime?
)
