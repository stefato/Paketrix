package com.stefato.paketrix.data.local.mapper


import com.stefato.paketrix.data.local.entities.TrackingEntity
import com.stefato.paketrix.data.local.entities.WaypointEntity
import com.stefato.paketrix.domain.model.Carrier
import com.stefato.paketrix.domain.model.Tracking
import com.stefato.paketrix.domain.model.Waypoint
import org.threeten.bp.LocalDateTime
import java.util.*

object TrackingEntityMapper {

    @JvmStatic
    fun toTrackingList(trackingEntities: List<TrackingEntity>): List<Tracking> {
        val trackings = ArrayList<Tracking>(trackingEntities.size)

        for (trackingEntity in trackingEntities) {
            trackings.add(toTracking(trackingEntity))
        }
        return trackings
    }

    @JvmStatic
    fun toTracking(trackingEntity: TrackingEntity) = Tracking(
            trackingNumber = trackingEntity.trackingNumber.orEmpty(),
            tag = trackingEntity.tag.orEmpty(),
            carrier = Carrier.fromId(trackingEntity.carrier),
            waypoints = toWaypointList(trackingEntity.waypoints.orEmpty()),
            id = trackingEntity.id,
            name = trackingEntity.name.orEmpty())

    private fun toWaypointList(waypointEntities: List<WaypointEntity>): List<Waypoint> {
        val waypoints = ArrayList<Waypoint>(waypointEntities.size)

        for (waypointEntity in waypointEntities) {
            waypoints.add(toWaypoint(waypointEntity))
        }
        return waypoints
    }

    private fun toWaypoint(waypointEntity: WaypointEntity) = Waypoint(
            city = waypointEntity.city.orEmpty(),
            dateTime = waypointEntity.dateTime ?: LocalDateTime.MIN,
            message = waypointEntity.message.orEmpty()
    )


    @JvmStatic
    fun toTrackingEntity(tracking: Tracking) = TrackingEntity(
            trackingNumber = tracking.trackingNumber,
            tag = tracking.tag,
            carrier = tracking.carrier.id,
            waypoints = toWaypointEntityList(tracking.waypoints),
            id = tracking.id,
            name = tracking.name
    )


    private fun toWaypointEntityList(waypoints: List<Waypoint>?): List<WaypointEntity> {
        val waypointEntities = ArrayList<WaypointEntity>()

        if (waypoints != null) {
            for (waypoint in waypoints) {
                waypointEntities.add(toWaypointEntity(waypoint))
            }
        }
        return waypointEntities

    }

    private fun toWaypointEntity(waypoint: Waypoint) = WaypointEntity(
            city = waypoint.city,
            dateTime = waypoint.dateTime,
            message = waypoint.message)


    @JvmStatic
    fun toTrackingEntityList(trackings: List<Tracking>): List<TrackingEntity> {
        val trackingEntities = ArrayList<TrackingEntity>(trackings.size)

        for (tracking in trackings) {
            trackingEntities.add(toTrackingEntity(tracking))
        }
        return trackingEntities
    }
}
