package com.stefato.paketrix.data.remote;


import com.stefato.paketrix.data.remote.api.ApiModule;
import com.stefato.paketrix.data.remote.net.NetModule;

import dagger.Module;

@Module(includes = {NetModule.class, ApiModule.class})
public class RemoteModule {
}
