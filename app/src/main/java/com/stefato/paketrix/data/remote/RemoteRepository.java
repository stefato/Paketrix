package com.stefato.paketrix.data.remote;


import com.stefato.paketrix.data.remote.net.NetUtil;
import com.stefato.paketrix.domain.model.Tracking;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;

@Singleton
public class RemoteRepository {

    private final NetUtil netUtil;
    private final TrackingRequester requester;

    @Inject
    RemoteRepository(TrackingRequester requester, NetUtil netUtil) {
        this.requester = requester;
        this.netUtil = netUtil;
    }

    public Observable<List<Tracking>> getTrackings(List<Tracking> trackings) {
        return Observable.just(trackings)
                .flatMapIterable(list -> list)
                .flatMap(this::getTracking)
                .toList()
                .toObservable();
    }

    public Observable<Tracking> getTracking(Tracking tracking) {
        return requester.request(tracking).compose(netUtil.networkCheck());
    }
}
