package com.stefato.paketrix.data.remote

import com.stefato.paketrix.data.remote.api.dhl.DhlRepository
import com.stefato.paketrix.data.remote.api.dpd.DpdRepository
import com.stefato.paketrix.data.remote.api.gls.GlsRepository
import com.stefato.paketrix.data.remote.api.hermes.HermesRepository
import com.stefato.paketrix.data.remote.api.ups.UpsRepository
import com.stefato.paketrix.domain.model.Carrier
import com.stefato.paketrix.domain.model.Tracking
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TrackingRequester @Inject constructor(
        private val dpdRepository: DpdRepository,
        private val glsRepository: GlsRepository,
        private val dhlRepository: DhlRepository,
        private val upsRepository: UpsRepository,
        private val hermesRepository: HermesRepository
) {

    fun request(tracking: Tracking) =
            when (tracking.carrier) {
                Carrier.DHL -> dhlRepository.getTracking(tracking).toObservable()
                Carrier.DPD -> dpdRepository.getTracking(tracking)
                Carrier.UPS -> upsRepository.getTracking(tracking).toObservable()
                Carrier.GLS -> glsRepository.getTracking(tracking)
                Carrier.HERMES -> hermesRepository.getTracking(tracking)
                else -> Observable.empty<Tracking>()
            }
}
