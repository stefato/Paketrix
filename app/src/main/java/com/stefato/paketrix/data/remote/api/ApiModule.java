package com.stefato.paketrix.data.remote.api;


import com.squareup.moshi.Moshi;
import com.stefato.paketrix.data.remote.api.dhl.DhlConstantsKt;
import com.stefato.paketrix.data.remote.api.dhl.DhlService;
import com.stefato.paketrix.data.remote.api.dpd.DpdConstantsKt;
import com.stefato.paketrix.data.remote.api.dpd.DpdService;
import com.stefato.paketrix.data.remote.api.gls.GlsConstantsKt;
import com.stefato.paketrix.data.remote.api.gls.GlsService;
import com.stefato.paketrix.data.remote.api.hermes.HermesConstantsKt;
import com.stefato.paketrix.data.remote.api.hermes.HermesService;
import com.stefato.paketrix.data.remote.api.injection.DhlScrapingRetrofit;
import com.stefato.paketrix.data.remote.api.injection.DpdRetrofit;
import com.stefato.paketrix.data.remote.api.injection.GlsRetrofit;
import com.stefato.paketrix.data.remote.api.injection.HermesRetrofit;
import com.stefato.paketrix.data.remote.api.injection.UpsScrapingRetrofit;
import com.stefato.paketrix.data.remote.api.ups.UpsConstantsKt;
import com.stefato.paketrix.data.remote.api.ups.UpsScrapingService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.moshi.MoshiConverterFactory;

@Module
public class ApiModule {

    @Provides
    @Singleton
    DhlService provideDhlScraping(@DhlScrapingRetrofit Retrofit retrofit) {
        return retrofit.create(DhlService.class);
    }

    @Provides
    @Singleton
    DpdService provideDpd(@DpdRetrofit Retrofit retrofit) {
        return retrofit.create(DpdService.class);
    }

    @Provides
    @Singleton
    GlsService provideGls(@GlsRetrofit Retrofit retrofit) {
        return retrofit.create(GlsService.class);
    }

    @Provides
    @Singleton
    HermesService provideHermes(@HermesRetrofit Retrofit retrofit) {
        return retrofit.create(HermesService.class);
    }

    @Provides
    @Singleton
    UpsScrapingService provideUpsScraping(@UpsScrapingRetrofit Retrofit retrofit) {
        return retrofit.create(UpsScrapingService.class);
    }

    @Provides
    @Singleton
    @DhlScrapingRetrofit
    Retrofit provideDhlScrapingRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(DhlConstantsKt.DHL_BASE_URL)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    @DpdRetrofit
    Retrofit provideDpdRetrofit(Moshi moshi, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(DpdConstantsKt.DPD_BASE_URL)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    @GlsRetrofit
    Retrofit provideGlsRetrofit(Moshi moshi, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(GlsConstantsKt.GLS_BASE_URL)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    @HermesRetrofit
    Retrofit provideHermesRetrofit(Moshi moshi, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(HermesConstantsKt.HERMES_BASE_URL)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    @UpsScrapingRetrofit
    Retrofit provideUpsScrapingRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(UpsConstantsKt.UPS_SCRAPING_BASE_URL)
                .client(okHttpClient)
                .build();
    }
}
