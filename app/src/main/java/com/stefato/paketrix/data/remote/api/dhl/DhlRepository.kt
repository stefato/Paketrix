package com.stefato.paketrix.data.remote.api.dhl


import com.stefato.paketrix.domain.model.Tracking
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DhlRepository @Inject
constructor(private val service: DhlService,
            private val mapper: DhlResponseMapper) {

    fun getTracking(tracking: Tracking): Single<Tracking> {
        return service.getTracking(tracking.trackingNumber)
                .map { mapper.map(it,onto = tracking) }
    }

}
