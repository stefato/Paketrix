package com.stefato.paketrix.data.remote.api.dhl


import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.stefato.paketrix.data.remote.api.dhl.response.DhlResponse
import com.stefato.paketrix.data.remote.api.dhl.response.Event
import com.stefato.paketrix.domain.model.Carrier
import com.stefato.paketrix.domain.model.Tracking
import com.stefato.paketrix.domain.model.Waypoint
import com.stefato.paketrix.utils.toLocalDateTime
import okhttp3.ResponseBody
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

private const val SCRIPT_TAG = "script"
private val ENCLOSED_IN_ROUND_BRACKETS = "\\(\"(.*?)\"\\)".toRegex()
private val BACKSLASH = "\\\\".toRegex()

@Singleton
class DhlResponseMapper @Inject
constructor(private val moshi: Moshi) {

    private val dhlAdapter: JsonAdapter<DhlResponse>
            by lazy { moshi.adapter(DhlResponse::class.java) }

    fun map(response: ResponseBody?, onto: Tracking) = response
            ?.toJsonString()?.toDhlResponse().addTo(onto)

    private fun ResponseBody.toJsonString(): String {
        val doc = Jsoup.parse(string())
        val scriptTag = doc.getElementsByTag(SCRIPT_TAG).first() ?: Element("")
        return extractJson(scriptTag.data().orEmpty())
    }

    private fun String.toDhlResponse() = dhlAdapter.fromJson(this)

    private fun DhlResponse?.addTo(tracking: Tracking): Tracking {

        return this?.run {
            val history = sendungen.firstOrNull()?.let { (details, notFound) ->
                notFound?.let { return tracking }
                details.sendungsverlauf
            }

            tracking.copy(
                    trackingNumber = tracking.trackingNumber.toUpperCase(Locale.US),
                    carrier = Carrier.DHL,
                    tag = history?.kurzStatus.orEmpty(),
                    waypoints = history?.events?.mapWaypoints().orEmpty())

        } ?: tracking
    }

    private fun List<Event>.mapWaypoints() =
            reversed().map { (date, status, city) ->
                Waypoint(
                        dateTime = date.toLocalDateTime(),
                        city = city,
                        message = status
                )
            }


    private fun extractJson(string: String): String {
        val jsonString = ENCLOSED_IN_ROUND_BRACKETS.find(string)?.groups?.get(1)?.value
        return jsonString?.replace(BACKSLASH, "").orEmpty()
    }

}



