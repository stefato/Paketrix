package com.stefato.paketrix.data.remote.api.dhl


import io.reactivex.Single
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query

interface DhlService {
    @GET("de/set_identcodes.do")
    fun getTracking(@Query("idc") trackingNumber: String): Single<ResponseBody>
}
