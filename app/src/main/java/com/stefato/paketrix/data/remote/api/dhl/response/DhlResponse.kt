package com.stefato.paketrix.data.remote.api.dhl.response

import com.squareup.moshi.Json

data class DhlResponse(

        @Json(name = "sendungen")
        val sendungen: List<Sendungen> = listOf()
)

