package com.stefato.paketrix.data.remote.api.dhl.response

import com.squareup.moshi.Json

data class Event(

        @Json(name = "datum")
        val datum: String = "",

        @Json(name = "status")
        val status: String = "k. A.",

        @Json(name = "ort")
        val ort: String = ""
)
