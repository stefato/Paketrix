package com.stefato.paketrix.data.remote.api.dhl.response

import com.squareup.moshi.Json

data class SendungNichtGefunden(

        @Json(name = "keineDatenVerfuegbar")
        val keineDatenVerfuegbar: Boolean = false
)
