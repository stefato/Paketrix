package com.stefato.paketrix.data.remote.api.dhl.response

import com.squareup.moshi.Json

data class Sendungen(

        @Json(name = "sendungsdetails")
        val sendungsdetails: Sendungsdetails,

        @Json(name = "sendungNichtGefunden")
        val sendungNichtGefunden: SendungNichtGefunden?
)
