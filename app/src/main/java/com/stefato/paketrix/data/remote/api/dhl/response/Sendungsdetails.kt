package com.stefato.paketrix.data.remote.api.dhl.response

import com.squareup.moshi.Json

class Sendungsdetails(

        @Json(name = "sendungsverlauf")
        val sendungsverlauf: Sendungsverlauf?
)
