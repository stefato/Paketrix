package com.stefato.paketrix.data.remote.api.dhl.response

import com.squareup.moshi.Json

class Sendungsverlauf(

        @Json(name = "kurzStatus")
        val kurzStatus: String?,

        @Json(name = "events")
        val events: List<Event>?
)
