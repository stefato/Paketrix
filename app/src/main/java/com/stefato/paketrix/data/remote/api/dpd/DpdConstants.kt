package com.stefato.paketrix.data.remote.api.dpd

import org.threeten.bp.format.DateTimeFormatter

const val DPD_BASE_URL = "https://tracking.dpd.de/rest/plc/"

val DPD_DATE_TIME_FORMAT = DateTimeFormatter
        .ofPattern("dd.MM.yyyy, HH:mm")
