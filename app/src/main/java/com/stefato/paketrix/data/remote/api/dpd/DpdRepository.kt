package com.stefato.paketrix.data.remote.api.dpd


import com.stefato.paketrix.domain.model.Tracking
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DpdRepository @Inject
internal constructor(
        private val service: DpdService,
        private val mapper: DpdResponseMapper
) {

    fun getTracking(tracking: Tracking): Observable<Tracking> {
        return service.getTracking(tracking.trackingNumber, "de_DE")
                .map { mapper.map(it,onto = tracking) }
    }
}
