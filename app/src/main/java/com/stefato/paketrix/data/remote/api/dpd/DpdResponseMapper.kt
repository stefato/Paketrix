package com.stefato.paketrix.data.remote.api.dpd


import com.stefato.paketrix.data.remote.api.dpd.response.DpdResponse
import com.stefato.paketrix.data.remote.api.dpd.response.StatusInfo
import com.stefato.paketrix.domain.model.Carrier
import com.stefato.paketrix.domain.model.Tracking
import com.stefato.paketrix.domain.model.Waypoint
import com.stefato.paketrix.utils.toLocalDateTime
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class DpdResponseMapper @Inject constructor() {

    fun map(response: DpdResponse?, onto: Tracking) = response.addTo(onto)

    private fun DpdResponse?.addTo(tracking: Tracking) =
            this?.parcellifecycleResponse?.parcelLifeCycleData?.statusInfo?.let {
                val waypoints = it.toWaypoints()

                tracking.copy(
                        trackingNumber = tracking.trackingNumber.toUpperCase(Locale.US),
                        carrier = Carrier.DPD,
                        tag = waypoints.firstOrNull()?.message.orEmpty(),
                        waypoints = waypoints)

            } ?: tracking

    private fun List<StatusInfo>.toWaypoints() = reversed().map {
        it.run {
            Waypoint(
                    dateTime = date.toLocalDateTime(DPD_DATE_TIME_FORMAT),
                    city = location,
                    message = description.content.firstOrNull() ?: label
            )
        }
    }

}



