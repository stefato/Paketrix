package com.stefato.paketrix.data.remote.api.dpd


import com.stefato.paketrix.data.remote.api.dpd.response.DpdResponse

import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface DpdService {

    @GET("{locale}/{trackingNr}")
    fun getTracking(@Path("trackingNr") trackingNumber: String,
                    @Path("locale") locale: String): Observable<DpdResponse>

}
