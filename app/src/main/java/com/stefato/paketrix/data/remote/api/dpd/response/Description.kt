package com.stefato.paketrix.data.remote.api.dpd.response

import com.squareup.moshi.Json

data class Description(

        @Json(name = "content")
        val content: List<String> = listOf()
)
