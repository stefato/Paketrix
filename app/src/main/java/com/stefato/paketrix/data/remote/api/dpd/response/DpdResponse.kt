package com.stefato.paketrix.data.remote.api.dpd.response

import com.squareup.moshi.Json

data class DpdResponse(

        @Json(name = "parcellifecycleResponse")
        val parcellifecycleResponse: ParcellifecycleResponse
)
