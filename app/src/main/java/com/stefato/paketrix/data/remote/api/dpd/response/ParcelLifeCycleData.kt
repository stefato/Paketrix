package com.stefato.paketrix.data.remote.api.dpd.response

import com.squareup.moshi.Json

data class ParcelLifeCycleData(

        @Json(name = "statusInfo")
        val statusInfo: List<StatusInfo>
)
