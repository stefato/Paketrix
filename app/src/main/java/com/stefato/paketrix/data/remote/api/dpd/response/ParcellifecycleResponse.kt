package com.stefato.paketrix.data.remote.api.dpd.response

import com.squareup.moshi.Json

data class ParcellifecycleResponse(

        @Json(name = "parcelLifeCycleData")
        val parcelLifeCycleData: ParcelLifeCycleData?
)
