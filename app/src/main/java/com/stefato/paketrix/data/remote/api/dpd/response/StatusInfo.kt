package com.stefato.paketrix.data.remote.api.dpd.response

import com.squareup.moshi.Json

data class StatusInfo(

        @Json(name = "label")
        val label: String = "",

        @Json(name = "description")
        val description: Description = Description(),

        @Json(name = "location")
        val location: String = "",

        @Json(name = "date")
        val date: String = ""
)
