package com.stefato.paketrix.data.remote.api.gls

import org.threeten.bp.format.DateTimeFormatter

const val GLS_BASE_URL = "https://gls-group.eu/app/service/open/rest/DE/de/"

val GLS_DATE_TIME_FORMAT = DateTimeFormatter
        .ofPattern("yyyy-MM-dd HH:mm:ss")
