package com.stefato.paketrix.data.remote.api.gls

import com.stefato.paketrix.data.remote.api.gls.response.GlsResponse
import com.stefato.paketrix.domain.model.Tracking
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GlsRepository @Inject
internal constructor(
        private val service: GlsService,
        private val mapper: GlsResponseMapper) {


    fun getTracking(tracking: Tracking): Observable<Tracking> {
        return service.getTracking(tracking.trackingNumber)
                .onErrorReturn { GlsResponse.empty() }
                .map { mapper.map(it, onto = tracking) }
    }
}

