package com.stefato.paketrix.data.remote.api.gls

import com.stefato.paketrix.data.remote.api.gls.response.GlsResponse
import com.stefato.paketrix.data.remote.api.gls.response.HistoryItem
import com.stefato.paketrix.domain.model.Carrier
import com.stefato.paketrix.domain.model.Tracking
import com.stefato.paketrix.domain.model.Waypoint
import com.stefato.paketrix.utils.toLocalDateTime
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GlsResponseMapper @Inject constructor() {

    fun map(glsResponse: GlsResponse?, onto: Tracking) = glsResponse.addTo(onto)

    private fun GlsResponse?.addTo(tracking: Tracking) =
            this?.tuStatus?.get(0)?.history?.run {
                val waypoints = toWaypoints()

                tracking.copy(
                        //just ASCII replacement
                        trackingNumber = tracking.trackingNumber.toUpperCase(Locale.US),
                        carrier = Carrier.GLS,
                        tag = waypoints.firstOrNull()?.message.orEmpty(),
                        waypoints = waypoints)

            } ?: tracking


    private fun List<HistoryItem>.toWaypoints() = map { (date, time, address, message) ->
        Waypoint(
                dateTime = "$date $time".toLocalDateTime(GLS_DATE_TIME_FORMAT).withSecond(0),
                city = address?.city.orEmpty(),
                message = message.orEmpty())
    }


}
