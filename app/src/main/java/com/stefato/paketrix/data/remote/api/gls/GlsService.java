package com.stefato.paketrix.data.remote.api.gls;


import com.stefato.paketrix.data.remote.api.gls.response.GlsResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GlsService {

    @GET("rstt001")
    Observable<GlsResponse> getTracking(@Query(value = "match") String trackingNumber);

}
