package com.stefato.paketrix.data.remote.api.gls.response

import com.squareup.moshi.Json

data class Address(

        @Json(name = "city")
        val city: String,

        @Json(name = "countryCode")
        val countryCode: String,

        @Json(name = "countryName")
        val countryName: String
)





