package com.stefato.paketrix.data.remote.api.gls.response

import com.squareup.moshi.Json

data class GlsResponse(

        @Json(name = "tuStatus")
        val tuStatus: List<TuStatusItem>? = null
) {

    companion object {

        @JvmStatic
        fun empty(): GlsResponse {
            return GlsResponse()
        }
    }
}
