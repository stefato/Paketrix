package com.stefato.paketrix.data.remote.api.gls.response

import com.squareup.moshi.Json

data class HistoryItem(

        @Json(name = "date")
        val date: String?,

        @Json(name = "time")
        val time: String?,

        @Json(name = "address")
        val address: Address?,

        @Json(name = "evtDscr")
        val evtDscr: String?
)
