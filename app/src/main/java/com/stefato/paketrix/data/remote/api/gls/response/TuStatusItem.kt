package com.stefato.paketrix.data.remote.api.gls.response

import com.squareup.moshi.Json

data class TuStatusItem(

        @Json(name = "history")
        var history: List<HistoryItem>? = null
)
