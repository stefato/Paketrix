package com.stefato.paketrix.data.remote.api.hermes


import com.stefato.paketrix.data.remote.api.hermes.response.HermesResponse
import com.stefato.paketrix.domain.model.Tracking
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HermesRepository @Inject
constructor(
        private val service: HermesService,
        private val mapper: HermesResponseMapper
) {

    fun getTracking(tracking: Tracking): Observable<Tracking> {
        return service.getTracking(tracking.trackingNumber)
                .map { list -> list[0] }
                .onErrorReturn { HermesResponse.empty() }
                .map { mapper.map(it, onto = tracking) }
    }
}
