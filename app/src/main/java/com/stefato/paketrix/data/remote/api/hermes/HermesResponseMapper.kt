package com.stefato.paketrix.data.remote.api.hermes


import com.stefato.paketrix.data.remote.api.hermes.response.HermesResponse
import com.stefato.paketrix.data.remote.api.hermes.response.StatusHistoryItem
import com.stefato.paketrix.domain.model.Carrier
import com.stefato.paketrix.domain.model.Tracking
import com.stefato.paketrix.domain.model.Waypoint
import com.stefato.paketrix.utils.toLocalDateTime
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HermesResponseMapper @Inject constructor() {

    fun map(hermesResponse: HermesResponse?, onto: Tracking) = hermesResponse.addTo(onto)

    private fun HermesResponse?.addTo(tracking: Tracking) =
            this?.statusHistory?.run {
                tracking.copy(
                        //just ASCII replacement
                        trackingNumber = tracking.trackingNumber.toUpperCase(Locale.US),
                        carrier = Carrier.HERMES,
                        tag = lastStatusMessage.orEmpty(),
                        waypoints = mapWaypoints())
            } ?: tracking


    private fun List<StatusHistoryItem>.mapWaypoints() = map { (date, message) ->
        Waypoint(
                dateTime = date.orEmpty().toLocalDateTime(),
                city = "",
                message = message.orEmpty())
    }


}
