package com.stefato.paketrix.data.remote.api.hermes;


import com.stefato.paketrix.data.remote.api.hermes.response.HermesResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface HermesService {

    @GET("shipments/{trackingNr}")
    Observable<List<HermesResponse>> getTracking(@Path("trackingNr") String trackingNumber);

}

