package com.stefato.paketrix.data.remote.api.hermes.response

import com.squareup.moshi.Json


data class HermesResponse(

        @Json(name = "lastStatusMessage")
        val lastStatusMessage: String? = "",

        @Json(name = "statusHistory")
        val statusHistory: List<StatusHistoryItem>? = null
) {

    companion object {

        @JvmStatic
        fun empty(): HermesResponse {
            return HermesResponse()
        }
    }
}

