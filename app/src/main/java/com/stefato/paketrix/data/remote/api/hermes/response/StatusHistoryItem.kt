package com.stefato.paketrix.data.remote.api.hermes.response

import com.squareup.moshi.Json


data class StatusHistoryItem(

        @Json(name = "date")
        var date: String? = null,

        @Json(name = "message")
        var message: String? = null
)
