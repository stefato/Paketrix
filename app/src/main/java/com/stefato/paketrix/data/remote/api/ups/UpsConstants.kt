package com.stefato.paketrix.data.remote.api.ups

import org.threeten.bp.format.DateTimeFormatter

const val UPS_SCRAPING_BASE_URL = "https://wwwapps.ups.com/WebTracking/"

val UPS_FORMATTER = DateTimeFormatter
        .ofPattern("dd.MM.yyyy H:mm")
