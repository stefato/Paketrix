package com.stefato.paketrix.data.remote.api.ups

import com.stefato.paketrix.domain.model.Tracking
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UpsRepository @Inject
constructor(
        private val service: UpsScrapingService,
        private val mapper: UpsResponseMapper) {

    fun getTracking(tracking: Tracking): Single<Tracking> {
        return service.getTracking(tracking.trackingNumber, "de_DE")
                .map {mapper.map(it,onto = tracking)}
    }
}
