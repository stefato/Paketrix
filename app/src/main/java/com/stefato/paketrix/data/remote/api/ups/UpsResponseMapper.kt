package com.stefato.paketrix.data.remote.api.ups


import com.stefato.paketrix.domain.model.Carrier
import com.stefato.paketrix.domain.model.Tracking
import com.stefato.paketrix.domain.model.Waypoint
import com.stefato.paketrix.utils.toLocalDateTime
import okhttp3.ResponseBody
import org.jsoup.Jsoup
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

private const val waypointsCssQuery = "#fontControl>div.appBody.clearfix.fontControlBody>div>div>" +
        "div.seccol18.marginBegin>div.panel.panel-default.module3> div.panel-body>table>tbody>tr"

@Singleton
class UpsResponseMapper @Inject constructor() {

    fun map(response: ResponseBody?, onto: Tracking) = response?.toTracking().addTo(onto)

    private fun Tracking?.addTo(tracking: Tracking) = this?.run {
        tracking.copy(
                trackingNumber = tracking.trackingNumber.toUpperCase(Locale.US),
                carrier = Carrier.UPS,
                tag = waypoints.firstOrNull()?.message.orEmpty(),
                waypoints = waypoints
        )
    } ?: tracking

    private fun ResponseBody.toTracking(): Tracking {
        val doc = Jsoup.parse(string())
        val waypointsHtml = doc.select(waypointsCssQuery)
        return waypointsHtml.extractTracking()
    }

    private fun Elements.extractTracking(): Tracking {
        if (isEmpty()) return Tracking()
        val waypoints = mutableListOf<Waypoint>()

        //remove table header
        removeAt(0)

        for (waypointHtml in this) {
            waypoints.add(waypointHtml.extractWaypoint())
        }

        return Tracking(
                carrier = Carrier.UPS,
                tag = waypoints.first().message,
                waypoints = waypoints
        )
    }

    private fun Element.extractWaypoint(): Waypoint {
        val waypointCols = children()
        val city = waypointCols[0].text()
        val date = waypointCols[1].text()
        val time = waypointCols[2].text()
        val message = waypointCols[3].text()

        return Waypoint(
                city = city,
                dateTime = "$date $time".toLocalDateTime(UPS_FORMATTER),
                message = message
        )

    }
}
