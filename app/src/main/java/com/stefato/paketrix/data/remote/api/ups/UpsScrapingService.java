package com.stefato.paketrix.data.remote.api.ups;


import io.reactivex.Single;
import okhttp3.ResponseBody;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface UpsScrapingService {

    @GET("processRequest")
    Single<ResponseBody> getTracking(@Query("tracknum") String trackingNumber,
                                     @Query("loc") String locale);

}
