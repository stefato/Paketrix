package com.stefato.paketrix.data.remote.net;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.stefato.paketrix.R;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;

@Singleton
public class NetUtil {

    private Context context;

    @Inject
    NetUtil(Application application) {
        this.context = application;
    }

    private Observable<Boolean> isNetworkConnected() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return Observable.just(activeNetworkInfo != null && activeNetworkInfo.isConnected());
    }

    /**
     * Doesn't alter the Observable if Network is connected, but propagates an error up the
     * transformed chain if no Network is available.
     *
     * @param <T> any Observable compatible type
     * @return Observable that errors when there is no Network
     */
    public <T> ObservableTransformer<T, T> networkCheck() {
        return observable -> isNetworkConnected().flatMap(isConnected -> {
            if (isConnected) {
                return observable;
            } else {
                return Observable.error(new Throwable(context.getString(R.string.network_error)));
            }
        });
    }
}

