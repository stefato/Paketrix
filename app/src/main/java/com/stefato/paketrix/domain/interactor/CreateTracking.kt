package com.stefato.paketrix.domain.interactor

import com.stefato.paketrix.domain.model.Tracking
import com.stefato.paketrix.domain.repository.TrackingRepository
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CreateTracking @Inject constructor(private val trackingRepository: TrackingRepository) {

    /**
     * Saves the tracking and updates it with remote information. If there is no valid
     * remote tracking the inserted tracking stub will be deleted.
     *
     * @param tracking [Tracking] to create
     * @return Observable with true if tracking was created successfully asnd false if there
     * wasn't a valid remote tracking. If no network is available the returned Observable errors.
     */
    fun createTracking(tracking: Tracking): Observable<Boolean> {
        return Observable.just(tracking)
                .flatMap { insertAndUpdate(it) }
                .flatMap { checkTracking(it) }
    }

    private fun insertAndUpdate(tracking: Tracking): Observable<Tracking> {
        //zip combines emissions of observables
        return Observables.zip(
                trackingRepository.insertTracking(tracking),
                trackingRepository.getRemoteTracking(tracking))
        { id, t -> t.copy(id = id) }

                //ugly
                .doOnError {
                    trackingRepository.deleteTracking(
                            trackingRepository.getLastInsertedTracking().blockingFirst())
                            .subscribeOn(Schedulers.io()).subscribe()
                }
    }

    private fun checkTracking(tracking: Tracking): Observable<Boolean> {
        return if (tracking.waypoints.isEmpty()) {
            trackingRepository.deleteTracking(tracking).map {java.lang.Boolean.FALSE }
        } else {
            trackingRepository.updateTracking(tracking).map {java.lang.Boolean.TRUE }
        }
    }
}
