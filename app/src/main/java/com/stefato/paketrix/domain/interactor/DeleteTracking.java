package com.stefato.paketrix.domain.interactor;

import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.domain.repository.TrackingRepository;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;

public class DeleteTracking {

    private final TrackingRepository trackingRepository;
    private Subject<Tracking> lastRemovedSubject = BehaviorSubject.create();

    @Inject
    DeleteTracking(TrackingRepository trackingRepository) {
        this.trackingRepository = trackingRepository;
    }

    /**
     * Deletes the the passed {@link Tracking}
     *
     * @param tracking {@link Tracking} to delete
     * @return Observable with blank Boolean value.
     */
    public Observable<Boolean> delete(Tracking tracking) {
        return Observable.just(tracking)
                .doOnNext(lastRemovedSubject::onNext)
                .flatMap(trackingRepository::deleteTracking);
    }

    /**
     * Inserts last deleted {@link Tracking}
     *
     */
    //TODO make this part of stream
    public void undo() {
        lastRemovedSubject.flatMap(trackingRepository::insertTracking)
                .subscribeWith(new DisposableObserver<Long>() {
                    @Override
                    public void onNext(Long aLong) {
                        dispose();
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onComplete() {
                    }
                });
    }
}
