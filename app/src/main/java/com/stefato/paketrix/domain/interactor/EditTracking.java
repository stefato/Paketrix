package com.stefato.paketrix.domain.interactor;

import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.domain.repository.TrackingRepository;
import com.stefato.paketrix.presentation.model.TrackingUiModel;

import javax.inject.Inject;

import io.reactivex.Observable;

public class EditTracking {

    private final TrackingRepository trackingRepository;

    @Inject
    EditTracking(TrackingRepository trackingRepository) {
        this.trackingRepository = trackingRepository;
    }

    /**
     * Updates the passed tracking
     *
     * @param tracking {@link Tracking} to be updated
     * @return Observable with blank Boolean value
     */
    public Observable<Boolean> edit(Tracking tracking) {
        return trackingRepository.updateTracking(tracking);
    }
}
