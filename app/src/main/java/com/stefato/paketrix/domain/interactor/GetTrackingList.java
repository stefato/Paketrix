package com.stefato.paketrix.domain.interactor;

import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.domain.repository.TrackingRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;

public class GetTrackingList {

    private final TrackingRepository trackingRepository;

    @Inject
    GetTrackingList(TrackingRepository trackingRepository) {
        this.trackingRepository = trackingRepository;
    }

    /**
     * Returns an Observable that emits all saved {@link Tracking Trackings} on change.
     *
     * @return Observable with {@link List} of saved {@link Tracking Trackings}
     */
    public Observable<List<Tracking>> getTrackingList() {
        return trackingRepository.getTrackingsFlowable();
    }

}
