package com.stefato.paketrix.domain.interactor;

import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.domain.repository.TrackingRepository;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class Refresh {

    private final TrackingRepository trackingRepository;

    @Inject
    Refresh(TrackingRepository trackingRepository) {
        this.trackingRepository = trackingRepository;
    }

    /**
     * Checks all saved {@link Tracking Trackings} for remote changes and updates them
     *
     * @return Observable with blank Boolean value
     */
    public Observable<Boolean> refresh() {
        return trackingRepository.getTrackings()
                .flatMap(trackingRepository::getRemoteTrackings)
                .flatMap(trackingRepository::updateTrackings)
                .subscribeOn(Schedulers.io());
    }

    /**
     * Checks all saved {@link Tracking Trackings} for remote changes and updates them.
     * Emits all updated trackings
     *
     * @return Observable with List of updated trackings
     */
    public Observable<List<Tracking>> refreshAndDiff() {
        return trackingRepository.getTrackings()
                .flatMap(allTrackings ->
                        Observable.zip(
                                Observable.just(allTrackings),
                                refresh().flatMap(ignored -> trackingRepository.getTrackings()),
                                this::findDiff));
    }

    private List<Tracking> findDiff(List<Tracking> before, List<Tracking> after) {
        List<Tracking> changedTrackings = new ArrayList<>(before.size());

        for (int i = 0; i < before.size(); i++) {
            if (!before.get(i).equals(after.get(i))) {
                changedTrackings.add(after.get(i));
            }
        }
        return changedTrackings;
    }
}
