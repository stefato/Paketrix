package com.stefato.paketrix.domain.interactor

import com.stefato.paketrix.domain.model.Carrier

object ValidateCarriers {

    /**
     * Checks validity of passed tracking number and returns all possible Carriers
     *
     * @param trackingNumber tracking number to check
     * @return Set of all possible Carriers for given tracking number
     */
    @JvmStatic
    fun validateTrackingNumber(trackingNumber: String): Set<Carrier> = Carrier.values()
            .dropWhile { it == Carrier.NONE }
            .filter { it.trackingNrPattern.matches(trackingNumber) }
            .run { if (isEmpty()) setOf(Carrier.NONE) else toSet() }


    /**
     * Checks validity of passed tracking number
     *
     * @param trackingNumber tracking number to check
     * @return true for valid tracking number, false otherwise
     */
    @JvmStatic
    fun isTrackingNumberValid(trackingNumber: String) =
            !validateTrackingNumber(trackingNumber).contains(Carrier.NONE)
}

