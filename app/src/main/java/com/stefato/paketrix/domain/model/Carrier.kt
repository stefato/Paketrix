package com.stefato.paketrix.domain.model

enum class Carrier(
        val id: Int,
        val displayName: String,
        val color: Int,
        val hours: String,
        val deliveryTime: String,
        val trackingNrPattern: Regex
) {

    NONE(0, "NONE", 0, "0", "0", "a^".toRegex()),

    DHL(1, "DHL", -0x3300, "7-18", "1-2",
            "^[jJdD0-9]{12,35}|[aArRvVcClLeE][a-zA-Z]\\d{9}\\w{2}$".toRegex()),

    DPD(2, "DPD", -0x23ffce, "8-18", "1-2",
            "^[0-9]{14}$".toRegex()),

    UPS(3, "UPS", -0xa6d1de, "8-18", "1-5",
            "^1[zZ][a-zA-Z0-9]{16}$".toRegex()),

    HERMES(4, "Hermes", -0xfa6e34, "8-20", "1-2",
            "^[0-9]{14}$".toRegex()),

    GLS(5, "GLS", -0xcfbf72, "8-18", "1-2",
            "^[0-9]{11,14}".toRegex());

    companion object {

        @JvmStatic
        fun fromId(i: Int): Carrier {
            for (carrier in Carrier.values()) {
                if (carrier.id == i) {
                    return carrier
                }
            }
            return Carrier.NONE
        }
    }
}
