package com.stefato.paketrix.domain.model

data class Tracking(
        val id: Long? = null,
        val trackingNumber: String = "",
        val carrier: Carrier = Carrier.NONE,
        val tag: String = "",
        val name: String = "",
        val waypoints: List<Waypoint> = emptyList()
)