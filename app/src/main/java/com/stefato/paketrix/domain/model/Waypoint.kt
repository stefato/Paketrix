package com.stefato.paketrix.domain.model

import org.threeten.bp.LocalDateTime

data class Waypoint(
        val dateTime: LocalDateTime = LocalDateTime.MIN,
        val city: String = "",
        val message: String = ""
)