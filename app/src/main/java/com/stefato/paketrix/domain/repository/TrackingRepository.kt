package com.stefato.paketrix.domain.repository


import com.stefato.paketrix.domain.model.Tracking

import io.reactivex.Observable

interface TrackingRepository {

    fun getRemoteTracking(tracking: Tracking): Observable<Tracking>

    fun getTrackings(): Observable<List<Tracking>>

    fun getRemoteTrackings(trackings: List<Tracking>): Observable<List<Tracking>>

    fun deleteTracking(tracking: Tracking): Observable<Boolean>

    fun updateTracking(tracking: Tracking): Observable<Boolean>

    fun updateTrackings(trackings: List<Tracking>): Observable<Boolean>

    fun insertTracking(tracking: Tracking): Observable<Long>

    fun getTrackingsFlowable(): Observable<List<Tracking>>

    fun getLastInsertedTracking(): Observable<Tracking>
}
