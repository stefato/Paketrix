package com.stefato.paketrix.presentation;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.stefato.paketrix.R;
import com.stefato.paketrix.presentation.navigation.ActionBarManager;
import com.stefato.paketrix.presentation.navigation.Navigator;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity implements HasSupportFragmentInjector {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.nvView)
    NavigationView navigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @Inject
    Navigator navigator;
    @Inject
    ActionBarManager actionBarManager;
    @Inject
    DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector;
    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //need to inject before call to super
        //https://github.com/google/dagger/issues/598
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        //can't use injection for toolbar and fragmentManager
        //toolbar hast to be registered first
        //fragmentManager before super.onCreate() is from destroyed activity
        actionBarManager.initActionBar(getSupportActionBar());
        navigator.setActivity(this);

        setupNavigationDrawer();

        if (savedInstanceState == null) {
            navigator.showListFragment();
        }
    }

    private void setupNavigationDrawer() {
        navigationView.setNavigationItemSelectedListener(navigator::onDrawerNavigation);
        navigationView.setCheckedItem(R.id.nav_home);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout,
                R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(drawerToggle);

        actionBarManager.setDrawerToggle(drawerToggle);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }


    @Override
    public void onBackPressed() {
        Timber.i("back pressed");
        FragmentManager fragmentManager = getSupportFragmentManager();
        int backStackEntryCount = fragmentManager.getBackStackEntryCount();
        MenuItem homeItem = navigationView.getMenu().findItem(R.id.nav_home);

        homeItem.setChecked(true);
        drawerToggle.setDrawerIndicatorEnabled(true);

        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else if (backStackEntryCount == 1) {
            finish();
        } else if (backStackEntryCount > 2) {
            navigator.resetBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return drawerToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }


    @Override
    public AndroidInjector<Fragment> supportFragmentInjector() {
        return fragmentDispatchingAndroidInjector;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        navigator.onDestroy();
    }
}
