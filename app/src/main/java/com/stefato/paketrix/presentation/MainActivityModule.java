package com.stefato.paketrix.presentation;


import com.stefato.paketrix.presentation.injection.PerFragment;
import com.stefato.paketrix.presentation.views.about.AboutFragment;
import com.stefato.paketrix.presentation.views.detail.DetailFragment;
import com.stefato.paketrix.presentation.views.list.ListFragment;
import com.stefato.paketrix.presentation.views.list.ListFragmentModule;
import com.stefato.paketrix.presentation.views.addtracking.AddTrackingDialog;
import com.stefato.paketrix.presentation.navigation.ActionBarHandler;
import com.stefato.paketrix.presentation.navigation.ActionBarManager;
import com.stefato.paketrix.presentation.views.settings.SettingsFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainActivityModule {

    @Binds
    abstract ActionBarHandler bindActionBarHandler(ActionBarManager actionBarManager);

    @PerFragment
    @ContributesAndroidInjector(modules = ListFragmentModule.class)
    abstract ListFragment bindListFragment();

    @PerFragment
    @ContributesAndroidInjector
    abstract DetailFragment bindDetailFragment();

    @PerFragment
    @ContributesAndroidInjector
    abstract SettingsFragment bindSettingsFragment();

    @PerFragment
    @ContributesAndroidInjector
    abstract AboutFragment bindAboutFragment();

    @PerFragment
    @ContributesAndroidInjector
    abstract AddTrackingDialog bindAddTrackingDialogFragment();

}
