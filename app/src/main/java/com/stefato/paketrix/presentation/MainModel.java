package com.stefato.paketrix.presentation;


import com.stefato.paketrix.presentation.model.TrackingUiModel;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

@Singleton
public class MainModel {

    private Subject<TrackingUiModel> detailItemSubject = BehaviorSubject.create();
    private Subject<TrackingUiModel> createTrackingSubject = PublishSubject.create();

    @Inject
    MainModel() {
    }

    public Observable<TrackingUiModel> getDetailItemSubject() {
        return detailItemSubject;
    }

    public void setDetailItem(TrackingUiModel tracking) {
        detailItemSubject.onNext(tracking);
    }

    public Observable<TrackingUiModel> getCreateTrackingSubject() {
        return createTrackingSubject;
    }

    public void setCreateTracking(TrackingUiModel tracking) {
        createTrackingSubject.onNext(tracking);
    }

}
