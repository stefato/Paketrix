package com.stefato.paketrix.presentation.base


import android.content.Context
import android.os.Bundle
import android.widget.Toast
import com.hannesdorfmann.mosby3.mvi.MviPresenter
import com.hannesdorfmann.mosby3.mvp.MvpView
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

open class BaseDialogFragment<V : MvpView, P : MviPresenter<V, *>> : MviDialogFragment<V, P>() {

    @Inject
    lateinit var presenter: P

    private var onActivityCreatedCalled = false

    override fun createPresenter() = presenter

    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    //use view of dialog for kotlinx view bindings
    //view has to be null until onActivityCreated()
    // to avoid call to setContentView() (conflict with MD)
    override fun getView() =
            if (onActivityCreatedCalled) dialog.window.decorView
            else super.getView()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        onActivityCreatedCalled = true
    }

    fun showErrorMessage(error: Throwable) {
        Toast.makeText(context, error.message, Toast.LENGTH_LONG).show()
    }
}

