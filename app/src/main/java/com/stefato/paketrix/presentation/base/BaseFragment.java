package com.stefato.paketrix.presentation.base;


import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.hannesdorfmann.mosby3.mvi.MviFragment;
import com.hannesdorfmann.mosby3.mvi.MviPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public class BaseFragment<V extends MvpView, P extends MviPresenter<V, ?>>
        extends MviFragment<V, P> {

    @Inject
    P presenter;

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @NonNull
    @Override
    public P createPresenter() {
        return presenter;
    }

    public void showErrorMessage(Throwable error) {
        String errorMessage = error.getMessage();

        if (errorMessage != null) {
            Toast.makeText(this.getContext(), errorMessage, Toast.LENGTH_LONG).show();
        }
    }
}
