package com.stefato.paketrix.presentation.jobs;


import android.support.annotation.NonNull;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobCreator;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

@Singleton
public class BackgroundJobCreator implements JobCreator {
    
    Map<String, Provider<Job>> jobs;

    @Inject
    BackgroundJobCreator(Map<String, Provider<Job>> jobs) {
        this.jobs = jobs;
    }

    @Override
    public Job create(@NonNull String tag) {
        Provider<Job> jobProvider = jobs.get(tag);
        return jobProvider != null ? jobProvider.get() : null;
    }
}
