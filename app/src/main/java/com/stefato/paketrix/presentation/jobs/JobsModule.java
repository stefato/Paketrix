package com.stefato.paketrix.presentation.jobs;


import com.evernote.android.job.Job;
import com.stefato.paketrix.domain.interactor.Refresh;

import dagger.Module;
import dagger.Provides;
import dagger.multibindings.IntoMap;
import dagger.multibindings.StringKey;

@Module
public class JobsModule {

    @Provides
    @IntoMap
    @StringKey(PeriodicRefreshJob.TAG)
    Job providePeriodicRefreshJob(Refresh refresh) {
        return new PeriodicRefreshJob(refresh);
    }

}
