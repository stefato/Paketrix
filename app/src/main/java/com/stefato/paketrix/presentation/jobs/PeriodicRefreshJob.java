package com.stefato.paketrix.presentation.jobs;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.evernote.android.job.Job;
import com.evernote.android.job.JobManager;
import com.evernote.android.job.JobRequest;
import com.stefato.paketrix.domain.interactor.Refresh;
import com.stefato.paketrix.presentation.util.NotificationUtil;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import timber.log.Timber;


public class PeriodicRefreshJob extends Job {

    static final String TAG = "periodic_refresh_job";
    private Refresh refreshInteractor;

    @Inject
    PeriodicRefreshJob(Refresh refreshInteractor) {
        this.refreshInteractor = refreshInteractor;
    }

    public static void schedulePeriodic(int period) {
        JobRequest periodicRefreshJobRequest = new JobRequest.Builder(TAG)
                .setPeriodic(TimeUnit.MINUTES.toMillis(period), TimeUnit.MINUTES.toMillis(5))
                .setUpdateCurrent(true)
                .setRequiredNetworkType(JobRequest.NetworkType.CONNECTED)
                .setRequirementsEnforced(true)
                .build();
        periodicRefreshJobRequest.schedule();
        Timber.d("new Background job with %s period", period);
    }

    public static void cancelJobs() {
        int canceled = JobManager.instance().cancelAll();
        Timber.d("%s jobs canceled", canceled);
    }

    @NonNull
    @Override
    protected Result onRunJob(@NonNull Params params) {
        executeJob(getContext());
        return Result.SUCCESS;
    }

    @VisibleForTesting
    public void executeJob(Context context) {
        refreshInteractor.refreshAndDiff()
                .blockingForEach(newTrackings -> {
                    if (newTrackings.size() > 0) {
                        NotificationUtil.initChannels(context); //is ignored this if channel exists
                        NotificationUtil.createUpdateNotification(context, newTrackings);
                        Timber.d("refreshInteractor job finished, %s updates",
                                newTrackings.size());
                    } else {
                        Timber.d("refreshInteractor job finished, no updates");
                    }
                });
    }

}
