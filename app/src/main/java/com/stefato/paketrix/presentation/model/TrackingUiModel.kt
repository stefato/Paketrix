package com.stefato.paketrix.presentation.model

import android.os.Parcelable
import com.stefato.paketrix.domain.model.Carrier
import com.stefato.paketrix.domain.model.Tracking
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TrackingUiModel(
        val id: Long? = null,
        val trackingNumber: String = "",
        val carrier: Carrier = Carrier.NONE,
        val tag: String = "",
        val name: String = "",
        val waypoints: List<WaypointUiModel> = emptyList()
) : Parcelable {

    companion object {
        @JvmStatic
        fun fromTracking(tracking: Tracking) = with(tracking) { TrackingUiModel(id, trackingNumber, carrier, tag, name, WaypointUiModel.fromWaypoints(waypoints)) }

        @JvmStatic
        fun fromTrackingList(trackingList: List<Tracking>) = trackingList.map { fromTracking(it) }

        @JvmStatic
        fun toTrackingList(trackingUiModelList: List<TrackingUiModel>) = trackingUiModelList.map { it.toTracking() }
    }

    fun toTracking() = Tracking(id, trackingNumber, carrier, tag, name, WaypointUiModel.toWaypoints(waypoints))
}