package com.stefato.paketrix.presentation.model

import android.os.Parcelable
import com.stefato.paketrix.domain.model.Waypoint
import kotlinx.android.parcel.Parcelize
import org.threeten.bp.LocalDateTime

@Parcelize
data class WaypointUiModel(
        val dateTime: LocalDateTime = LocalDateTime.MIN,
        val city: String = "",
        val message: String = ""
) : Parcelable{


    companion object {
        fun fromWaypoints(waypoints: List<Waypoint>) = waypoints.map {WaypointUiModel(it.dateTime,it.city,it.message) }
        fun toWaypoints(waypointUiModels: List<WaypointUiModel>) = waypointUiModels.map { Waypoint(it.dateTime,it.city,it.message) }
    }
}
