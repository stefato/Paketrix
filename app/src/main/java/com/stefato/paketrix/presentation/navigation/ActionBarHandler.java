package com.stefato.paketrix.presentation.navigation;


public interface ActionBarHandler {

    void setActionBarTitle(String title);

    void setActionBarTitle(int resId);

    void setDrawerIndicatorEnabled(boolean enable);
}
