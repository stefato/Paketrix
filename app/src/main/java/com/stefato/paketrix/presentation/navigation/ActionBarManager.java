package com.stefato.paketrix.presentation.navigation;


import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;

import com.stefato.paketrix.presentation.injection.PerActivity;

import javax.inject.Inject;

@PerActivity
public class ActionBarManager implements ActionBarHandler {

    private ActionBar actionBar;
    private ActionBarDrawerToggle drawerToggle;

    @Inject
    ActionBarManager() {
    }

    public void initActionBar(ActionBar actionBar) {
        this.actionBar = actionBar;
        this.actionBar.setDisplayHomeAsUpEnabled(true);
    }


    public void setDrawerToggle(ActionBarDrawerToggle drawerToggle) {
        this.drawerToggle = drawerToggle;
    }

    @Override
    public void setActionBarTitle(String title) {
        actionBar.setTitle(title);
    }

    @Override
    public void setActionBarTitle(int resId) {
        actionBar.setTitle(resId);
    }

    @Override
    public void setDrawerIndicatorEnabled(boolean enable) {
        drawerToggle.setDrawerIndicatorEnabled(enable);
    }
}
