package com.stefato.paketrix.presentation.navigation;


public interface ListViewNavigator {

    void showDialog();

    void openTracking();
}
