package com.stefato.paketrix.presentation.navigation;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.stefato.paketrix.R;
import com.stefato.paketrix.presentation.views.about.AboutFragment;
import com.stefato.paketrix.presentation.views.detail.DetailFragment;
import com.stefato.paketrix.presentation.views.list.ListFragment;
import com.stefato.paketrix.presentation.views.addtracking.AddTrackingDialog;
import com.stefato.paketrix.presentation.views.settings.SettingsFragment;

import javax.inject.Inject;
import javax.inject.Singleton;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

//needs to be singleton not perActivity
//on orientation change presenters are recreated before setActivity()
//is called in onCreate() of activity
@Singleton
public class Navigator implements ListViewNavigator {

    private static final int POP_BACK_STACK_EXCLUSIVE = 0;
    private static final String ROOT_BACK_STATE = "root_back_state";
    private static final String DIALOG_FRAGMENT = "dialogFragment";

    private AppCompatActivity activity;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    private FragmentManager fragmentManager;

    @Inject
    public Navigator() {
    }

    public void showListFragment() {
        showRootFragment(ListFragment.newInstance());
        Timber.i("show ListFragment");
    }

    private void showRootFragment(Fragment fragment) {
        fragmentManager.beginTransaction()
                .replace(R.id.root_layout, fragment)
                .addToBackStack(ROOT_BACK_STATE)
                .commit();

        fragmentManager.executePendingTransactions();
    }

    @Override
    public void showDialog() {
        AddTrackingDialog addTrackingDialog = AddTrackingDialog.newInstance();
        addTrackingDialog.show(fragmentManager, DIALOG_FRAGMENT);
        Timber.i("show AddTrackingDialog");
    }

    @Override
    public void openTracking() {
        showRootFragment(DetailFragment.newInstance());
        Timber.i("show DetailFragment");
    }

    private void showSettings() {
        showFragment(SettingsFragment.newInstance());
        Timber.i("show SettingsFragment");
    }

    private void showAbout() {
        showFragment(AboutFragment.newInstance());
        Timber.i("show AboutFragment");
    }

    private void showFragment(Fragment fragment) {
        fragmentManager.beginTransaction()
                .replace(R.id.root_layout, fragment)
                .addToBackStack(null)
                .commit();

        fragmentManager.executePendingTransactions();
    }

    public void resetBackStack() {
        fragmentManager.popBackStackImmediate(ROOT_BACK_STATE, POP_BACK_STACK_EXCLUSIVE);
    }

    public void onDestroy() {
        fragmentManager = null;
    }

    public void setActivity(AppCompatActivity activity) {
        this.activity = activity;
        this.fragmentManager = activity.getSupportFragmentManager();
        ButterKnife.bind(this, activity);
    }

    private void sendEmailIntent() {
        //TODO to res
        String uriText =
                "mailto:bugs@support.stefato.com";
        Uri uri = Uri.parse(uriText);
        Intent sendIntent = new Intent(Intent.ACTION_SENDTO);
        sendIntent.setData(uri);
        if (sendIntent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(Intent.createChooser(sendIntent, "EMail senden..."));
            Timber.i("send Email intent");
        }
    }

    public boolean onDrawerNavigation(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                resetBackStack();
                break;
            case R.id.nav_settings:
                showSettings();
                break;
            case R.id.nav_about:
                showAbout();
                break;
            case R.id.nav_error_report:
                sendEmailIntent();
                break;
            default:
                return false;
        }
        menuItem.setChecked(true);
        drawerLayout.closeDrawers();

        return true;
    }
}

