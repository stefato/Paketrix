package com.stefato.paketrix.presentation.util;


import android.content.Context;
import android.support.v7.preference.ListPreference;
import android.util.AttributeSet;

/**
 * Nearly identical to Androids {@link ListPreference} except for updating the summary field
 * on value change
 */
public class AutoSummaryListPreference extends ListPreference {
    public AutoSummaryListPreference(Context context) {
        super(context);
    }

    public AutoSummaryListPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setValue(String value) {
        super.setValue(value);
        setSummary(getEntry());
    }
}
