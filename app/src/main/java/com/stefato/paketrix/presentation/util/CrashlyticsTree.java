package com.stefato.paketrix.presentation.util;


import android.util.Log;

import com.crashlytics.android.Crashlytics;

import timber.log.Timber;

//DebugTree sets class tags automatically, normal tree does not
public class CrashlyticsTree extends Timber.DebugTree {

    public CrashlyticsTree() {
        // Ensure crashlytics class is available, fail-fast if not available.
        Crashlytics.class.getCanonicalName();
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        if (priority == Log.VERBOSE || priority == Log.DEBUG) {
            return;
        }
        Crashlytics.log(tag != null ? "[" + tag + "] " + message : message);
    }
}
