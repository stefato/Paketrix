package com.stefato.paketrix.presentation.util;


import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;

import com.stefato.paketrix.R;
import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.presentation.MainActivity;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.FormatStyle;

import java.util.List;

public class NotificationUtil {

    private static final int INBOX_STYLE_DISPLAY_SIZE = 3;
    private static final String DEFAULT_NOTIFICATION_CHANNEL = "default";

    public static void initChannels(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            return;
        }
        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationChannel channel = new NotificationChannel(DEFAULT_NOTIFICATION_CHANNEL,
                context.getString(R.string.notification_channel_tracking),
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription(context.getString(R.string.notification_channel_updates_desc));

        if (notificationManager != null) {
            //is ignored if channel exists
            notificationManager.createNotificationChannel(channel);
        }
    }

    public static void createUpdateNotification(Context context, List<Tracking> trackings) {
        PendingIntent pi = PendingIntent.getActivity(context, 0,
                new Intent(context, MainActivity.class), 0);
        NotificationCompat.InboxStyle inboxStyle = createInboxStyle(trackings);

        Tracking firstTracking = trackings.get(0);
        String firstParcelName = firstTracking.getName().length() == 0 ?
                firstTracking.getTrackingNumber() : firstTracking.getName();
        String firstParcelContent = firstTracking.getTag();

        Notification notification = new NotificationCompat.Builder(
                context, DEFAULT_NOTIFICATION_CHANNEL)
                .setContentTitle(context.getString(R.string.notification_content_title))
                .setContentText(firstParcelName + " " + firstParcelContent)
                .setAutoCancel(true)
                .setContentIntent(pi)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setShowWhen(true)
                .setColor(Color.BLUE)
                .setLocalOnly(true)
                .setStyle(inboxStyle)
                .build();

        //replace current/dismissed notification
        int id = 0; //(int) System.currentTimeMillis();

        NotificationManagerCompat.from(context)
                .notify(context.getPackageName(), id, notification);
    }

    private static NotificationCompat.InboxStyle createInboxStyle(List<Tracking> newTrackings) {
        NotificationCompat.InboxStyle inboxStyle =
                new NotificationCompat.InboxStyle();
        int moreCount = newTrackings.size() - INBOX_STYLE_DISPLAY_SIZE;
        inboxStyle.setSummaryText(moreCount > 0 ? Integer.toString(moreCount) : "");
        int nbrDisplayedUpdates = newTrackings.size() > INBOX_STYLE_DISPLAY_SIZE ?
                INBOX_STYLE_DISPLAY_SIZE : newTrackings.size();

        for (int i = 0; i < nbrDisplayedUpdates; i++) {
            addToInboxStyle(newTrackings.get(i), inboxStyle);
        }

        if (nbrDisplayedUpdates == INBOX_STYLE_DISPLAY_SIZE) {
            inboxStyle.addLine("...");
        }
        return inboxStyle;
    }

    private static void addToInboxStyle(Tracking tracking,
                                        NotificationCompat.InboxStyle inboxStyle) {
        LocalDateTime dateTimeLastWaypoint = tracking.getWaypoints().get(0).getDateTime();
        String date = dateTimeLastWaypoint.toLocalDate()
                .format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM));
        String time = dateTimeLastWaypoint.toLocalTime()
                .format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT));

        String name = tracking.getName().length() == 0 ?
                tracking.getTrackingNumber() : tracking.getName();
        Spannable span = new SpannableString(name + " - " + date + " " + time);
        span.setSpan(new StyleSpan(android.graphics.Typeface.BOLD),
                0, name.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        inboxStyle.addLine(span);
        inboxStyle.addLine(tracking.getTag());
    }
}
