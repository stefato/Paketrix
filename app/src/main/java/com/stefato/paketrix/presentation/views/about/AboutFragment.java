package com.stefato.paketrix.presentation.views.about;

import android.content.Context;
import android.os.Bundle;

import com.mikepenz.aboutlibraries.ui.LibsSupportFragment;
import com.stefato.paketrix.R;
import com.stefato.paketrix.presentation.navigation.ActionBarHandler;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

public class AboutFragment extends LibsSupportFragment {

    private static final String[] INCLUDED_LIBRARIES = {"materialdialogs", "android_job",
            "room", "threetenabp", "mosby3", "autovalue", "autovaluegson", "autovalueparcel",
            "okhttp", "retrofit", "rxjava", "rxbinding", "rxandroid", "butterknife",
            "dagger2", "gson", "guava", "jsoup", "leakcanary", "SupportLibrary",
            "timber"};
    private static final String[] EXCLUDED_LIBRARIES = {"AndroidIconics", "fastadapter"};
    //TODO to res
    private static final String ABOUT_DESCRIPTION = "<b>Verwendete OpenSource-Bibliotheken</b>";
    private static final String APP_NAME = "Paketrix";


    @Inject
    ActionBarHandler actionBarHandler;

    public static LibsSupportFragment newInstance() {
        return new CustomLibsBuilder()
                .withAutoDetect(false)
                .withLibraries(INCLUDED_LIBRARIES)
                .withLicenseShown(true)
                .withAboutIconShown(true)
                .withLicenseDialog(true)
                .withAboutAppName(APP_NAME)
                .withAboutVersionShown(true)
                .withAboutDescription(ABOUT_DESCRIPTION)
                .withExcludedLibraries(EXCLUDED_LIBRARIES)
                .supportFragment();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        actionBarHandler.setActionBarTitle(R.string.about);
        actionBarHandler.setDrawerIndicatorEnabled(true);
    }
}
