package com.stefato.paketrix.presentation.views.about;

import android.os.Bundle;

import com.mikepenz.aboutlibraries.LibsBuilder;
import com.mikepenz.aboutlibraries.ui.LibsSupportFragment;

public class CustomLibsBuilder extends LibsBuilder {

    @Override
    public LibsSupportFragment supportFragment() {
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", this);

        LibsSupportFragment fragment = new AboutFragment();
        fragment.setArguments(bundle);

        return fragment;
    }
}
