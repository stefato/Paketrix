package com.stefato.paketrix.presentation.views.addtracking

import android.app.Dialog
import android.os.Bundle
import android.view.WindowManager
import android.widget.RadioButton
import com.afollestad.materialdialogs.DialogAction
import com.afollestad.materialdialogs.MaterialDialog
import com.jakewharton.rxbinding2.widget.RxRadioGroup
import com.jakewharton.rxbinding2.widget.RxTextView
import com.stefato.paketrix.R
import com.stefato.paketrix.domain.model.Carrier
import com.stefato.paketrix.presentation.base.BaseDialogFragment
import com.stefato.paketrix.presentation.model.TrackingUiModel
import io.reactivex.Observable
import io.reactivex.rxkotlin.Observables
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_add_tracking.*
import timber.log.Timber

class AddTrackingDialog : BaseDialogFragment<AddTrackingView, AddTrackingPresenter>(), AddTrackingView {

    private val sendBackSubject = PublishSubject.create<TrackingUiModel>()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // can't use onCreateView() and onCreateDialog()
        // so we inflate the view here and pass it to MviDialogFragment
        val dialogView = requireActivity().layoutInflater.inflate(R.layout.fragment_add_tracking, null)

        super.onViewCreated(dialogView, savedInstanceState)

        return MaterialDialog.Builder(requireContext())
                .customView(dialogView, true)
                .title(R.string.add_tracking_title)
                .positiveText(R.string.save)
                .negativeText(R.string.cancel)
                .onPositive { _, _ -> sendBackResult() }
                .build().apply {
                    window.setSoftInputMode(
                            WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
                }
    }

    override fun trackingIdObservable(): Observable<CharSequence> {
        return RxTextView.textChanges(id_input)
    }

    override fun carrierSelectionObservable(): Observable<Int> {
        return RxRadioGroup.checkedChanges(carrier_radios)
    }

    override fun sendResultObservable(): Observable<TrackingUiModel> {
        return sendBackSubject
    }

    override fun formvalidation(): Observable<TrackingUiModel> {
        return Observables.combineLatest(trackingIdObservable(), carrierSelectionObservable())
        { trackingId, carrierId ->
            TrackingUiModel(
                    trackingNumber = trackingId.toString(),
                    carrier = Carrier.fromId(carrierId))
        }
    }

    override fun render(uiModel: AddTrackingUiModel) {
        Timber.d("received UiModel: %s", uiModel.toString())
        //TODO clear state after error
        if (uiModel.error != null) {
            showErrorMessage(uiModel.error)
        }
        if (uiModel.possibleCarriers != null) {
            showPossibleCarriers(uiModel.possibleCarriers)
        }
        if (uiModel.isFormValid) {
            formIsValid()
        } else {
            formIsNotValid()
        }
    }

    private fun showPossibleCarriers(possibleCarriers: Set<Carrier>) {
        if (!possibleCarriers.contains(Carrier.NONE)) {
            carrier_radios.removeAllViews()
            makeRadioButtons(possibleCarriers)
        } else {
            carrier_radios.removeAllViews()
        }

    }

    private fun formIsNotValid() {
        setSaveButtonEnabled(false)
        //TODO custom validation error
    }

    private fun formIsValid() {
        setSaveButtonEnabled(true)
    }

    private fun makeRadioButtons(possibleCarriers: Set<Carrier>) {
        for (carrier in possibleCarriers) {
            val rb = RadioButton(context).apply {
                id = carrier.id
                text = carrier.displayName
            }
            carrier_radios.addView(rb)
        }

        if (carrier_radios.childCount == 1) {
            carrier_radios.clearCheck()
            (carrier_radios.getChildAt(0) as RadioButton).isChecked = true
        } else {
            carrier_radios.clearCheck()
        }
    }

    private fun setSaveButtonEnabled(enabled: Boolean) {
        (dialog as MaterialDialog).getActionButton(DialogAction.POSITIVE).isEnabled = enabled
    }

    private fun sendBackResult() {
        val checkedRadioButton = carrier_radios.checkedRadioButtonId
        sendBackSubject.onNext(TrackingUiModel(
                name = name_input.text.toString(),
                trackingNumber = id_input.text.toString(),
                carrier = Carrier.fromId(checkedRadioButton)
        ))
    }

    companion object {
        @JvmStatic
        fun newInstance(): AddTrackingDialog {
            return AddTrackingDialog()
        }
    }
}

