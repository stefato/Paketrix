package com.stefato.paketrix.presentation.views.addtracking;

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter;
import com.stefato.paketrix.domain.interactor.ValidateCarriers;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.presentation.MainModel;
import com.stefato.paketrix.presentation.injection.PerFragment;
import com.stefato.paketrix.presentation.model.TrackingUiModel;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;


@PerFragment
public class AddTrackingPresenter extends MviBasePresenter<AddTrackingView, AddTrackingUiModel> {

    private MainModel mainModel;

    @Inject
    public AddTrackingPresenter(MainModel mainModel) {
        this.mainModel = mainModel;
    }

    private Observable<Boolean> validateForm(TrackingUiModel tracking) {
        return Observable.just(
                tracking.getCarrier() != Carrier.NONE
                        && ValidateCarriers.
                        isTrackingNumberValid(tracking.getTrackingNumber()));
    }

    @Override
    protected void bindIntents() {

        intent(AddTrackingView::sendResultObservable)
                .doOnNext(ignored -> Timber.i("sendResult received"))
                .subscribe(mainModel::setCreateTracking);

        Observable<AddTrackingUiModel> observables =
                Observable.merge(possibleCarriers(), formValidation())
                        .doOnNext(addTrackingUiModel ->
                                Timber.i("send %s", addTrackingUiModel));

        subscribeViewState(observables, AddTrackingView::render);
    }

    /**
     * Calls {@link #validateForm(TrackingUiModel)} when {@link AddTrackingView#formvalidation()} fires
     *
     * @return Observable with {@link AddTrackingUiModel#formIsValid(boolean)} or
     * {@link AddTrackingUiModel#error}
     */
    private Observable<AddTrackingUiModel> formValidation() {
        return intent(AddTrackingView::formvalidation)
                .doOnNext(ignored -> Timber.i("formValidation received"))
                .flatMap(this::validateForm)
                .map(AddTrackingUiModel::formIsValid)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread());
    }

    /**
     * Calls {@link ValidateCarriers#validateTrackingNumber(String)} when
     * {@link AddTrackingView#trackingIdObservable} emits
     *
     * @return Observable with {@link AddTrackingUiModel#possibleCarriers} or
     * {@link AddTrackingUiModel#error}
     */
    private Observable<AddTrackingUiModel> possibleCarriers() {
        return intent(AddTrackingView::trackingIdObservable)
                .doOnNext(ignored -> Timber.i("trackingId received"))
                .flatMap(trackingId ->
                        Observable.just(
                                ValidateCarriers.validateTrackingNumber(trackingId.toString()))
                                .map(AddTrackingUiModel::possibleCarriers))
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturn(AddTrackingUiModel::error);
    }


}
