package com.stefato.paketrix.presentation.views.addtracking


import com.stefato.paketrix.domain.model.Carrier

data class AddTrackingUiModel(
        val isFormValid: Boolean = false,
        val error: Throwable? = null,
        val possibleCarriers: Set<Carrier>? = null
) {
    companion object {
        @JvmStatic
        fun error(error: Throwable) = AddTrackingUiModel(error = error)

        @JvmStatic
        fun formIsValid(isFormValid: Boolean) = AddTrackingUiModel(isFormValid = isFormValid)

        @JvmStatic
        fun possibleCarriers(possibleCarriers: Set<Carrier>) = AddTrackingUiModel(possibleCarriers = possibleCarriers)
    }
}

