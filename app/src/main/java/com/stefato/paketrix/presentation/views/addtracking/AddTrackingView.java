package com.stefato.paketrix.presentation.views.addtracking;

import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.presentation.model.TrackingUiModel;

import javax.annotation.Nonnull;

import io.reactivex.Observable;


public interface AddTrackingView extends MvpView {

    void render(AddTrackingUiModel uiModel);

    @Nonnull
    Observable<CharSequence> trackingIdObservable();

    @Nonnull
    Observable<TrackingUiModel> formvalidation();

    @Nonnull
    Observable<TrackingUiModel> sendResultObservable();

    @Nonnull
    Observable<Integer> carrierSelectionObservable();

}
