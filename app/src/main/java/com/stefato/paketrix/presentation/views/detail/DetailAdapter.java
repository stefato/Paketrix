package com.stefato.paketrix.presentation.views.detail;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stefato.paketrix.R;
import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.domain.model.Waypoint;
import com.stefato.paketrix.presentation.injection.PerFragment;
import com.stefato.paketrix.presentation.model.TrackingUiModel;
import com.stefato.paketrix.presentation.model.WaypointUiModel;

import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.FormatStyle;
import org.threeten.bp.format.TextStyle;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

@PerFragment
public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.DetailViewHolder> {

    private List<WaypointUiModel> waypoints;

    @Inject
    DetailAdapter() {
        waypoints = Collections.emptyList();
    }

    void setParcels(TrackingUiModel tracking) {
        this.waypoints = tracking.getWaypoints();
        notifyDataSetChanged();
    }

    @Override
    public DetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_detail, parent, false);

        return new DetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DetailViewHolder holder, int position) {
        WaypointUiModel waypoint = this.waypoints.get(position);
        holder.onBind(waypoint);
    }

    @Override
    public int getItemCount() {
        return waypoints.size();
    }

    class DetailViewHolder extends RecyclerView.ViewHolder {

        private static final int EXPANDED_LINE_COUNT = 10;
        private static final int MIN_LINE_COUNT = 2;
        
        @BindView(R.id.detail_tag)
        TextView tag;
        @BindView(R.id.detail_city)
        TextView city;
        @BindView(R.id.detail_date)
        TextView date;
        @BindView(R.id.detail_time)
        TextView time;
        @BindView(R.id.detail_month)
        TextView month;

        private boolean expandedTag;

        DetailViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void onBind(WaypointUiModel waypoint) {
            //TODO all logic should be done on uiModel creation
            final LocalDate localDate = waypoint.getDateTime().toLocalDate();

            tag.setText(waypoint.getMessage());
            tag.setOnClickListener(view -> {
                if (!expandedTag) {
                    expandedTag = true;
                    tag.setMaxLines(EXPANDED_LINE_COUNT);
                } else {
                    expandedTag = false;
                    tag.setMaxLines(MIN_LINE_COUNT);
                }
            });

            city.setText(waypoint.getCity());
            date.setText(String.format(
                    Locale.getDefault(), "%d.", localDate.getDayOfMonth()));
            time.setText(
                    waypoint.getDateTime().toLocalTime()
                            .format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
            month.setText(localDate.getMonth().getDisplayName(
                    TextStyle.SHORT, Locale.getDefault()));
        }
    }
}

