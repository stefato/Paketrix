package com.stefato.paketrix.presentation.views.detail

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import com.stefato.paketrix.R
import com.stefato.paketrix.presentation.base.BaseFragment
import com.stefato.paketrix.presentation.model.TrackingUiModel
import com.stefato.paketrix.presentation.navigation.ActionBarHandler
import com.stefato.paketrix.presentation.views.detail.util.ItemDecorator
import io.reactivex.Observable
import kotlinx.android.synthetic.main.fragment_detail.*
import timber.log.Timber
import javax.inject.Inject

private const val KEY_LAST_STATE = "lastState"
private const val ITEM_BOTTOM_SPACE = -5

class DetailFragment : BaseFragment<DetailView, DetailPresenter>(), DetailView {

    @Inject
    lateinit var detailAdapter: DetailAdapter
    @Inject
    lateinit var actionBarHandler: ActionBarHandler

    private var lastUiModel: DetailUiModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            lastUiModel = it.getParcelable(KEY_LAST_STATE)
        }
        Timber.d("onCreate")
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_detail, container, false)
        setHasOptionsMenu(true)
        return view
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val i = item.itemId
        if (i == android.R.id.home) {
            requireActivity().onBackPressed()
            return true
        }
        return true
    }

    private fun showDetails(tracking: TrackingUiModel) {
        detailAdapter.setParcels(tracking)
        appbar.setBackgroundColor(tracking.carrier.color)
        carrier_name.text = tracking.carrier.displayName
        carrier_hours.text = String.format(resources.getString(R.string.time_format),
                tracking.carrier.hours)
        carrier_delivery_time.text = String.format(resources.getString(R.string.day_format),
                tracking.carrier.deliveryTime)
        actionBarHandler.setActionBarTitle(if (tracking.name.isEmpty())
            tracking.trackingNumber
        else
            tracking.name)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        actionBarHandler.setDrawerIndicatorEnabled(false)
        initRecyclerView()
    }

    override fun loadItem(): Observable<Boolean> {
        //after process death there is no item set in MainModel
        //so we get last uiModel from saved bundle
        //TODO send to presenter (logs, reducer, tests, stateless view)
        lastUiModel?.let { render(it) }
        Timber.d("send loadItem")
        return Observable.just(java.lang.Boolean.TRUE)
    }

    override fun render(uiModel: DetailUiModel) {
        Timber.i("received UiModel: %s", uiModel)
        lastUiModel = uiModel
        val tracking = uiModel.tracking

        if (uiModel.error != null) {
            showErrorMessage(uiModel.error)
        } else if (tracking != null) {
            showDetails(tracking)
        }
    }

    private fun initRecyclerView() {
        val itemDecorator = ItemDecorator(ITEM_BOTTOM_SPACE)
        detail_rv.apply {
            addItemDecoration(itemDecorator)
            layoutManager = LinearLayoutManager(context)
            adapter = detailAdapter
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Timber.d("save UiModel")
        outState.putParcelable(KEY_LAST_STATE, lastUiModel)
    }

    companion object {
        @JvmStatic
        fun newInstance() = DetailFragment()
    }
}
