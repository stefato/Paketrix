package com.stefato.paketrix.presentation.views.detail;

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter;
import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.presentation.MainModel;
import com.stefato.paketrix.presentation.injection.PerFragment;
import com.stefato.paketrix.presentation.model.TrackingUiModel;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

@PerFragment
public class DetailPresenter extends MviBasePresenter<DetailView, DetailUiModel> {

    private MainModel mainModel;

    @Inject
    DetailPresenter(MainModel mainModel) {
        this.mainModel = mainModel;
        Timber.d("created");
    }

    @Override
    protected void bindIntents() {

        Observable<DetailUiModel> stateObservable = loadDetailItem()
                .doOnNext(detailUiModel -> Timber.i("send %s", detailUiModel));

        subscribeViewState(stateObservable, DetailView::render);

        Timber.d("bind Intents");
    }

    /**
     * Gets {@link Tracking} from {@link MainModel#getDetailItemSubject()}
     *
     * @return Observable with {@link DetailUiModel#successWithContent(TrackingUiModel)}} or
     * {@link DetailUiModel#error}
     */
    private Observable<DetailUiModel> loadDetailItem() {
        return intent(DetailView::loadItem)
                .doOnNext(ignored -> Timber.i("loadItem received"))
                .subscribeOn(Schedulers.io())
                .flatMap(ignored -> mainModel.getDetailItemSubject()
                        .map(DetailUiModel::successWithContent)
                        .onErrorReturn(DetailUiModel::error))
                .observeOn(AndroidSchedulers.mainThread());
    }
}
