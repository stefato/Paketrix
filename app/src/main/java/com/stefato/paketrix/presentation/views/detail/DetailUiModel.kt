package com.stefato.paketrix.presentation.views.detail

import android.os.Parcelable
import com.stefato.paketrix.presentation.model.TrackingUiModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DetailUiModel(
        val tracking: TrackingUiModel? = null,
        val error: Throwable? = null
) : Parcelable {
    companion object {
        @JvmStatic
        fun error(error: Throwable) = DetailUiModel(error = error)

        @JvmStatic
        fun successWithContent(tracking: TrackingUiModel) = DetailUiModel(tracking = tracking)
    }
}
