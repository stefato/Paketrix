package com.stefato.paketrix.presentation.views.detail;


import com.hannesdorfmann.mosby3.mvp.MvpView;

import javax.annotation.Nonnull;

import io.reactivex.Observable;

public interface DetailView extends MvpView {

    @Nonnull
    Observable<Boolean> loadItem();

    void render(DetailUiModel uiModel);
}
