package com.stefato.paketrix.presentation.views.detail.util;


import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class ItemDecorator extends RecyclerView.ItemDecoration {

    private final int bottomSpace;

    public ItemDecorator(int bottomSpace) {
        this.bottomSpace = bottomSpace;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                               RecyclerView.State state) {
        outRect.bottom = bottomSpace;
    }
}
