package com.stefato.paketrix.presentation.views.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stefato.paketrix.R;
import com.stefato.paketrix.presentation.injection.PerFragment;
import com.stefato.paketrix.presentation.model.TrackingUiModel;
import com.stefato.paketrix.presentation.views.list.util.ItemTouchHelperAdapter;

import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.FormatStyle;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.subjects.PublishSubject;

@PerFragment
public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ItemViewHolder>
        implements ItemTouchHelperAdapter {

    private List<TrackingUiModel> trackingList;
    private PublishSubject<TrackingUiModel> trackingClickSubject;
    private PublishSubject<TrackingUiModel> deleteSubject;
    private PublishSubject<TrackingUiModel> editSubject;

    @Inject
    public ListAdapter() {
        trackingClickSubject = PublishSubject.create();
        deleteSubject = PublishSubject.create();
        editSubject = PublishSubject.create();
        trackingList = new ArrayList<>();
    }

    void setTrackingList(List<TrackingUiModel> trackingList) {
        this.trackingList = new ArrayList<>(trackingList);
        notifyDataSetChanged();
    }

    PublishSubject<TrackingUiModel> getTrackingClick() {
        return trackingClickSubject;
    }

    PublishSubject<TrackingUiModel> getTrackingDeleteSubject() {
        return deleteSubject;
    }

    PublishSubject<TrackingUiModel> getEditSubject() {
        return editSubject;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tracking, parent, false);

        ItemViewHolder holder = new ItemViewHolder(view);
        holder.itemView.setOnClickListener(v ->
                onItemClick(holder.getAdapterPosition()));
        return holder;
    }

    @Override
    public void onBindViewHolder(ListAdapter.ItemViewHolder holder, int position) {
        holder.onBind(trackingList.get(position));
    }

    @Override
    public int getItemCount() {
        return trackingList.size();
    }

    @Override
    public void onItemDismiss(int position) {
        deleteSubject
                .onNext(trackingList.get(position));
    }

    private void onItemClick(int adapterPosition) {
        trackingClickSubject
                .onNext(trackingList.get(adapterPosition));
    }

    public void onItemEdit(int position) {
        editSubject
                .onNext(trackingList.get(position));
        notifyItemChanged(position); //redraws item after swipe animation
    }


    static class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.list_tracking_name)
        TextView trackingName;
        @BindView(R.id.list_carrier)
        TextView carrier;
        @BindView(R.id.list_tag)
        TextView tag;
        @BindView(R.id.list_last_updated_date)
        TextView lastUpdatedDate;
        @BindView(R.id.list_last_updated_time)
        TextView lastUpdatedTime;
        @BindView(R.id.list_carrier_color)
        TextView carrierColor;

        ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void onBind(TrackingUiModel tracking) {
            //TODO all logic should be done on uiModel creation
            String name = (tracking.getName().length() != 0) ?
                    tracking.getName() : tracking.getTrackingNumber();

            trackingName.setText(name);
            carrier.setText(tracking.getCarrier().getDisplayName());
            carrierColor.setBackgroundColor(tracking.getCarrier().getColor());
            tag.setText(tracking.getTag());
            if (!tracking.getWaypoints().isEmpty()) {
                lastUpdatedDate.setText(tracking.getWaypoints().get(0)
                        .getDateTime().toLocalDate()
                        .format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)));
                lastUpdatedTime.setText(tracking.getWaypoints().get(0)
                        .getDateTime().toLocalTime().format(
                                DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)));
            }
        }
    }
}
