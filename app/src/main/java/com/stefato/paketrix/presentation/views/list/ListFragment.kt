package com.stefato.paketrix.presentation.views.list

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.helper.ItemTouchHelper
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.jakewharton.rxbinding2.support.v4.widget.RxSwipeRefreshLayout
import com.jakewharton.rxbinding2.view.RxView
import com.stefato.paketrix.R
import com.stefato.paketrix.presentation.base.BaseFragment
import com.stefato.paketrix.presentation.model.TrackingUiModel
import com.stefato.paketrix.presentation.navigation.ActionBarHandler
import com.stefato.paketrix.presentation.views.list.util.SimpleItemTouchHelperCallback
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_list.*
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val CLICK_DEBOUNCE_TIMEOUT = 200
private const val MAX_INPUT_LENGTH = 58

class ListFragment : BaseFragment<ListView, ListPresenter>(), ListView {
    @Inject
    lateinit var listAdapter: ListAdapter
    @Inject
    lateinit var actionBarHandler: ActionBarHandler

    private val clearStateSubject = PublishSubject.create<Boolean>()
    private val editSubject = PublishSubject.create<TrackingUiModel>()
    private val undoSubject = PublishSubject.create<Boolean>()

    private val editDisposable = CompositeDisposable()
    private var editDialog: MaterialDialog? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        actionBarHandler.setActionBarTitle(R.string.tracking_overview)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        Timber.d("onCreateView")
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initListeners()
    }

    private fun initListeners() {
        editDisposable.add(listAdapter.editSubject.subscribe { openEditDialog(it) })
    }

    private fun initRecyclerView() {
        tracking_rv.apply {
            adapter = listAdapter

            val llm = LinearLayoutManager(context)
            val orientation = llm.orientation
            layoutManager = llm
            addItemDecoration(DividerItemDecoration(context, orientation))

            val callback = SimpleItemTouchHelperCallback(listAdapter, context)
            ItemTouchHelper(callback).attachToRecyclerView(this)
        }

    }

    override fun render(viewState: ListUiModel) {
        with(viewState) {
            Timber.d("received UiModel: %s", toString())
            refresh_layout.isRefreshing = isInProgress
            update_btn.isEnabled = !isInProgress

            if (!isInProgress) {
                if (trackings != null) {
                    showTrackingList(trackings)
                }
                if (isDeleted) {
                    showSnackbarDeleted()
                } else if (error != null) {
                    showErrorMessage(error)
                    clearStateSubject.onNext(java.lang.Boolean.TRUE)
                } else if (!isCreated) {
                    showNotCreated()
                    clearStateSubject.onNext(java.lang.Boolean.TRUE)
                }
            }
        }


    }

    override fun fabClick(): Observable<Any> {
        return RxView.clicks(create_btn).debounce(CLICK_DEBOUNCE_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
    }

    override fun trackingClick(): Observable<TrackingUiModel> {
        return listAdapter.trackingClick
    }

    override fun clearState(): Observable<Boolean> {
        return clearStateSubject
    }

    override fun refresh(): Observable<Boolean> {
        return RxSwipeRefreshLayout.refreshes(refresh_layout)
                .mergeWith(RxView.clicks(update_btn))
                .debounce(CLICK_DEBOUNCE_TIMEOUT.toLong(), TimeUnit.MILLISECONDS)
                .map { java.lang.Boolean.TRUE }
    }

    override fun getEditObservable(): Observable<TrackingUiModel> {
        return editSubject
    }

    override fun delete(): Observable<TrackingUiModel> {
        return listAdapter.trackingDeleteSubject
    }

    override fun loadItems(): Observable<Boolean> {
        return Observable.just(java.lang.Boolean.TRUE)
    }

    override fun getUndoObservable(): Observable<Boolean> {
        return undoSubject
    }

    fun showTrackingList(trackings: List<TrackingUiModel>) {
        listAdapter.setTrackingList(trackings)
    }

    private fun showNotCreated() {
        Toast.makeText(this.context, R.string.error_tracking_not_found, Toast.LENGTH_LONG).show()
    }

    private fun openEditDialog(tracking: TrackingUiModel) {
        val hint = resources.getString(R.string.hint_name)
        editDialog = MaterialDialog.Builder(requireContext())
                .title(R.string.dialog_edit_name)
                .inputRange(0, MAX_INPUT_LENGTH)
                .inputType(InputType.TYPE_CLASS_TEXT)
                .input(hint, tracking.name, true) { _, input ->
                    editSubject.onNext(tracking.copy(name = input.toString()))
                }
                .show()
    }

    private fun showSnackbarDeleted() {
        Snackbar.make(snackbar_placeholder, R.string.snack_bar_item_removed, Snackbar.LENGTH_LONG)
                .setAction(R.string.undo) { undoSubject.onNext(java.lang.Boolean.TRUE) }
                .show()
    }

    override fun onDestroyView() {
        Timber.d("onDestroyView")
        editDisposable.dispose()
        super.onDestroyView()
    }

    override fun onPause() {
        super.onPause()
        editDialog?.apply {
            dismiss()
            editDialog = null
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(): ListFragment {
            return ListFragment()
        }
    }
}

