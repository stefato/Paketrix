package com.stefato.paketrix.presentation.views.list;

import com.stefato.paketrix.presentation.navigation.ListViewNavigator;
import com.stefato.paketrix.presentation.navigation.Navigator;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ListFragmentModule {

    @Binds
    abstract ListViewNavigator bindListViewNavigator(Navigator navigator);
}
