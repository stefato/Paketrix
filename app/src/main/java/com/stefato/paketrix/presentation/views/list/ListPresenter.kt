package com.stefato.paketrix.presentation.views.list

import com.hannesdorfmann.mosby3.mvi.MviBasePresenter
import com.stefato.paketrix.domain.interactor.*
import com.stefato.paketrix.presentation.MainModel
import com.stefato.paketrix.presentation.injection.PerFragment
import com.stefato.paketrix.presentation.model.TrackingUiModel
import com.stefato.paketrix.presentation.navigation.ListViewNavigator
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.util.*
import javax.inject.Inject

@PerFragment
class ListPresenter//maybe too many deps
@Inject
internal constructor(
        private val navigator: ListViewNavigator,
        private val getTrackingListInteractor: GetTrackingList,
        private val editTrackingInteractor: EditTracking,
        private val deleteTrackingInteractor: DeleteTracking,
        private val refreshInteractor: Refresh,
        private val createTrackingInteractor: CreateTracking,
        private val mainModel: MainModel
) : MviBasePresenter<ListView, ListUiModel>() {

    init {
        Timber.d("created")
    }

    override fun bindIntents() {
        //TODO let non view model intents emit view models to show erros on ui and make crash analytics easier
        setupNonViewModelIntents()

        subscribeViewState(stateObservable()) { listView, uiModel ->
            listView.render(uiModel)
        }
    }

    private fun stateObservable(): Observable<ListUiModel> {
        val allEvents = Observable.merge(Arrays.asList(
                loadItems(), refresh(), delete(), createTracking(), clearState()))

        val initialState = ListUiModel.idle()

        return allEvents.scan(
                initialState) { previousState, changes -> viewStateReducer(previousState, changes) }
                .doOnNext { detailUiModel -> Timber.i("send %s", detailUiModel) }
    }

    private fun setupNonViewModelIntents() {
        //show dialog when fabClick emits
        intent { it.fabClick() }
                .doOnNext { Timber.i("fabClick received") }
                .subscribe { navigator.showDialog() }

        //getEditObservable tracking when getEditObservable emits
        intent { it.editObservable }.subscribeOn(Schedulers.io())
                .doOnNext { Timber.i("editObservable received") }
                .subscribeOn(Schedulers.io())
                .map { it.toTracking() }
                .flatMap { editTrackingInteractor.edit(it) }
                .subscribe()

        //set detail item in mainModel and open detailView when tracking click emits
        intent { it.trackingClick() }
                .doOnNext { Timber.i("trackingClick received") }
                .subscribe {
                    mainModel.setDetailItem(it)
                    navigator.openTracking()
                }

        //getUndoObservable deletion when getUndoObservable emits
        intent { it.undoObservable }
                .doOnNext { Timber.i("undoObservable received") }
                .subscribe { deleteTrackingInteractor.undo() }
    }

    private fun clearState(): Observable<ListUiModel> {
        return intent { it.clearState() }
                .doOnNext { Timber.i("clearState received") }
                .map { ListUiModel.idle() }
    }

    /**
     * Calls [CreateTracking.createTracking] when
     * [MainModel.getCreateTrackingSubject] fires
     *
     * @return Observable with [ListUiModel.created]} with `true` for
     * found tracking and `false` for empty tracking;
     * [ListUiModel.error]} for Errors
     */
    private fun createTracking(): Observable<ListUiModel> {
        return mainModel.createTrackingSubject
                .doOnNext { Timber.i("createTrackingInteractor received") }
                .subscribeOn(Schedulers.io())
                .map { it.toTracking() }
                .flatMap { tracking ->
                    createTrackingInteractor.createTracking(tracking)
                            .map { ListUiModel.created(it) }
                            .onErrorReturn { ListUiModel.error(it) }
                            .subscribeOn(Schedulers.io())
                }
                .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * Calls [DeleteTracking.delete] when [ListView.delete] fires
     *
     * @return Observable with [ListUiModel] in corresponding state
     */
    private fun delete(): Observable<ListUiModel> {
        return intent { it.delete() }
                .doOnNext { Timber.i("delete received") }
                .subscribeOn(Schedulers.io())
                .map { it.toTracking() }
                .flatMap { tracking ->
                    deleteTrackingInteractor.delete(tracking)
                            .map { ListUiModel.deleted() }
                            .onErrorReturn { ListUiModel.error(it) }
                }
                .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * Emits [ListUiModel.inProgress] when [ListView.loadItems] fires,
     * then calls [and emits][GetTrackingList.getTrackingList]
     */
    private fun loadItems(): Observable<ListUiModel> {
        return intent { it.loadItems() }
                .doOnNext { Timber.i("loadItems received") }
                .flatMap { _ ->
                    getTrackingListInteractor.trackingList
                            .map { TrackingUiModel.fromTrackingList(it) }
                            .map { ListUiModel.successWithContent(it) }
                            .onErrorReturn { ListUiModel.error(it) }
                            .startWith(ListUiModel.inProgress())
                            .subscribeOn(Schedulers.io())
                }
                .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * Emits [ListUiModel.inProgress] when [ListView.refresh] fires
     * then calls [Refresh.refresh] and emits [ListUiModel.idle] when finished
     *
     * @return Observable with [ListUiModel] in corresponding state
     */
    private fun refresh(): Observable<ListUiModel> {
        return intent { it.refresh() }
                .doOnNext { Timber.i("refresh received") }
                .subscribeOn(Schedulers.io())
                .flatMap { _ ->
                    refreshInteractor.refresh()
                            .map { ListUiModel.idle() }
                            .onErrorReturn { ListUiModel.error(it) }
                            .startWith(ListUiModel.inProgress())
                }
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun viewStateReducer(previousState: ListUiModel, changes: ListUiModel): ListUiModel {
        return if (changes.trackings == null) {
            changes.copy(trackings = previousState.trackings)
        } else changes
    }

}
