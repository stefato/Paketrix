package com.stefato.paketrix.presentation.views.list

import com.stefato.paketrix.presentation.model.TrackingUiModel

data class ListUiModel(
        val isInProgress: Boolean = false,
        val isCreated: Boolean = true,
        val isDeleted: Boolean = false,
        val trackings: List<TrackingUiModel>? = null,
        val error: Throwable? = null
) {
    companion object {
        fun error(error: Throwable) = ListUiModel(error = error)
        fun successWithContent(trackings: List<TrackingUiModel>) = ListUiModel(trackings = trackings)
        fun inProgress() = ListUiModel(isInProgress = true)
        fun idle() = ListUiModel()
        fun deleted() = ListUiModel(isDeleted = true)
        fun created(created: Boolean) = ListUiModel(isCreated = created)
    }
}