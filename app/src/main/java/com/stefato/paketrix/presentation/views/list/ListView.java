package com.stefato.paketrix.presentation.views.list;


import com.hannesdorfmann.mosby3.mvp.MvpView;
import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.presentation.model.TrackingUiModel;

import javax.annotation.Nonnull;

import io.reactivex.Observable;


public interface ListView extends MvpView {


    void render(ListUiModel uiModel);

    @Nonnull
    Observable<Boolean> loadItems();

    @Nonnull
    Observable<Boolean> refresh();

    @Nonnull
    Observable<Boolean> clearState();

    @Nonnull
    Observable<Boolean> getUndoObservable();

    @Nonnull
    Observable<TrackingUiModel> delete();

    @Nonnull
    Observable<TrackingUiModel> trackingClick();

    @Nonnull
    Observable<TrackingUiModel> getEditObservable();

    @Nonnull
    Observable<Object> fabClick();

}
