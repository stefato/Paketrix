package com.stefato.paketrix.presentation.views.list.util;


public interface ItemTouchHelperAdapter {

    void onItemDismiss(int position);
}
