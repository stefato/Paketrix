package com.stefato.paketrix.presentation.views.list.util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.stefato.paketrix.R;
import com.stefato.paketrix.presentation.views.list.ListAdapter;

public class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback {

    private final ListAdapter mAdapter;

    private Drawable deleteBackground;
    private Drawable editBackground;
    private Drawable editIcon;
    private Drawable deleteIcon;

    private boolean initiated;
    private Context context;


    public SimpleItemTouchHelperCallback(ListAdapter adapter, Context context) {
        this.mAdapter = adapter;
        this.context = context;
    }

    private void init() {
        deleteBackground = new ColorDrawable(ContextCompat.getColor(context, R.color.colorDelete));
        editBackground = new ColorDrawable(ContextCompat.getColor(context, R.color.colorEdit));
        deleteIcon = ContextCompat.getDrawable(context, android.R.drawable.ic_menu_delete);
        editIcon = ContextCompat.getDrawable(context, android.R.drawable.ic_menu_edit);
        editIcon.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        deleteIcon.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        initiated = true;
    }

    @Override
    public boolean isLongPressDragEnabled() {
        return true;
    }

    @Override
    public boolean isItemViewSwipeEnabled() {
        return true;
    }

    @Override
    public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        final int dragFlags = 0;
        final int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;
        return makeMovementFlags(dragFlags, swipeFlags);
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder source,
                          RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        int position = viewHolder.getAdapterPosition();

        if (direction == ItemTouchHelper.START) {
            mAdapter.onItemDismiss(position);
        } else {
            mAdapter.onItemEdit(position);
        }
    }

    //Views are redrawn frequently, avoid expensive operations
    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                            float dX, float dY, int actionState, boolean isCurrentlyActive) {

        if (viewHolder.getAdapterPosition() == RecyclerView.NO_POSITION) {
            return;
        }

        if (!initiated) {
            init();
        }

        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            View itemView = viewHolder.itemView;

            if (dX < 0) {
                deleteSwipe(c, dX, itemView);
            } else {
                editSwipe(c, dX, itemView);
            }
        }

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }

    private void deleteSwipe(Canvas c, float dX, View itemView) {
        int itemHeight = itemView.getHeight();
        int itemViewTop = itemView.getTop();
        int itemViewBottom = itemView.getBottom();
        int itemViewRight = itemView.getRight();

        int deleteIconIntrinsicHeight = deleteIcon.getIntrinsicHeight();
        int deleteIconTop = itemViewTop + (itemHeight - deleteIconIntrinsicHeight) / 2;
        int deleteIconMargin = (itemHeight - deleteIconIntrinsicHeight) / 2;
        int deleteIconLeft = itemViewRight - deleteIconMargin - deleteIcon.getIntrinsicWidth();
        int deleteIconRight = itemViewRight - deleteIconMargin;
        int deleteIconBottom = deleteIconTop + deleteIconIntrinsicHeight;

        deleteBackground.setBounds(itemViewRight + Math.round(dX), itemViewTop,
                itemViewRight, itemViewBottom);
        deleteBackground.draw(c);
        deleteIcon.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom);
        deleteIcon.draw(c);
    }

    private void editSwipe(Canvas c, float dX, View itemView) {
        int itemHeight = itemView.getHeight();
        int itemViewTop = itemView.getTop();
        int itemViewBottom = itemView.getBottom();
        int itemViewLeft = itemView.getLeft();

        int editIconIntrinsicHeight = editIcon.getIntrinsicHeight();
        int editIconTop = itemViewTop + (itemHeight - editIconIntrinsicHeight) / 2;
        int editIconMargin = (itemHeight - editIconIntrinsicHeight) / 2;
        int editIconLeft = itemViewLeft + editIconMargin;
        int editIconRight = itemViewLeft + editIconMargin + editIcon.getIntrinsicWidth();
        int editIconBottom = editIconTop + editIconIntrinsicHeight;

        editBackground.setBounds(itemViewLeft, itemViewTop,
                itemViewLeft + Math.round(dX), itemViewBottom);
        editBackground.draw(c);
        editIcon.setBounds(editIconLeft, editIconTop, editIconRight, editIconBottom);
        editIcon.draw(c);
    }

}
