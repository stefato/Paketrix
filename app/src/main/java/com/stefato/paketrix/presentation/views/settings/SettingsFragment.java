package com.stefato.paketrix.presentation.views.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;

import com.stefato.paketrix.R;
import com.stefato.paketrix.presentation.jobs.PeriodicRefreshJob;
import com.stefato.paketrix.presentation.navigation.ActionBarHandler;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import timber.log.Timber;

public class SettingsFragment extends PreferenceFragmentCompat
        implements SharedPreferences.OnSharedPreferenceChangeListener {

    public static final String PREF_TRACKING_UPDATES = "pref_tracking_updates";
    public static final String PREF_TRACKING_UPDATES_PERIOD = "pref_tracking_updates_period";
    public static final String DEFAULT_PERIOD = "15";

    @Inject
    ActionBarHandler actionBarHandler;

    private SharedPreferences sharedPreferences;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onAttach(Context context) {
        AndroidSupportInjection.inject(this);
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        actionBarHandler.setActionBarTitle(R.string.settings);
        actionBarHandler.setDrawerIndicatorEnabled(true);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);
        sharedPreferences = getPreferenceManager().getSharedPreferences();
    }

    @Override
    public void onPause() {
        super.onPause();
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

        boolean backgroundUpdateActive =
                sharedPreferences.getBoolean(PREF_TRACKING_UPDATES, false);
        Timber.d("backgroundUpdateActive %s", backgroundUpdateActive);
        if (backgroundUpdateActive) {
            String period = sharedPreferences
                    .getString(PREF_TRACKING_UPDATES_PERIOD, DEFAULT_PERIOD);
            PeriodicRefreshJob.schedulePeriodic(Integer.parseInt(period));
        } else {
            PeriodicRefreshJob.cancelJobs();
        }
    }
}
