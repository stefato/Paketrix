package com.stefato.paketrix.utils

import org.threeten.bp.LocalDateTime
import org.threeten.bp.format.DateTimeFormatter

fun String.defaultIfBlank(default: String) = if (isBlank()) default else this

fun String.toLocalDateTime(
        dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME) =
        LocalDateTime.parse(this, dateTimeFormatter)
