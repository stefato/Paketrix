package com.stefato.paketrix;


import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.stefato.paketrix.data.remote.api.dpd.response.DpdResponse;
import com.stefato.paketrix.data.remote.api.gls.response.GlsResponse;
import com.stefato.paketrix.data.remote.api.hermes.response.HermesResponse;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class TestModelFactory {

    public static String buildUps() throws IOException, URISyntaxException {
        URL resource = TestModelFactory.class.getResource("/upsResponse.html");
        byte[] encoded = Files.readAllBytes(Paths.get(resource.toURI()));
        return new String(encoded, StandardCharsets.UTF_8);
    }

    public static GlsResponse buildGls() throws FileNotFoundException {
        URL resource = TestModelFactory.class.getResource("/glsResponseTest.json");
        JsonReader reader = new JsonReader(new FileReader(resource.getPath()));
        return UnitTestData.getGson().fromJson(reader, GlsResponse.class);
    }

    public static String buildDhl() throws IOException, URISyntaxException {
        URL resource = TestModelFactory.class.getResource("/dhlResponse.html");
        byte[] encoded = Files.readAllBytes(Paths.get(resource.toURI()));
        return new String(encoded, StandardCharsets.UTF_8);
    }

    public static HermesResponse buildHermes() throws FileNotFoundException {
        URL resource = TestModelFactory.class.getResource("/hermesResponseTest.json");
        JsonReader reader = new JsonReader(new FileReader(resource.getPath()));
        Type listType = new TypeToken<ArrayList<HermesResponse>>() {
        }.getType();
        List<HermesResponse> yourClassList = UnitTestData.getGson().fromJson(reader, listType);
        return yourClassList.get(0);
    }

    public static DpdResponse buildDpd() throws FileNotFoundException {
        URL resource = TestModelFactory.class.getResource("/dpdResponseTest.json");
        JsonReader reader = new JsonReader(new FileReader(resource.getPath()));
        return UnitTestData.getGson().fromJson(reader, DpdResponse.class);
    }

    public static DpdResponse buildEmptyDpd() throws FileNotFoundException {
        URL resource = TestModelFactory.class.getResource("/dpdEmptyResponse.json");
        JsonReader reader = new JsonReader(new FileReader(resource.getPath()));
        return UnitTestData.getGson().fromJson(reader, DpdResponse.class);
    }
}
