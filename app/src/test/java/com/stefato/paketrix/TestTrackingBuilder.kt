package com.stefato.paketrix


import com.stefato.paketrix.UnitTestData.CITIES
import com.stefato.paketrix.UnitTestData.DATE_TIMES
import com.stefato.paketrix.UnitTestData.IDS
import com.stefato.paketrix.UnitTestData.MESSAGES
import com.stefato.paketrix.UnitTestData.TRACKING_NUMBERS
import com.stefato.paketrix.domain.model.Carrier
import com.stefato.paketrix.domain.model.Tracking
import com.stefato.paketrix.domain.model.Waypoint

class TestTrackingBuilder {

    private var datasetId = 0
    private var carrier: Carrier? = null

    private val waypoints: List<Waypoint>
        get() {
            return (0 until CITIES.size).map {
                val city = if (carrier == Carrier.HERMES) "" else CITIES[it]
                Waypoint(dateTime = DATE_TIMES[it], city = city, message = MESSAGES[it])
            }

        }

    fun buildMinimal() = Tracking(id = IDS[datasetId], trackingNumber = TRACKING_NUMBERS[carrier].orEmpty())

    fun withDataset(i: Int): TestTrackingBuilder {
        if (i >= IDS.size) throw IllegalArgumentException("Dataset with this ID not available")
        this.datasetId = i
        return this
    }

    fun withCarrier(carrier: Carrier): TestTrackingBuilder {
        this.carrier = carrier
        return this
    }

    fun build(): Tracking {
        return getTracking(carrier, datasetId)
    }

    private fun getTracking(carrier: Carrier?, i: Int) = Tracking(
            id = IDS[i],
            trackingNumber = TRACKING_NUMBERS[carrier].orEmpty(),
            carrier = carrier!!,
            tag = MESSAGES[0],
            waypoints = waypoints)
}

