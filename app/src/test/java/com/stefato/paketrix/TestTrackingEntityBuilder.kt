package com.stefato.paketrix


import com.stefato.paketrix.UnitTestData.CITIES
import com.stefato.paketrix.data.local.entities.TrackingEntity
import com.stefato.paketrix.data.local.entities.WaypointEntity
import com.stefato.paketrix.domain.model.Carrier

class TestTrackingEntityBuilder {

    private var datasetId: Int = 0
    private var carrier: Carrier? = null

    private val waypointEntities = (0 until CITIES.size).map {
        WaypointEntity(
                city = UnitTestData.CITIES[it],
                message = UnitTestData.MESSAGES[it],
                dateTime = UnitTestData.DATE_TIMES[it]
        )
    }

    fun withDataset(i: Int): TestTrackingEntityBuilder {
        datasetId = i
        return this
    }

    fun withCarrier(carrier: Carrier): TestTrackingEntityBuilder {
        this.carrier = carrier
        return this
    }

    fun build(): TrackingEntity {
        return getTrackingEntity(carrier, datasetId)
    }

    private fun getTrackingEntity(carrier: Carrier?, i: Int): TrackingEntity {
        assert(i < UnitTestData.IDS.size)
        return TrackingEntity(
                id = UnitTestData.IDS[i], name = "",
                trackingNumber = UnitTestData.TRACKING_NUMBERS[carrier],
                carrier = carrier!!.id,
                tag = UnitTestData.MESSAGES[0],
                waypoints = waypointEntities
        )
    }
}
