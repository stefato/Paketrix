package com.stefato.paketrix

import com.google.gson.GsonBuilder
import com.stefato.paketrix.domain.model.Carrier
import org.threeten.bp.LocalDateTime


object UnitTestData {

    @JvmStatic
    val gson = GsonBuilder()
            .setLenient()
            .create()

    @JvmStatic
    val DATE_TIMES = listOf(
            LocalDateTime.of(2017, 3, 12, 15, 33),
            LocalDateTime.of(2017, 10, 9, 11, 48),
            LocalDateTime.of(2014, 11, 11, 11, 11),
            LocalDateTime.of(2018, 12, 3, 0, 29),
            LocalDateTime.of(2009, 4, 2, 13, 44))

    @JvmStatic
    val CITIES = listOf(
            "Berlin",
            "Munich",
            "Hamburg",
            "Cologne",
            "Dresden")

    @JvmStatic
    val MESSAGES = listOf(
            "Lorem ipsum dolor sit amet, consetetur sadipscing elitr.",
            "Cras sodales nunc lorem, in consequat mauris pulvinar ut.",
            "Duis ligula orci, pretium eu sodales ut, porttitor at mauris.",
            "In vel metus non erat blandit vulputate nec eget ipsum.",
            "Nulla aliquam euismod ligula et finibus.")

    @JvmStatic
    val IDS = listOf(22L, 35L, 44L, 61L)

    @JvmStatic
    val TRACKING_NUMBERS = mapOf(
            Carrier.DHL to "CB127759248DE",
            Carrier.DPD to "01997402945165",
            Carrier.HERMES to "73031206515537",
            Carrier.GLS to "612829371231",
            Carrier.UPS to "1ZW000186893032102"
    )

}
