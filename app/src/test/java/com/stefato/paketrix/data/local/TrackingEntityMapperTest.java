package com.stefato.paketrix.data.local;

import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.TestTrackingEntityBuilder;
import com.stefato.paketrix.data.local.entities.TrackingEntity;
import com.stefato.paketrix.data.local.mapper.TrackingEntityMapper;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TrackingEntityMapperTest {

    @Test
    public void toParcelList() throws Exception {
        List<Tracking> expectedTrackings = new ArrayList<>(2);
        expectedTrackings.add(
                new TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).build());
        expectedTrackings.add(
                new TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(1).build());

        List<TrackingEntity> trackingEntities = new ArrayList<>(2);
        trackingEntities.add(
                new TestTrackingEntityBuilder().withCarrier(Carrier.DHL).withDataset(0).build());
        trackingEntities.add(
                new TestTrackingEntityBuilder().withCarrier(Carrier.DHL).withDataset(1).build());

        List<Tracking> actualTrackings = TrackingEntityMapper.toTrackingList(trackingEntities);

        assertEquals(expectedTrackings, actualTrackings);
    }

    @Test
    public void toParcel() throws Exception {
        Tracking trackingExpected =
                new TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).build();

        TrackingEntity trackingEntity =
                new TestTrackingEntityBuilder().withCarrier(Carrier.DHL).withDataset(0).build();
        Tracking trackingActual = TrackingEntityMapper.toTracking(trackingEntity);

        assertEquals(trackingExpected, trackingActual);
    }

    @Test
    public void toParcelEntity() throws Exception {
        TrackingEntity trackingEntityExpected =
                new TestTrackingEntityBuilder().withCarrier(Carrier.DHL).withDataset(0).build();

        TrackingEntity trackingEntityActual =
                TrackingEntityMapper.toTrackingEntity(
                        new TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).build());

        assertEquals(trackingEntityExpected, trackingEntityActual);
    }


    @Test
    public void toParcelEntityList() throws Exception {
        List<TrackingEntity> expectedTrackingEntities = new ArrayList<>(2);
        expectedTrackingEntities.add(
                new TestTrackingEntityBuilder().withCarrier(Carrier.DHL).withDataset(0).build());
        expectedTrackingEntities.add(
                new TestTrackingEntityBuilder().withCarrier(Carrier.DHL).withDataset(1).build());

        List<Tracking> trackingList = new ArrayList<>();
        trackingList.add(new TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).build());
        trackingList.add(new TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(1).build());

        List<TrackingEntity> actualTrackingEntities =
                TrackingEntityMapper.toTrackingEntityList(trackingList);

        assertEquals(expectedTrackingEntities, actualTrackingEntities);
    }


}