package com.stefato.paketrix.data.remote.api.dhl;

import com.squareup.moshi.Moshi;
import com.stefato.paketrix.TestModelFactory;
import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;

import org.junit.Assert;
import org.junit.Test;

import okhttp3.ResponseBody;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DhlModelMapperTest {

    private final DhlResponseMapper mapper = new DhlResponseMapper(new Moshi.Builder().build());

    @Test
    public void fromDhlScrapingModel() throws Exception {
        Tracking trackingExpected =
                new TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).build();

        Tracking minimalTracking =
                new TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).buildMinimal();
        ResponseBody responseBody = mock(ResponseBody.class);
        when(responseBody.string()).thenReturn(TestModelFactory.buildDhl());

        Tracking trackingActual = mapper.map(responseBody, minimalTracking);

        assertEquals(trackingExpected, trackingActual);
    }


    @Test
    public void shouldReturnInputParcelIfReponseIsNull() {
        Tracking parcelExpected =
                new TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).buildMinimal();

        Tracking parcelActual = mapper.map(null, parcelExpected);

        Assert.assertEquals(parcelExpected, parcelActual);
    }

}