package com.stefato.paketrix.data.remote.api.dhl;

import com.stefato.paketrix.TestModelFactory;
import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.ResponseBody;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DhlRepositoryTest {

    @Mock
    private DhlService service;

    @Mock
    private DhlResponseMapper mapper;

    @InjectMocks
    private DhlRepository repository;

    private Tracking minimalTracking;
    private Tracking expectedTracking = new TestTrackingBuilder()
            .withCarrier(Carrier.DHL).withDataset(0).build();

    @Before
    public void setUp() throws Exception {
        minimalTracking =
                new TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).buildMinimal();
        ResponseBody responseBody = ResponseBody.create(MediaType.parse("text/html"),TestModelFactory.buildDhl());
        when(service.getTracking(minimalTracking.getTrackingNumber()))
                .thenReturn(Single.just(responseBody));

        when(mapper.map(responseBody,minimalTracking)).thenReturn(expectedTracking);
    }

    @Test
    public void getTrackingShouldReturnTracking() throws Exception {
        Tracking actualTracking = repository.getTracking(minimalTracking).blockingGet();

        assertEquals(expectedTracking, actualTracking);
    }

}
