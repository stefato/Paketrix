package com.stefato.paketrix.data.remote.api.dpd;

import com.stefato.paketrix.TestModelFactory;
import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.data.remote.api.dpd.response.DpdResponse;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.ResponseBody;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DpdRepositoryTest {


    @Mock
    private DpdService service;
    private DpdRepository repository;
    private DpdResponseMapper mapper = new DpdResponseMapper();
    private Tracking minimalTracking = new TestTrackingBuilder()
            .withCarrier(Carrier.DPD).withDataset(0).buildMinimal();

    @Before
    public void setUp() throws Exception {
        repository =  new DpdRepository(service,mapper);

        when(service.getTracking(minimalTracking.getTrackingNumber(), "de_DE"))
                .thenReturn(Observable.just(TestModelFactory.buildDpd()));
    }

    @Test
    public void getTracking() throws Exception {
        Tracking expectedTracking =
                new TestTrackingBuilder().withCarrier(Carrier.DPD).withDataset(0).build();

        Tracking actualTracking = repository.getTracking(minimalTracking).blockingFirst();

        assertEquals(expectedTracking, actualTracking);
    }

}