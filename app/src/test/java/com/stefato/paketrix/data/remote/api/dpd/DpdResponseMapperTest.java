package com.stefato.paketrix.data.remote.api.dpd;

import com.stefato.paketrix.TestModelFactory;
import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.data.remote.api.dpd.response.DpdResponse;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DpdResponseMapperTest {

    private final DpdResponseMapper mapper = new DpdResponseMapper();

    @Test
    public void fromDpdResponse() throws Exception {
        Tracking expectedTracking =
                new TestTrackingBuilder().withCarrier(Carrier.DPD).withDataset(0).build();

        DpdResponse dpdResponse = TestModelFactory.buildDpd();

        Tracking minimalTracking =
                new TestTrackingBuilder().withCarrier(Carrier.DPD).withDataset(0).buildMinimal();
        Tracking actualTracking = mapper.map(dpdResponse, minimalTracking);

        assertEquals(expectedTracking, actualTracking);
    }


    @Test
    public void shouldReturnInputParcelIfResponseIsNull() {
        Tracking parcelExpected =
                new TestTrackingBuilder().withCarrier(Carrier.DPD).withDataset(0).buildMinimal();

        Tracking parcelActual = mapper.map(null, parcelExpected);

        assertEquals(parcelExpected, parcelActual);
    }

    @Test
    public void shouldReturnInputParcelIfResponseIsEmpty() throws Exception {
        Tracking parcelExpected =
                new TestTrackingBuilder().withCarrier(Carrier.DPD).withDataset(0).buildMinimal();

        Tracking parcelActual = mapper.map(TestModelFactory.buildEmptyDpd(), parcelExpected);

        assertEquals(parcelExpected, parcelActual);
    }


}