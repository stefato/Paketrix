package com.stefato.paketrix.data.remote.api.gls;

import com.stefato.paketrix.TestModelFactory;
import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.data.remote.api.gls.response.GlsResponse;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Observable;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GlsRepositoryTest {

    @Mock
    private GlsService service;

    @Mock
    private GlsResponseMapper mapper;

    @InjectMocks
    private GlsRepository repository;

    private Tracking minimalTracking;

    private GlsResponse glsResponse;


    @Before
    public void setUp() throws Exception {
        minimalTracking =
                new TestTrackingBuilder().withCarrier(Carrier.GLS).withDataset(0).buildMinimal();

        glsResponse = TestModelFactory.buildGls();

        when(service.getTracking(minimalTracking.getTrackingNumber()))
                .thenReturn(Observable.just(glsResponse));
    }

    @Test
    public void getTracking() throws Exception {
        Tracking expectedTracking =
                new TestTrackingBuilder().withCarrier(Carrier.GLS).withDataset(0).build();
        when(mapper.map(glsResponse, minimalTracking)).thenReturn(expectedTracking);


        Tracking actualTracking = repository.getTracking(minimalTracking).blockingFirst();

        assertEquals(expectedTracking, actualTracking);
    }

}