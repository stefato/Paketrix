package com.stefato.paketrix.data.remote.api.gls;

import com.stefato.paketrix.TestModelFactory;
import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.data.remote.api.gls.response.GlsResponse;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GlsResponseMapperTest {
    @Test
    public void fromGlsResponse() throws Exception {
        Tracking expectedTracking =
                new TestTrackingBuilder().withCarrier(Carrier.GLS).withDataset(0).build();

        GlsResponse glsResponse = TestModelFactory.buildGls();
        Tracking minimalTracking=
                new TestTrackingBuilder().withCarrier(Carrier.GLS).withDataset(0).buildMinimal();

        Tracking parcelActual = new GlsResponseMapper().map(glsResponse,minimalTracking);

        assertEquals(expectedTracking, parcelActual);
    }


    @Test
    public void shouldReturnInputParcelIfReponseIsNull() throws Exception {
        Tracking parcelExpected =
                new TestTrackingBuilder().withCarrier(Carrier.DPD).withDataset(0).buildMinimal();

        Tracking parcelActual = new GlsResponseMapper().map(null, parcelExpected);

        assertEquals(parcelExpected, parcelActual);
    }

}