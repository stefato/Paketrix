package com.stefato.paketrix.data.remote.api.hermes;

import com.stefato.paketrix.TestModelFactory;
import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.data.remote.api.hermes.response.HermesResponse;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HermesRepositoryTest {
    @Mock
    private HermesService service;

    @Mock
    private HermesResponseMapper mapper;

    @InjectMocks
    private HermesRepository repository;

    private Tracking minimalTracking;

    private HermesResponse hermesResponse;


    @Before
    public void setUp() throws Exception {
        minimalTracking =
                new TestTrackingBuilder().withCarrier(Carrier.HERMES).withDataset(0).buildMinimal();
        hermesResponse = TestModelFactory.buildHermes();
        List<HermesResponse> responseList = new ArrayList<>(1);
        responseList.add(hermesResponse);
        when(service.getTracking(minimalTracking.getTrackingNumber()))
                .thenReturn(Observable.just(responseList));
    }

    @Test
    public void getTracking() throws Exception {
        Tracking expectedTracking =
                new TestTrackingBuilder().withCarrier(Carrier.HERMES).withDataset(0).build();

        when(mapper.map(hermesResponse,minimalTracking)).thenReturn(expectedTracking);

        Tracking actualTracking = repository.getTracking(minimalTracking).blockingFirst();

        assertEquals(expectedTracking, actualTracking);
    }

}