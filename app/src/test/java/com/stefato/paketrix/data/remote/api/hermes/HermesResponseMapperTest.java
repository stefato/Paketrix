package com.stefato.paketrix.data.remote.api.hermes;

import com.stefato.paketrix.TestModelFactory;
import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.data.remote.api.hermes.response.HermesResponse;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HermesResponseMapperTest {

    @Test
    public void fromHermesResponse() throws Exception {
        Tracking expectedTracking =
                new TestTrackingBuilder().withCarrier(Carrier.HERMES).withDataset(0).build();

        HermesResponse hermesResponse = TestModelFactory.buildHermes();
        Tracking minimalTracking =
                new TestTrackingBuilder().withCarrier(Carrier.HERMES).withDataset(0).buildMinimal();

        Tracking parcelActual = new HermesResponseMapper().map(
                hermesResponse, minimalTracking);

        assertEquals(expectedTracking, parcelActual);
    }


    @Test
    public void shouldReturnInputParcelIfReponseIsNull() throws Exception {
        Tracking parcelExpected =
                new TestTrackingBuilder().withCarrier(Carrier.DPD).withDataset(0).buildMinimal();

        Tracking parcelActual = new HermesResponseMapper().map(
                null, parcelExpected);

        assertEquals(parcelExpected, parcelActual);
    }

}