package com.stefato.paketrix.data.remote.api.ups;

import com.stefato.paketrix.TestModelFactory;
import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;

import org.junit.Assert;
import org.junit.Test;

import okhttp3.ResponseBody;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UpsModelMapperTest {

    private Tracking minimalTracking =
            new TestTrackingBuilder().withCarrier(Carrier.UPS).withDataset(0).buildMinimal();
    @Test
    public void fromUpsScrapingModel() throws Exception {
        Tracking trackingExpected =
                new TestTrackingBuilder().withCarrier(Carrier.UPS).withDataset(0).build();



        ResponseBody responseBody = mock(ResponseBody.class);
        when(responseBody.string()).thenReturn(TestModelFactory.buildUps());

        Tracking trackingActual = new UpsResponseMapper().map(responseBody,minimalTracking);

        assertEquals(trackingExpected, trackingActual);
    }

    @Test
    public void shouldReturnInputParcelIfReponseIsNull() throws Exception {
        Tracking expectedTracking =
                new TestTrackingBuilder().withCarrier(Carrier.UPS).withDataset(0).buildMinimal();

        Tracking actualTracking = new UpsResponseMapper().map(null,minimalTracking);

        Assert.assertEquals(expectedTracking, actualTracking);
    }

}