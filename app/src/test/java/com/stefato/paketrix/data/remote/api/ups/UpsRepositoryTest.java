package com.stefato.paketrix.data.remote.api.ups;

import com.stefato.paketrix.TestModelFactory;
import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.ResponseBody;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpsRepositoryTest {

    @Mock
    private UpsScrapingService service;

    @Mock
    private UpsResponseMapper mapper;

    @InjectMocks
    private UpsRepository repository;

    private Tracking minimalTracking = new TestTrackingBuilder()
            .withCarrier(Carrier.DHL).withDataset(0).buildMinimal();

    @Test
    public void getTracking() throws Exception {
        ResponseBody responseBody =
                ResponseBody.create(MediaType.parse("text/html"),TestModelFactory.buildUps());

        when(service.getTracking(minimalTracking.getTrackingNumber(), "de_DE"))
                .thenReturn(Single.just(responseBody));

        Tracking expectedTracking = new TestTrackingBuilder()
                .withCarrier(Carrier.UPS).withDataset(0).build();

        when(mapper.map(responseBody,minimalTracking)).thenReturn(expectedTracking);



        Tracking actualTracking = repository.getTracking(minimalTracking).blockingGet();

        assertEquals(expectedTracking, actualTracking);
    }

}