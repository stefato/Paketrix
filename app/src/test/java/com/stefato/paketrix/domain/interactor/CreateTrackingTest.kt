package com.stefato.paketrix.domain.interactor

import com.stefato.paketrix.TestTrackingBuilder
import com.stefato.paketrix.domain.model.Carrier
import com.stefato.paketrix.domain.model.Tracking
import com.stefato.paketrix.domain.repository.TrackingRepository
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CreateTrackingTest {

    @Mock
    lateinit var trackingRepository: TrackingRepository

    @InjectMocks
    lateinit var createTrackingInteractor: CreateTracking

    private val minimalTracking: Tracking = TestTrackingBuilder().buildMinimal()

    @Test
    @Throws(Exception::class)
    fun createTrackingShouldInsertMinimalTrackingUpdateToRemoteTrackingAndReturnTrue() {
        val tracking = TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).build()
        val id = tracking.id
        val remoteTracking = tracking.copy(id = null)

        `when`(trackingRepository.insertTracking(minimalTracking)).thenReturn(Observable.just(id!!))
        `when`(trackingRepository.getRemoteTracking(minimalTracking)).thenReturn(Observable.just(remoteTracking))
        `when`(trackingRepository.updateTracking(tracking)).thenReturn(Observable.just(java.lang.Boolean.TRUE))

        val expectedBoolean = createTrackingInteractor.createTracking(minimalTracking).blockingFirst()

        assertTrue(expectedBoolean!!)
    }

    @Test
    @Throws(Exception::class)
    fun createTrackingShouldPropagateNetworkErrorAndDeleteObsoleteTracking() {
        val expectedError = Throwable("keine Netzwerkverbindung")

        val (id) = TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).build()

        `when`(trackingRepository.insertTracking(minimalTracking)).thenReturn(Observable.just(id!!))
        `when`(trackingRepository.getRemoteTracking(minimalTracking)).thenReturn(Observable.error(expectedError))
        `when`(trackingRepository.getLastInsertedTracking()).thenReturn(Observable.just(minimalTracking))
        `when`(trackingRepository.deleteTracking(minimalTracking)).thenReturn(Observable.just(java.lang.Boolean.TRUE))

        val testObserver = TestObserver<Boolean>()
        createTrackingInteractor.createTracking(minimalTracking).subscribe(testObserver)

        testObserver.assertError(expectedError)
    }

    @Test
    @Throws(Exception::class)
    fun createTrackingShouldDeleteTrackingIfRemoteTrackingIsInvalidAndReturnFalse() {
        val tracking = TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).build()
        val id = tracking.id
        val remoteTracking = tracking.copy(id = null, waypoints = emptyList())
        val invalidTracking = remoteTracking.copy(id = id)

        `when`(trackingRepository.insertTracking(minimalTracking)).thenReturn(Observable.just(id!!))
        `when`(trackingRepository.getRemoteTracking(minimalTracking)).thenReturn(Observable.just(remoteTracking))
        `when`(trackingRepository.deleteTracking(invalidTracking)).thenReturn(Observable.just(java.lang.Boolean.TRUE))

        val expectedBoolean = createTrackingInteractor.createTracking(minimalTracking).blockingFirst()!!

        assertFalse(expectedBoolean)
    }
}