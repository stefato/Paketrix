package com.stefato.paketrix.domain.interactor;

import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.domain.repository.TrackingRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Observable;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DeleteTrackingTest {

    @Mock
    private TrackingRepository trackingRepository;

    @InjectMocks
    private DeleteTracking deleteTrackingInteractor;

    private Tracking tracking;

    @Before
    public void setUp() throws Exception {
        tracking = new TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).build();
        when(trackingRepository.deleteTracking(tracking)).thenReturn(Observable.just(Boolean.TRUE));
    }

    @Test
    public void deleteTrackingShould() throws Exception {
        Boolean expectedBoolean = deleteTrackingInteractor.delete(tracking).blockingFirst();
        verify(trackingRepository, times(1)).deleteTracking(tracking);

        assertTrue(expectedBoolean);
    }

    @Test
    public void undoLastRemovedShouldInsertLastRemovedTracking() throws Exception {
        deleteTrackingInteractor.delete(tracking).blockingFirst();
        deleteTrackingInteractor.undo();

        verify(trackingRepository, times(1)).insertTracking(tracking);
    }

    @Test
    public void undoLastRemovedShouldDoNothingIfNoTrackingWasRemoved() throws Exception {
        deleteTrackingInteractor.undo();
        verify(trackingRepository, never()).insertTracking(tracking);
    }
}