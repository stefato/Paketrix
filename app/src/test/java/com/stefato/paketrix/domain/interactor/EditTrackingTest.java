package com.stefato.paketrix.domain.interactor;

import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.domain.repository.TrackingRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Observable;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EditTrackingTest {

    @Mock
    private TrackingRepository trackingRepository;

    @InjectMocks
    EditTracking editTrackingInteractor;

    @Test
    public void editTracking() throws Exception {
        Tracking tracking= new TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).buildMinimal();

        when(trackingRepository.updateTracking(tracking)).thenReturn(Observable.just(Boolean.TRUE));
        Boolean actualBoolean=editTrackingInteractor.edit(tracking).blockingFirst();

        assertTrue(actualBoolean);
    }

}