package com.stefato.paketrix.domain.interactor;

import com.stefato.paketrix.TestTrackingBuilder;
import com.stefato.paketrix.domain.model.Carrier;
import com.stefato.paketrix.domain.model.Tracking;
import com.stefato.paketrix.domain.repository.TrackingRepository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetTrackingListTest {

    @Mock
    private TrackingRepository trackingRepository;

    @InjectMocks
    private GetTrackingList getTrackingListInteractor;

    @Test
    public void getTrackingList() throws Exception {
        Tracking tracking1 = new TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).build();
        Tracking tracking2 = new TestTrackingBuilder().withCarrier(Carrier.DPD).withDataset(1).build();

        List<Tracking> trackings = new ArrayList<>(2);
        trackings.add(tracking1);
        trackings.add(tracking2);

        when(trackingRepository.getTrackingsFlowable()).thenReturn(Observable.just(trackings));

        List<Tracking> actualTracking = getTrackingListInteractor.getTrackingList().blockingFirst();

        assertEquals(trackings, actualTracking);
    }

}