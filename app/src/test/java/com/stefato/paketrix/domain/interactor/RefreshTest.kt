package com.stefato.paketrix.domain.interactor

import com.stefato.paketrix.TestTrackingBuilder
import com.stefato.paketrix.domain.model.Carrier
import com.stefato.paketrix.domain.model.Tracking
import com.stefato.paketrix.domain.repository.TrackingRepository
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class RefreshTest {

    @Mock
    lateinit var trackingRepository: TrackingRepository

    @InjectMocks
    lateinit var refreshInteractor: Refresh

    @Test
    @Throws(Exception::class)
    fun refresh() {
        val tracking = TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).build()
        val tracking2 = TestTrackingBuilder().withCarrier(Carrier.DPD).withDataset(1).build()


        val trackings = ArrayList<Tracking>(2)
        trackings.add(tracking)
        trackings.add(tracking2)

        `when`(trackingRepository.getTrackings()).thenReturn(Observable.just(trackings))
        `when`(trackingRepository.getRemoteTrackings(trackings)).thenReturn(Observable.just(trackings))
        `when`(trackingRepository.updateTrackings(trackings)).thenReturn(Observable.just(java.lang.Boolean.TRUE))

        val actualBoolean = refreshInteractor.refresh().blockingFirst()

        assertTrue(actualBoolean!!)
    }

    @Test
    @Throws(Exception::class)
    fun refreshAndDiff() {
        val tracking = TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).build()
        val tracking2 = TestTrackingBuilder().withCarrier(Carrier.DPD).withDataset(1).build()


        val trackings = ArrayList<Tracking>(2)
        trackings.add(tracking)
        trackings.add(tracking2)

        `when`(trackingRepository.getTrackings()).thenReturn(Observable.just(trackings))
        `when`(trackingRepository.getRemoteTrackings(trackings)).thenReturn(Observable.just(trackings))
        `when`(trackingRepository.updateTrackings(trackings)).thenReturn(Observable.just(java.lang.Boolean.TRUE))

        val actualList = refreshInteractor.refreshAndDiff().blockingFirst()

        assertTrue(actualList.isEmpty())
    }

    @Test
    @Throws(Exception::class)
    fun refreshAndDiff2() {
        val tracking = TestTrackingBuilder().withCarrier(Carrier.DHL).withDataset(0).build()
        val tracking2 = TestTrackingBuilder().withCarrier(Carrier.DPD).withDataset(1).build()


        val trackings = ArrayList<Tracking>(2)
        trackings.add(tracking)
        trackings.add(tracking2)

        val list2 = ArrayList(trackings)

        val newTracking = tracking2.copy(tag = "abc")
        list2[1] = newTracking

        val expectedTrackings = ArrayList<Tracking>(1)
        expectedTrackings.add(newTracking)

        `when`(trackingRepository.getTrackings()).thenReturn(Observable.just(trackings)).thenReturn(Observable.just(trackings)).thenReturn(Observable.just(list2))
        `when`(trackingRepository.getRemoteTrackings(trackings)).thenReturn(Observable.just(list2))
        `when`(trackingRepository.updateTrackings(list2)).thenReturn(Observable.just(java.lang.Boolean.TRUE))

        val actualTrackings = refreshInteractor.refreshAndDiff().blockingFirst()

        assertEquals(expectedTrackings, actualTrackings)
    }

}