package com.stefato.paketrix.domain.interactor;

import com.stefato.paketrix.UnitTestData;
import com.stefato.paketrix.domain.model.Carrier;

import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidateCarriersTest {

    private static final String INVALID_TRACKING_NUMBER = "????????$§!????";


    @Test
    public void shouldMatchDhl() throws Exception {
        String trackingNumber = UnitTestData.getTRACKING_NUMBERS().get(Carrier.DHL);

        Set<Carrier> actualCarriers = ValidateCarriers.validateTrackingNumber(trackingNumber);

        assertTrue(actualCarriers.contains(Carrier.DHL));
    }

    @Test
    public void shouldMatchDpd() throws Exception {
        String trackingNumber = UnitTestData.getTRACKING_NUMBERS().get(Carrier.DPD);

        Set<Carrier> actualCarriers = ValidateCarriers.validateTrackingNumber(trackingNumber);

        assertTrue(actualCarriers.contains(Carrier.DPD));
    }

    @Test
    public void shouldMatchHermes() throws Exception {
        String trackingNumber = UnitTestData.getTRACKING_NUMBERS().get(Carrier.HERMES);

        Set<Carrier> actualCarriers = ValidateCarriers.validateTrackingNumber(trackingNumber);

        assertTrue(actualCarriers.contains(Carrier.HERMES));
    }

    @Test
    public void shouldMatchGls() throws Exception {
        String trackingNumber = UnitTestData.getTRACKING_NUMBERS().get(Carrier.GLS);

        Set<Carrier> actualCarriers = ValidateCarriers.validateTrackingNumber(trackingNumber);

        assertTrue(actualCarriers.contains(Carrier.GLS));
    }

    @Test
    public void shouldMatchUps() throws Exception {
        String trackingNumber = UnitTestData.getTRACKING_NUMBERS().get(Carrier.UPS);

        Set<Carrier> actualCarriers = ValidateCarriers.validateTrackingNumber(trackingNumber);

        assertTrue(actualCarriers.contains(Carrier.UPS));
    }

    @Test
    public void shouldMatchNone() throws Exception {
        Set<Carrier> expectedCarriers = new HashSet<>();
        expectedCarriers.add(Carrier.NONE);

        Set<Carrier> actualCarriers =
                ValidateCarriers.validateTrackingNumber(INVALID_TRACKING_NUMBER);

        assertEquals(expectedCarriers, actualCarriers);
    }

    @Test
    public void DhlTrackingNumberShouldBeValid() {
        String trackingNumber = UnitTestData.getTRACKING_NUMBERS().get(Carrier.DHL);

        assertTrue(ValidateCarriers.isTrackingNumberValid(trackingNumber));
    }

    @Test
    public void GlsTrackingNumberShouldBeValid() {
        String trackingNumber = UnitTestData.getTRACKING_NUMBERS().get(Carrier.GLS);

        assertTrue(ValidateCarriers.isTrackingNumberValid(trackingNumber));
    }

    @Test
    public void DPDTrackingNumberShouldBeValid() {
        String trackingNumber = UnitTestData.getTRACKING_NUMBERS().get(Carrier.DHL);

        assertTrue(ValidateCarriers.isTrackingNumberValid(trackingNumber));
    }

    @Test
    public void HermesTrackingNumberShouldBeValid() {
        String trackingNumber = UnitTestData.getTRACKING_NUMBERS().get(Carrier.HERMES);

        assertTrue(ValidateCarriers.isTrackingNumberValid(trackingNumber));
    }

    @Test
    public void UpsTrackingNumberShouldBeValid() {
        String trackingNumber = UnitTestData.getTRACKING_NUMBERS().get(Carrier.UPS);

        assertTrue(ValidateCarriers.isTrackingNumberValid(trackingNumber));
    }

    @Test
    public void InvalidTrackingNumberShouldNotBeValid() {
        assertFalse(ValidateCarriers.isTrackingNumberValid(INVALID_TRACKING_NUMBER));
    }

}