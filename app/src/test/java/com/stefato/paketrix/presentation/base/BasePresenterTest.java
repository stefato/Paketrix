package com.stefato.paketrix.presentation.base;

import org.junit.Test;

public class BasePresenterTest {
    @Test
    public void detachView() throws Exception {
    }

    @Test
    public void attachView() throws Exception {
    }

    @Test
    public void isViewAttached() throws Exception {
    }

    @Test
    public void checkViewAttached() throws Exception {
    }

    @Test
    public void addDisposable() throws Exception {
    }

}