package com.stefato.paketrix.presentation.views.addtracking;

import com.stefato.paketrix.testutil.RxImmediateSchedulerRule;
import com.stefato.paketrix.presentation.MainModel;

import org.junit.Before;
import org.junit.ClassRule;
import org.mockito.Mock;

public class AddTrackingPresenterTest {


    @ClassRule
    public static final RxImmediateSchedulerRule schedulers = new RxImmediateSchedulerRule();
    @Mock
    MainModel mainModel;
    @Mock
    AddTrackingView view;
    AddTrackingPresenter presenter;

    @Before
    public void setup() {
        presenter = new AddTrackingPresenter(mainModel);
        presenter.attachView(view);

    }

}