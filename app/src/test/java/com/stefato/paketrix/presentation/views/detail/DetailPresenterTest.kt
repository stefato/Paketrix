package com.stefato.paketrix.presentation.views.detail

import com.stefato.paketrix.presentation.MainModel
import com.stefato.paketrix.presentation.model.TrackingUiModel
import com.stefato.paketrix.testutil.RxImmediateSchedulerRule
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DetailPresenterTest {

    companion object {
        @JvmField
        @ClassRule
        val schedulers = RxImmediateSchedulerRule()
    }


    @Mock
    lateinit var view: DetailView
    @Mock
    lateinit var mainModel: MainModel
    @Captor
    private val argCaptor: ArgumentCaptor<DetailUiModel>? = null

    @InjectMocks
    lateinit var presenter: DetailPresenter

    @Test
    @Throws(Exception::class)
    fun shouldLoadItem() {
        val expectedUiModel = DetailUiModel.successWithContent(TrackingUiModel())


        `when`(view.loadItem()).thenReturn(Observable.just(java.lang.Boolean.TRUE))
        `when`(mainModel.detailItemSubject)
                .thenReturn(Observable.just(TrackingUiModel()))

        presenter.attachView(view)
        verify(view).render(argCaptor!!.capture())
        val actualUiModel = argCaptor.value

        assertEquals(expectedUiModel, actualUiModel)
    }

    @Test
    @Throws(Exception::class)
    fun shouldRenderErrorStateOnError() {
        val throwable = Throwable()
        val expectedUiModel = DetailUiModel.error(throwable)

        `when`(view.loadItem()).thenReturn(Observable.just(java.lang.Boolean.TRUE))
        `when`(mainModel.detailItemSubject).thenReturn(Observable.error(throwable))

        presenter.attachView(view)
        verify(view).render(argCaptor!!.capture())
        val actualUiModel = argCaptor.value

        assertEquals(expectedUiModel, actualUiModel)
    }



}