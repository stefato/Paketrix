package com.stefato.paketrix.presentation.views.list

import com.stefato.paketrix.domain.interactor.*
import com.stefato.paketrix.domain.model.Tracking
import com.stefato.paketrix.presentation.MainModel
import com.stefato.paketrix.presentation.model.TrackingUiModel
import com.stefato.paketrix.presentation.navigation.ListViewNavigator
import com.stefato.paketrix.testutil.RxImmediateSchedulerRule
import io.reactivex.Observable
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.ClassRule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import java.util.*

@RunWith(MockitoJUnitRunner::class)
class ListPresenterTest {

    companion object {
        @JvmField
        @ClassRule
        val schedulers = RxImmediateSchedulerRule()
    }

    @Mock
    lateinit var view: ListView
    @Mock
    lateinit var mainModel: MainModel
    @Mock
    lateinit var deleteTrackingInteractor: DeleteTracking
    @Mock
    lateinit var getTrackingListInteractor: GetTrackingList
    @Mock
    lateinit var editTrackingInteractor: EditTracking
    @Mock
    lateinit var refreshInteractor: Refresh
    @Mock
    lateinit var createTrackingInteractor: CreateTracking
    @Mock
    lateinit var navigator: ListViewNavigator

    @InjectMocks
    lateinit var presenter: ListPresenter

    @Captor
    lateinit var argCaptor: ArgumentCaptor<ListUiModel>
    @Captor
    lateinit var trackingUiModelCaptor: ArgumentCaptor<TrackingUiModel>
    @Captor
    lateinit var trackingCaptor: ArgumentCaptor<Tracking>

    @Before
    @Throws(Exception::class)
    fun setUp() {
        `when`(view.fabClick()).thenReturn(Observable.empty())
        `when`(view.loadItems()).thenReturn(Observable.empty())
        `when`(view.refresh()).thenReturn(Observable.empty())
        `when`(view.clearState()).thenReturn(Observable.empty())
        `when`(view.delete()).thenReturn(Observable.empty())
        `when`(view.trackingClick()).thenReturn(Observable.empty())
        `when`(view.getEditObservable()).thenReturn(Observable.empty())
        `when`(view.getUndoObservable()).thenReturn(Observable.empty())
        `when`(mainModel.createTrackingSubject).thenReturn(Observable.empty())
    }

    @Test
    @Throws(Exception::class)
    fun shouldOpenDialogOnFabClick() {
        `when`(view.fabClick()).thenReturn(Observable.just(Any()))

        presenter.attachView(view)

        verify<ListViewNavigator>(navigator).showDialog()
    }

    @Test
    @Throws(Exception::class)
    fun shouldUpdateParcelOnEdit() {
        val expectedTracking = Tracking(name = "Name")

        `when`(view.getEditObservable()).thenReturn(Observable.just(TrackingUiModel.fromTracking(expectedTracking)))
        `when`(editTrackingInteractor.edit(expectedTracking))
                .thenReturn(Observable.just(java.lang.Boolean.TRUE))

        presenter.attachView(view)
        verify(editTrackingInteractor).edit(trackingCaptor.capture())
        val actualTracking = trackingCaptor.value

        assertEquals(expectedTracking, actualTracking)
    }

    @Test
    @Throws(Exception::class)
    fun shouldSetDetailItemAndOpenTrackingOnParcelClick() {
        val expectedTracking = TrackingUiModel(name = "Name")

        `when`(view.trackingClick()).thenReturn(Observable.just(expectedTracking))

        presenter.attachView(view)
        verify(mainModel).setDetailItem(trackingUiModelCaptor.capture())
        val actualTracking = trackingUiModelCaptor.value

        verify(navigator).openTracking()
        assertEquals(expectedTracking, actualTracking)
    }

    @Test
    @Throws(Exception::class)
    fun shouldLoadItems() {
        val expectedList = listOf(TrackingUiModel(name = "Name"))
        val expectedUiModels = getUiModelList(
                ListUiModel.idle(),
                ListUiModel.inProgress(),
                ListUiModel.successWithContent(expectedList))

        `when`(view.loadItems()).thenReturn(Observable.just(java.lang.Boolean.TRUE))
        `when`(getTrackingListInteractor.trackingList).thenReturn(Observable.just(TrackingUiModel.toTrackingList(expectedList)))

        presenter.attachView(view)
        verify(view, times(3)).render(argCaptor.capture())
        val actualUiModels = argCaptor.allValues

        assertEquals(expectedUiModels, actualUiModels)
    }

    @Test
    @Throws(Exception::class)
    fun shouldInvokeUndoOnViewUndo() {
        `when`(view.getUndoObservable()).thenReturn(Observable.just(java.lang.Boolean.TRUE))

        presenter.attachView(view)

        verify<DeleteTracking>(deleteTrackingInteractor, times(1)).undo()
    }

    @Test
    @Throws(Exception::class)
    fun shouldRefresh() {
        val expectedUiModels = getUiModelList(
                ListUiModel.idle(),
                ListUiModel.inProgress(),
                ListUiModel.idle())

        `when`(view.refresh()).thenReturn(Observable.just(java.lang.Boolean.TRUE))
        `when`(refreshInteractor.refresh()).thenReturn(Observable.just(java.lang.Boolean.TRUE))

        presenter.attachView(view)
        verify(view, times(3)).render(argCaptor.capture())
        val actualUiModels = argCaptor.allValues

        assertEquals(expectedUiModels, actualUiModels)
    }

    @Test
    @Throws(Exception::class)
    fun shouldRenderErrorRefresh() {
        val expectedError = Throwable("keine Netwerkverbindung")
        val expectedUiModels = getUiModelList(
                ListUiModel.idle(),
                ListUiModel.inProgress(),
                ListUiModel.error(expectedError))

        `when`(view.refresh()).thenReturn(Observable.just(java.lang.Boolean.TRUE))
        `when`(refreshInteractor.refresh()).thenReturn(Observable.error(expectedError))

        presenter.attachView(view)
        verify(view, times(3)).render(argCaptor.capture())
        val actualUiModels = argCaptor.allValues

        assertEquals(expectedUiModels, actualUiModels)
    }

    @Test
    @Throws(Exception::class)
    fun shouldDelete() {
        val expectedDeletedTracking = Tracking()
        val expectedUiModels = getUiModelList(
                ListUiModel.idle(),
                ListUiModel.deleted())

        `when`(view.delete()).thenReturn(Observable.just(TrackingUiModel.fromTracking(expectedDeletedTracking)))
        `when`(deleteTrackingInteractor.delete(expectedDeletedTracking))
                .thenReturn(Observable.just(java.lang.Boolean.TRUE))

        presenter.attachView(view)
        verify(view, times(2)).render(argCaptor.capture())
        val actualUiModels = argCaptor.allValues

        assertEquals(expectedUiModels, actualUiModels)
    }

    @Test
    @Throws(Exception::class)
    fun shouldClearState() {
        val firstExpectedUiModel = ListUiModel.idle()
        val secondExpectedUiModel = ListUiModel.idle()
        val expectedUiModels = ArrayList<ListUiModel>(2)
        expectedUiModels.add(firstExpectedUiModel)
        expectedUiModels.add(secondExpectedUiModel)

        `when`(view.clearState()).thenReturn(Observable.just(java.lang.Boolean.TRUE))

        presenter.attachView(view)
        verify(view, times(2)).render(argCaptor.capture())
        val actualUiModels = argCaptor.allValues

        assertEquals(expectedUiModels, actualUiModels)
    }

    @Test
    @Throws(Exception::class)
    fun shouldCreateTracking() {
        val expectedCreatedTracking = Tracking()
        val expectedUiModel = ListUiModel.created(true)

        `when`(mainModel.createTrackingSubject)
                .thenReturn(Observable.just(TrackingUiModel.fromTracking(expectedCreatedTracking)))
        `when`(createTrackingInteractor.createTracking(expectedCreatedTracking))
                .thenReturn(Observable.just(java.lang.Boolean.TRUE))

        presenter.attachView(view)
        verify(view, times(1)).render(argCaptor.capture())
        val actualUiModel = argCaptor.value

        assertEquals(expectedUiModel, actualUiModel)
    }

    @Test
    @Throws(Exception::class)
    fun shouldRenderCreateTrackingError() {
        val expectedError = Throwable("keine Netwerkverbindung")
        val expectedCreatedTracking = Tracking()
        val expectedUiModel = ListUiModel.error(expectedError)

        `when`<Observable<TrackingUiModel>>(mainModel.createTrackingSubject)
                .thenReturn(Observable.just(TrackingUiModel.fromTracking(expectedCreatedTracking)))
        `when`(createTrackingInteractor.createTracking(expectedCreatedTracking))
                .thenReturn(Observable.error(expectedError))

        presenter.attachView(view)
        verify(view, times(1)).render(argCaptor.capture())
        val actualUiModel = argCaptor.value

        assertEquals(expectedUiModel, actualUiModel)
    }

    private fun getUiModelList(vararg listUiModels: ListUiModel): List<ListUiModel> {
        val uiModelList = ArrayList<ListUiModel>(listUiModels.size)
        Collections.addAll(uiModelList, *listUiModels)
        return uiModelList
    }

}